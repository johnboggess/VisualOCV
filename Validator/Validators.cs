﻿/* Created by John Bogggess 
 * Created to fullfill masters degree requirements at Southern Adventist Univeristy
 * 8-4-2020
 * johnboggess@jboggess.net
 */
using System;

namespace Validator
{
    public class Validators
    {
        public static Tuple<bool, string> LessThan<T>(T input, T value) where T: IComparable
        {
            return Tuple.Create(input.CompareTo(value) < 0, "Value must be less than "+value);
        }

        public static Tuple<bool, string> GreaterThan<T>(T input, T value) where T : IComparable
        {
            return Tuple.Create(input.CompareTo(value) > 0, "Value must be greater than " + value);
        }

        public static Tuple<bool, string> Equals<T>(T input, T value) where T : IComparable
        {
            return Tuple.Create(input.CompareTo(value) == 0, "Value must be equal to " + value);
        }

        public static Tuple<bool, string> NotEqual<T>(T input, T value) where T : IComparable
        {
            return Tuple.Create(input.CompareTo(value) != 0, "Value must not be equal to " + value);
        }

        public static Tuple<bool, string> InRange<T>(T input, T min, T max, bool inclusive) where T : IComparable
        {
            if (inclusive)
            {
                bool greaterThanEqualToMin = input.CompareTo(min) >= 0;
                bool lessThanEqualToMax = input.CompareTo(min) <= 0;
                return Tuple.Create(greaterThanEqualToMin && lessThanEqualToMax, "Value must be between " + min +" and "+max +" inclusive");
            }
            else
            {
                bool greaterThanToMin = input.CompareTo(min) > 0;
                bool lessThanToMax = input.CompareTo(min) < 0;
                return Tuple.Create(greaterThanToMin && lessThanToMax, "Value must be between " + min + " and " + max + " exclusive");
            }
        }

        public static Tuple<bool, string> NotInRangee<T>(T input, T min, T max, bool inclusive) where T : IComparable
        {
            if (inclusive)
            {
                bool greaterThanEqualToMin = input.CompareTo(min) >= 0;
                bool lessThanEqualToMax = input.CompareTo(min) <= 0;
                return Tuple.Create(!(greaterThanEqualToMin && lessThanEqualToMax), "Value must not be between " + min + " and " + max + " inclusive");
            }
            else
            {
                bool greaterThanToMin = input.CompareTo(min) > 0;
                bool lessThanToMax = input.CompareTo(min) < 0;
                return Tuple.Create(!(greaterThanToMin && lessThanToMax), "Value must not be between " + min + " and " + max + " exclusive");
            }
        }

        public static Tuple<bool, string> EvenNumber(double input)
        {
            return Tuple.Create(input % 2 == 0, "Value must be even");
        }

        public static Tuple<bool, string> OddNumber(double input)
        {
            return Tuple.Create(input % 2 != 0, "Value must be odd");
        }
    }
}
