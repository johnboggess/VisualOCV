﻿/* Created by John Bogggess 
 * Created to fullfill masters degree requirements at Southern Adventist Univeristy
 * 8-4-2020
 * johnboggess@jboggess.net
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.IO;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Reflection;
using Microsoft.Win32;

namespace OperatorConstructor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ListCollectionView typeListSource;
        ListCollectionView constructorListSource;
        ListCollectionView methodListSource;

        public MainWindow()
        {
            InitializeComponent();
            GetTypesInAssembly(Assembly.GetAssembly(typeof(int)));
        }

        public void GetTypesInAssembly(Assembly assembly)
        {
            Type[] types = assembly.GetTypes();
            typeListSource = new ListCollectionView(types);
            TypeList.ItemsSource = typeListSource;
        }

        public void GetConstructorsInType(Type type)
        {
            ConstructorInfo[] constructor = type.GetConstructors();
            constructorListSource = new ListCollectionView(constructor);
            ConstructorList.ItemsSource = constructorListSource;
        }

        public void GetMethodsInType(Type type)
        {
            MethodInfo[] methods = type.GetMethods();
            methodListSource = new ListCollectionView(methods);
            MethodList.ItemsSource = methodListSource;
        }

        private void SearchTypes_TextChanged(object sender, TextChangedEventArgs e)
        {
            typeListSource.Filter = (o) =>
            {
                return ((Type)o).Name.ToLower().Contains(tb_search_types.Text.ToLower());
            };
        }

        private void SearchMethods_TextChanged(object sender, TextChangedEventArgs e)
        {
            methodListSource.Filter = (o) =>
            {
                return ((MethodInfo)o).Name.ToLower().Contains(tb_search_functions.Text.ToLower());
            };
        }

        private void TypeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(sender == null) { return; }
            Type type = (Type)((ListBox)sender).SelectedItem;
            if(type == null) { return; }

            GetConstructorsInType(type);
            GetMethodsInType(type);

            AQN.Text = type.AssemblyQualifiedName;
        }

        private void ConstructorList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == null) { return; }
            ConstructorInfo constructor = (ConstructorInfo)((ListBox)sender).SelectedItem;
            if (constructor == null) { return; }

            MethodConstructorNameLabel.Text = "Constructor Name:";
            MethodConstructorName.Text = constructor.Name;

            MethodReturnTypeLabel.Visibility = Visibility.Collapsed;
            MethodReturnType.Visibility = Visibility.Collapsed;
        }

        private void MethodList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == null) { return; }
            MethodInfo method = (MethodInfo)((ListBox)sender).SelectedItem;
            if (method == null) { return; }

            MethodConstructorNameLabel.Text = "Method Name:";
            MethodConstructorName.Text = method.Name;
            
            ParamTypes.ItemsSource = method.GetParameters();

            MethodReturnTypeLabel.Visibility = Visibility.Visible;
            MethodReturnType.Visibility = Visibility.Visible;

            MethodReturnType.Text = method.ReturnType.Name;



            StringBuilder stringBuilder = new StringBuilder();
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlWriterSettings);

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Operator");
            
            xmlWriter.WriteStartElement("Type");
            xmlWriter.WriteAttributeString("AssemblyQualifiedName", method.DeclaringType.AssemblyQualifiedName);
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("Method");
            xmlWriter.WriteAttributeString("Name", method.Name);
            for (int i = 0; i < method.GetParameters().Length; i++)
            {
                xmlWriter.WriteAttributeString("param" + (i + 1), method.GetParameters()[i].ParameterType.AssemblyQualifiedName);
            }
            xmlWriter.WriteEndElement();
            
            xmlWriter.WriteStartElement("ID");
            xmlWriter.WriteAttributeString("ID", "OPERATOR_ID");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("OperatorName");
            xmlWriter.WriteAttributeString("Name", "OPERATOR_NAME");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("OperatorInputs");

                if(!method.IsStatic)
                {
                    xmlWriter.WriteStartElement("Input");

                        xmlWriter.WriteStartElement("Name");
                        xmlWriter.WriteAttributeString("Name", method.DeclaringType.Name);
                        xmlWriter.WriteEndElement();

                        xmlWriter.WriteStartElement("Type");
                        xmlWriter.WriteAttributeString("AssemblyQualifiedName", method.DeclaringType.AssemblyQualifiedName);
                        xmlWriter.WriteEndElement();

                        xmlWriter.WriteStartElement("Control");
                        xmlWriter.WriteAttributeString("Control", GetInputControlForType(method.DeclaringType));
                        xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
                }

                for (int i = 0; i < method.GetParameters().Length; i++)
                {
                    xmlWriter.WriteStartElement("Input");

                        xmlWriter.WriteStartElement("Name");
                        xmlWriter.WriteAttributeString("Name", method.GetParameters()[i].Name);
                        xmlWriter.WriteEndElement();

                        xmlWriter.WriteStartElement("MethodParamIndex");
                        xmlWriter.WriteAttributeString("Index", i.ToString());
                        xmlWriter.WriteEndElement();

                        xmlWriter.WriteStartElement("Control");
                        xmlWriter.WriteAttributeString("Control", GetInputControlForType(method.GetParameters()[i].ParameterType));
                        xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
                }
            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("OperatorOutputs");
                xmlWriter.WriteStartElement("Output");

                    xmlWriter.WriteStartElement("Name");
                    xmlWriter.WriteAttributeString("Name", method.ReturnType.Name);
                    xmlWriter.WriteEndElement();
            
                    xmlWriter.WriteStartElement("Control");
                    xmlWriter.WriteAttributeString("Control", GetOutputControlForType(method.ReturnType));
                    xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();

            xmlWriter.Close();

            OperatorXML.Text = stringBuilder.ToString();
        }

        private void Load_Assembly_Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "DotNet DLL (*.dll) | *.dll";
            if(openFileDialog.ShowDialog() == true)
            {
                string path = openFileDialog.FileName;
                GetTypesInAssembly(Assembly.LoadFile(path));
            }
        }

        private void Clear_Assembly_Button_Click(object sender, RoutedEventArgs e)
        {
            TypeList.ItemsSource = null;
            MethodList.ItemsSource = null;
            ConstructorList.ItemsSource = null;
        }

        private string GetInputControlForType(Type type)
        {
            if (typeof(bool) == type) { return nameof(VisualOCVBackend.Controls.Checkbox); }
            else if (typeof(byte) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<byte>); }
            else if (typeof(sbyte) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<sbyte>); }
            else if (typeof(char) == type) { return nameof(VisualOCVBackend.Controls.Textbox); }
            else if (typeof(decimal) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<decimal>); }
            else if (typeof(double) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<double>); }
            else if (typeof(float) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<float>); }
            else if (typeof(int) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<int>); }
            else if (typeof(uint) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<uint>); }
            else if (typeof(long) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<long>); }
            else if (typeof(ulong) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<ulong>); }
            else if (typeof(short) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<short>); }
            else if (typeof(ushort) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<ushort>); }
            else if (typeof(string) == type) { return nameof(VisualOCVBackend.Controls.SliderWithTextbox<string>); }
            else { return nameof(VisualOCVBackend.Controls.ControlType.None); }
        }

        private string GetOutputControlForType(Type type)
        {
            if (type.IsGenericType && typeof(Emgu.CV.Image<Emgu.CV.Structure.Rgb, byte>).GetGenericTypeDefinition().AssemblyQualifiedName == type.GetGenericTypeDefinition().AssemblyQualifiedName) { return nameof(VisualOCVBackend.Controls.ImageControl); }
            else { return nameof(VisualOCVBackend.Controls.ControlType.Checkbox); }
        }
    }
}
