﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;
namespace VisualOCVBackend
{
    public static class VideoManager
    {
        static Dictionary<int, Webcam> WebCams = new Dictionary<int, Webcam>();
        static Dictionary<string, Video> VideoFiles = new Dictionary<string, Video>();

        public static void Init()
        {
            WebCams.Add(0, new Webcam(0));//todo: Detect all web cams
        }

        #region WebCam functions
        public static void StartWebCam(int i)
        {
            try
            {
                if (!WebCams[i].IsRunning)
                {
                    WebCams[i].Start();
                }
            }
            catch
            {
                throw new Exception("Unable to start camera " + i);
            }
        }

        public static void StopWebCam(int i)
        {
            try
            {
                if (WebCams[i].IsRunning)
                {
                    WebCams[i].Stop();
                }
            }
            catch
            {
                throw new Exception("Unable to access camera " + i);
            }
        }

        public static bool IsWebCamRunning(int i)
        {
            try
            {
                return WebCams[i].IsRunning;
            }
            catch
            {
                throw new Exception("Unable to access camera " + i);
            }
        }

        public static Image<Rgb, byte> QueryWebCamImage(int i)
        {
            try
            {
                return WebCams[i].QueryFrame();
            }
            catch
            {
                throw new Exception("Unable to access camera " + i);
            }
        }
        #endregion

        #region Video functions
        public static void LoadVideo(string filePath)
        {
            try
            {
                if (!VideoFiles.ContainsKey(filePath) && File.Exists(filePath))
                {
                    VideoFiles[filePath] = new Video(filePath);
                }
                else if (!File.Exists(filePath))
                    throw new Exception();
            }
            catch
            {
                throw new Exception("Unable to load video " + filePath);
            }
        }

        public static void UnLoadVideo(string filePath)
        {
            if (VideoFiles.ContainsKey(filePath))
            {
                VideoFiles.Remove(filePath);
            }
        }

        public static Image<Rgb, byte> QueryVideoImage(string filePath)
        {
            LoadVideo(filePath);
            return VideoFiles[filePath].QueryFrame();
        }
        #endregion


        class VideoSource
        {
            internal VideoCapture _VideoCapture;
        }

        class Webcam : VideoSource
        {
            public bool IsRunning = false;
            public Webcam(int i)
            {
                _VideoCapture = new VideoCapture(i);
            }

            public void Start() { _VideoCapture.Start(); IsRunning = true; }
            public void Stop() { _VideoCapture.Stop(); IsRunning = false; }
            public Image<Rgb, byte> QueryFrame()
            {
                try
                {
                    return _VideoCapture.QueryFrame().ToImage<Bgr, byte>().Convert<Rgb, byte>();
                }
                catch
                {
                    return new Image<Rgb, byte>(new byte[,,] { { { 0, 0, 0 } } });//todo: QueryFrame throws errors sometimes, fix
                }
            }
        }

        class Video : VideoSource
        {
            int currentFrame = 0;
            public Video(string file)
            {
                _VideoCapture = new VideoCapture(file);
            }
            
            public Image<Rgb, byte> QueryFrame()
            {
                double maxFrames = _VideoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
                currentFrame += 1;
                if(currentFrame > maxFrames)
                {
                    _VideoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosAviRatio, 0);
                    currentFrame = 0;
                }
                return _VideoCapture.QueryFrame().ToImage<Bgr, byte>().Convert<Rgb, byte>();
            }
        }
    }
}
