﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Reflection;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using VisualOCVBackend.Operators;


namespace VisualOCVBackend.Controls
{
    public class ImageControl : Control
    {
        public ImageControl(Operator op, OperatorInputOutput inputOutput) : base(op, inputOutput, ControlType.Image)
        {
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new ImageControl(op,inputOutput);
        }

        public Bitmap ImageToBitmap()
        {
            try
            {
                return Utilities.ImageToBitmap(InputOutput.Value);
            }
            catch (Exception e)
            {//Sometime an exception occurs when trying to convert the image. It complains that it cant convert an rgb image to bgr because BGR2BGR is not valid. Calling  ImageToBitmap a second time does not give this exception
                return Utilities.ImageToBitmap(InputOutput.Value);
            }
        }

        public string ColorType()
        {
            if (InputOutput.Value != null)
            {
                return InputOutput.Value.GetType().GenericTypeArguments[0].Name;
            }
            return "None";
        }

        public string DepthType()
        {
            if (InputOutput.Value != null)
            {
                return InputOutput.Value.GetType().GenericTypeArguments[1].Name;
            }
            return "None";
        }

        public void LoadImageFromFile(string filePath)
        {
            InputOutput.Value = new Image<Rgb, byte>(filePath);
            InputOutput.Operator.ShouldReprocess = true;
        }
    }
}
