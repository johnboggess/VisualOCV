﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public enum ControlType
    {
        Label,
        Checkbox,
        Slider,
        SliderWithTextbox,
        Textbox,
        MultilineTextbox,
        Image,
        Dropdown,
        Button,
        None
    }

    public abstract class Control
    {
        public Operator Operator;
        public OperatorInputOutput InputOutput;
        public bool Enabled;
        public ControlType ControlType;
        
        public Control(Operator op, OperatorInputOutput inputOutput, ControlType controlType)
        {
            ControlType = controlType;
            Operator = op;
            InputOutput = inputOutput;
        }

        public abstract Control CreateCopy(Operator op, OperatorInputOutput inputOutput);
    }
}
