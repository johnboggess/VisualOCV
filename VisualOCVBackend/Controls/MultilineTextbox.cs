﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public class MultilineTextbox : Control
    {
        public string Value = "";

        public MultilineTextbox(Operator op, OperatorInputOutput inputOutput, string value) : base(op, inputOutput, ControlType.MultilineTextbox)
        {
            Value = value;
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new MultilineTextbox(op, inputOutput, Value);
        }
    }
}
