﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public class Checkbox : Control
    {
        public bool Value;
        public Checkbox(Operator op, OperatorInputOutput inputOutput, bool value) : base(op, inputOutput, ControlType.Checkbox)
        {
            Value = value;
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new Checkbox(op, inputOutput, Value);
        }
    }
}
