﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public class SliderWithTextbox<T> : Control
    {
        public T DefaultValue;
        public T Min;
        public T Max;

        public SliderWithTextbox(Operator op, OperatorInputOutput inputOutput, string defaultValue, string min, string max) : base(op, inputOutput, ControlType.SliderWithTextbox)
        {
            if (typeof(T) == typeof(byte))
            {
                DefaultValue = (T)Convert.ChangeType(byte.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(byte.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(byte.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(sbyte))
            {
                DefaultValue = (T)Convert.ChangeType(sbyte.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(sbyte.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(sbyte.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(short))
            {
                DefaultValue = (T)Convert.ChangeType(short.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(short.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(short.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(ushort))
            {
                DefaultValue = (T)Convert.ChangeType(ushort.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(ushort.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(ushort.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(int))
            {
                DefaultValue = (T)Convert.ChangeType(int.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(int.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(int.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(uint))
            {
                DefaultValue = (T)Convert.ChangeType(uint.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(uint.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(uint.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(float))
            {
                DefaultValue = (T)Convert.ChangeType(float.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(float.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(float.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(double))
            {
                DefaultValue = (T)Convert.ChangeType(double.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(double.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(double.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(long))
            {
                DefaultValue = (T)Convert.ChangeType(long.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(long.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(long.Parse(max), typeof(T));
            }
            else if (typeof(T) == typeof(ulong))
            {
                DefaultValue = (T)Convert.ChangeType(ulong.Parse(defaultValue), typeof(T));
                Min = (T)Convert.ChangeType(ulong.Parse(min), typeof(T));
                Max = (T)Convert.ChangeType(ulong.Parse(max), typeof(T));
            }
        }
        
        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            SliderWithTextbox<T> slider = new SliderWithTextbox<T>(op, inputOutput, "0", "0", "0");
            slider.DefaultValue = DefaultValue;
            slider.Min = Min;
            slider.Max = Max;
            return slider;
        }
    }
}
