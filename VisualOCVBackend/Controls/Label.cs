﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public class Label : Control
    {
        public string Value = "";

        public Label(Operator op, OperatorInputOutput inputOutput, string value) : base(op, inputOutput, ControlType.Label)
        {
            Value = value;
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new Label(op, inputOutput, Value);
        }
    }
}
