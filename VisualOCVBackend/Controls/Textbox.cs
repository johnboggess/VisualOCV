﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    public class Textbox : Control
    {
        public string Value = "";

        public Textbox(Operator op, OperatorInputOutput inputOutput, string value) : base(op, inputOutput, ControlType.Textbox)
        {
            Value = value;
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new Textbox(op, inputOutput, Value);
        }
    }
}
