﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.Controls
{
    class None : Control
    {
        public None(Operator op, OperatorInputOutput inputOutput) : base(op, inputOutput, ControlType.None) { }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            return new None(op, inputOutput);
        }
    }
}
