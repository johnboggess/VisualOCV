﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using VisualOCVBackend.Operators;

namespace VisualOCVBackend.Controls
{
    public class Dropdown : Control
    {
        public Dictionary<string, string> Options = new Dictionary<string, string>();
        public string DefaultOption { get { return defaultOption; } }
        Type enumType;
        string defaultOption = "";

        public Dropdown(Operator op, OperatorInputOutput operatorInputOutput, string enumAQN, string defaultOption, Dictionary<string, string> options) : base(op, operatorInputOutput, ControlType.Dropdown)
        {
            enumType = Utilities.GetType(enumAQN);
            Options = options;
            this.defaultOption = defaultOption;
        }

        public override Control CreateCopy(Operator op, OperatorInputOutput inputOutput)
        {
            Dropdown dropdown = new Dropdown(op, inputOutput, enumType.AssemblyQualifiedName, defaultOption, Options);
            return dropdown;
        }

        public object ParseStringToEnum(string str)
        {
            return Enum.Parse(enumType, str);
        }
    }
}
