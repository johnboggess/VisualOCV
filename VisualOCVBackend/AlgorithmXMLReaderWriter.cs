﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using VisualOCVBackend.Operators;
using VisualOCVBackend.Types;
using VisualOCVBackend.Exceptions;

namespace VisualOCVBackend
{
    public class AlgorithmXMLReaderWriter
    {
        static AlgorithmManager Manager;
        public static List<Exception> Load(AlgorithmManager manager, string saveData)
        {
            Manager = manager;

            XDocument xDocument = XDocument.Parse(saveData);

            IEnumerable<XElement> versionXml = xDocument.Descendants(XName.Get("Version"));
            if (versionXml.Count() == 0)
            {
                XMLRequiredTagMissing _exception = new XMLRequiredTagMissing("Version");
                return new List<Exception>() { _exception };
            }

            int version = -1;
            int.TryParse(versionXml.First().Attribute(XName.Get("Version")).Value, out version);

            if (version == 1)
            {
                return LoadV1(xDocument);
            }

            FileVersionNotSupported exception = new FileVersionNotSupported(versionXml.First().Attribute(XName.Get("Version")).Value, "the loaded file");
            return new List<Exception>() { exception };
        }

        #region LoadV1
        private static List<Exception> LoadV1(XDocument xDocument)
        {
            List<LoadedOperator> loadedOps = new List<LoadedOperator>();
            List<XElement> ops = xDocument.Descendants("Operator").ToList();
            foreach(XElement op in ops)
                loadedOps.Add(Operator.Load(op, Manager));

            foreach (LoadedOperator op in loadedOps)
            {
                foreach(LoadedOperator.InputConnection connection in op.Inputs)
                {
                    Operator connectedTo = Manager.GetOperator(connection.ConnectedTo);
                    bool b = connectedTo.Connect(op.Operator.Inputs[connection.InputIndex], connectedTo.Outputs[connection.ConnectedToIndex]);
                }
            }
            return new List<Exception>();//todo: handle exceptions
        }
        #endregion
        
        public static string Save(AlgorithmManager manager)
        {
            XDocument xDocument = new XDocument();
            XElement root = new XElement("File");
            xDocument.Add(root);

            XElement version = new XElement("Version");
            version.SetAttributeValue("Version", AlgorithmManager.SaveVersion.ToString());
            root.Add(version);

            List<Operator> ops = manager.GetListOfOperators();
            foreach(Operator op in ops)
                root.Add(op.Save());

            return xDocument.ToString();
        }
       

        public class OperatorMacroData
        {
            public Operator Operator;
            public string OpMacroType;
            public float X;
            public float Y;
            public Guid Guid;
            public List<InputData> InputData;
            public bool IsOperator;
        }

        public class LoadedOperator
        {
            public Operator Operator;
            public List<InputConnection> Inputs;

            public class InputConnection
            {
                public int InputIndex;
                public Guid ConnectedTo;
                public int ConnectedToIndex;
            }
        }

        public class InputData
        {
            public int InputIndex;// = -1;
            public Guid ConnectedTo;// = Guid.Empty;
            public int ConnectedToIndex;
            public string Value;
            public bool IsConnectedToOutput { get { return ConnectedTo != Guid.Empty; } }
        }
    }
}
