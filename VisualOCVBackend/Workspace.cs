﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Drawing;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Reflection;

using Emgu.CV.Structure;
using Emgu.CV;

using VisualOCVBackend.GUIListeners;
using VisualOCVBackend.Types;
using VisualOCVBackend.Operators;
using Emgu.CV.ML;

[assembly: InternalsVisibleTo("NUnitTests")]
namespace VisualOCVBackend
{
    public class Workspace
    {
        public const string AlgorithmFileExtension = "xml";
        public const string MacroFileExtension = "xml";

        public AlgorithmManager AlgorithmManager;
        public GUIListeners.GUIListeners GUIListeners = new GUIListeners.GUIListeners();
        public List<Operator> SelectedOperators = new List<Operator>();

        static CopyData CopyData;
        
        public Workspace()
        {
            AlgorithmManager = new AlgorithmManager(true, this);
            CopyData = new CopyData(this);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exception = (Exception)e.ExceptionObject;
            VisualOCVBackend.Exceptions.VisualOCVException.SetIsFatal(exception, true);
            GUIListeners.ExceptionOccured(exception);
        }

        public void OpenWiki()
        {
            System.Diagnostics.Process.Start("https://gitlab.com/johnboggess/VisualOCV/-/wikis/home");
        }

        public void OpenGitHub()
        {
            System.Diagnostics.Process.Start("https://gitlab.com/johnboggess/VisualOCV");
        }

        public void Clear()
        {
            AlgorithmManager._ClearAlgorithm();
            SelectedOperators.Clear();
        }

        public void Move(float deltaX, float deltaY)
        {
            List<Operator> ops = AlgorithmManager.GetListOfOperators();

            foreach(Operator op in ops)
            {
                op.Move(-deltaX, -deltaY);
            }
        }

        #region Manage Ops

        #region NewInstance
        //todo: remove all of these NewInstance functions
        public Operator NewInstance(string operatorTypeID, Guid guid)
        {
            //todo: handle not found exception
            return NewInstance(OperatorModule.AllOperatorTypes[operatorTypeID], guid);
        }

        public Operator NewInstance(string operatorTypeID)
        {
            //todo: handle not found exception
            return NewInstance(OperatorModule.AllOperatorTypes[operatorTypeID], Guid.Empty);
        }
        
        public Operator NewInstance(OperatorType opType)
        {
            return NewInstance(opType, Guid.Empty);
        }

        public Operator NewInstance(OperatorType opType, Guid guid)
        {
            return AlgorithmManager.NewOperatorInstance(opType, guid);
        }
        #endregion

        #region Select/Unselect

        public bool IsSelected(Guid guid)
        {
            Operator op = AlgorithmManager.GetOperator(guid);
            return SelectedOperators.Contains(AlgorithmManager.GetOperator(guid));
        }

        public void Select(Guid guid)
        {
            Operator op = AlgorithmManager.GetOperator(guid);

            if (!SelectedOperators.Contains(op))
            {
                SelectedOperators.Add(op);
            }
        }

        public void Select(Guid guid, bool ctrlHeld, bool shiftHeld)
        {
            if(ctrlHeld)
            {
                if (IsSelected(guid))
                {
                    Unselect(guid);
                }
                else
                {
                    Select(guid);
                }
                return;
            }
            else if(shiftHeld)
            {
                Select(guid);
                return;
            }
            Operator op = AlgorithmManager.GetOperator(guid);
            SelectedOperators.Clear();
            SelectedOperators.Add(op);
        }

        public void Unselect(Guid guid)
        {
            Operator op = AlgorithmManager.GetOperator(guid);
            if (SelectedOperators.Contains(op))
            {
                SelectedOperators.Remove(op);
            }
        }

        public void ClearSelection()
        {
            SelectedOperators.Clear();
        }
        #endregion

        #endregion

        #region Copy/Paste/Cut/Delete
        public void Copy(float originX, float originY)
        {
            CopyData.CopyOperators(SelectedOperators, originX, originY);
        }

        public List<Operator> Paste(float newOriginX, float newOriginY)
        {
            return CopyData.Paste(newOriginX, newOriginY);
        }

        public void Cut(float originX, float originY)
        {
            Copy(originX, originY);
            Operator[] selectedOpsTemp = new Operator[SelectedOperators.Count];
            SelectedOperators.CopyTo(selectedOpsTemp);

            foreach(Operator op in selectedOpsTemp)
            {
                op.Delete();
            }
        }

        public void DeleteSelectedOperators()
        {
            while(SelectedOperators.Count > 0)
            {
                SelectedOperators[0].Delete();
            }
        }
        #endregion

        public string Save()
        {
            //todo: handle exceptions when saving a workspace
            return AlgorithmManager.Save();
        }

        public void Load(string saveData)
        {
            AlgorithmManager.Load(saveData);
        }

        public void StopAlgorithm()
        {
            AlgorithmManager.StopAlgorithm();
        }
    }
}
