﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

using VisualOCVBackend.Types;
using VisualOCVBackend.HelperFunctions;

using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

namespace VisualOCVBackend
{
    static class Utilities
    {
        public static Type GetType(string aqn, Type typeContainingGeneric = null)
        {
            if(typeContainingGeneric != null && typeContainingGeneric.IsGenericType)
            {
                foreach(Type genericType in typeContainingGeneric.GetGenericArguments())
                {
                    if(genericType.Name == aqn) { return genericType; }
                }
            }

            Type type = Type.GetType(aqn);

            if(type == null)
            {
                //https://stackoverflow.com/a/24654075
                type = Type.GetType(aqn,
                (aName) => AssemblyManager.GetAssemby(aName.FullName),
                (assem, name, ignore) => assem == null ?
                    Type.GetType(name, false, ignore) :
                    assem.GetType(name, false, ignore));
            }

            if(type == null)
            {
                VisualOCVType visualOCVType = Types.TypeManager.GetVisualOCVTypeFromName(aqn);
                if(visualOCVType != null) { type = visualOCVType.ActualType; }
            }
            return type;
        }

        public static MethodInfo GetMethod(Type declaringType, string name, Type[] _paramTypes, bool isGeneric)
        {
            if (!isGeneric) { return declaringType.GetMethod(name, _paramTypes); }
            IEnumerable<MethodInfo> methods = declaringType.GetMethods().Where(x => x.Name == name);
            foreach(MethodInfo method in methods)
            {
                ParameterInfo[] parameterInfos = method.GetParameters();
                for(int i = 0; i < parameterInfos.Length; i++)
                {
                    Type _paramType = parameterInfos[i].ParameterType;
                    if(_paramType.ContainsGenericParameters && _paramType.GenericTypeArguments.Length > 0)
                    {
                        if (_paramType.GetGenericTypeDefinition() != _paramTypes[i].GetGenericTypeDefinition()) { break; }//if(_paramType.GetGenericTypeDefinition() != _paramTypes[i]) { break; }
                    }
                    else if(_paramType != _paramTypes[i]) { break; }

                    if(i == parameterInfos.Length-1)
                    {
                        return method;
                    }
                }
            }
            return null;
        }

        /*public static ConstructorInfo GetConstructor(Type declaringType, Type[] _paramTypes, bool isGeneric)
        {
            if (!isGeneric) { return declaringType.GetConstructor(_paramTypes); }
            IEnumerable<ConstructorInfo> constructors = declaringType.GetConstructors();
            foreach (ConstructorInfo constructor in constructors)
            {
                ParameterInfo[] parameterInfos = constructor.GetParameters();
                for (int i = 0; i < parameterInfos.Length; i++)
                {
                    Type _paramType = parameterInfos[i].ParameterType;
                    if (_paramType.ContainsGenericParameters)
                    {
                        if (_paramType.GetGenericTypeDefinition() != _paramTypes[i]) { break; }
                    }
                    else if (_paramType != _paramTypes[i]) { break; }

                    if (i == parameterInfos.Length - 1)
                    {
                        return constructor;
                    }
                }
            }
            return null;
        }*/

        public static int GetIEnumerableLength(IEnumerable enumerable)
        {
            int i = 0;
            IEnumerator enumerator = enumerable.GetEnumerator();
            while (enumerator.MoveNext())
            {
                i++;
            }
            return i;
        }
        
        public static Bitmap ImageToBitmap(object image)
        {
            if(image == null) { return null; }
            Type[] genericTypes = image.GetType().GenericTypeArguments;
            MethodInfo methodInfo = typeof(Utilities).GetMethod(nameof(ToBitmap));
            return (Bitmap) methodInfo.MakeGenericMethod(genericTypes).Invoke(null, new object[] { image });
        }

        //https://github.com/emgucv/emgucv/blob/6ee487ad2709d1258cc014103deab2719b026303/Emgu.CV.NativeImage/BitmapExtension.cs
        public static Bitmap ToBitmap<TColor, TDepth>(this Image<TColor, TDepth> image) where TColor : struct, IColor where TDepth : new()
        {
            Type typeOfColor = typeof(TColor);
            Type typeofDepth = typeof(TDepth);

            PixelFormat format = PixelFormat.Undefined;

            if (typeOfColor == typeof(Gray))
            {
                format = PixelFormat.Format8bppIndexed;
            }
            else if (typeOfColor == typeof(Bgra))
            {
                format = PixelFormat.Format32bppArgb;
            }
            else if (typeOfColor == typeof(Bgr))
            {
                format = PixelFormat.Format24bppRgb;
            }
            else
            {
                using (Image<Bgr, Byte> temp = image.Convert<Bgr, Byte>())
                    return ToBitmap<Bgr, Byte>(temp);
            }

            if (typeof(TDepth) == typeof(Byte))
            {
                Size size = image.Size;
                Bitmap bmp = new Bitmap(size.Width, size.Height, format);
                BitmapData data = bmp.LockBits(new Rectangle(Point.Empty, size), ImageLockMode.WriteOnly, format);
                using (Mat mat = new Mat(size.Height, size.Width, DepthType.Cv8U, image.NumberOfChannels, data.Scan0, data.Stride))
                {
                    image.Mat.CopyTo(mat);
                }

                bmp.UnlockBits(data);
                return bmp;
            }
            else
            {
                using (Image<TColor, Byte> temp = image.Convert<TColor, Byte>())
                    return temp.ToBitmap();
            }
        }
        
        public static Type GenericTypeFillParameters(Type generic, Type typeToMatch)
        {
            if(generic.IsGenericType && typeToMatch.IsGenericType && generic.IsGenericTypeDefinition)
            {
                return generic.MakeGenericType(typeToMatch.GenericTypeArguments);
            }

            throw new Exception("Nongeneric type given");//todo: handle
        }

        public static bool DoesTypeImplementInterface(Type type, Type _interface)
        {
            if(_interface.IsInterface)
            {
                return type.GetInterface(_interface.Name) != null;
            }
            else { throw new Exception("Noninterface type given"); }
        }
        
        public static Type TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum depthEnum)
        {
            if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.SByte) { return typeof(sbyte); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Byte) { return typeof(byte); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Short) { return typeof(short); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.UShort) { return typeof(ushort); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Int) { return typeof(int); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.UInt) { return typeof(uint); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Long) { return typeof(long); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.ULong) { return typeof(ulong); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Float) { return typeof(float); }
            else if (depthEnum == VisualOCVOperatorFunctions.TDepthEnum.Decimal) { return typeof(decimal); }
            else { return typeof(double); }
        }

        public static Type TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum colorEnum)
        {
            if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Bgr)           { return typeof(Emgu.CV.Structure.Bgr); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Bgr565)   { return typeof(Emgu.CV.Structure.Bgr565); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Bgra)     { return typeof(Emgu.CV.Structure.Bgra); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Gray)     { return typeof(Emgu.CV.Structure.Gray); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Hls)      { return typeof(Emgu.CV.Structure.Hls); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Hsv)      { return typeof(Emgu.CV.Structure.Hsv); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Lab)      { return typeof(Emgu.CV.Structure.Lab); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Luv)      { return typeof(Emgu.CV.Structure.Luv); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Rgb)      { return typeof(Emgu.CV.Structure.Rgb); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Rgba)     { return typeof(Emgu.CV.Structure.Rgba); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Xyz)      { return typeof(Emgu.CV.Structure.Xyz); }
            else if (colorEnum == VisualOCVOperatorFunctions.TColorEnum.Ycc)      { return typeof(Emgu.CV.Structure.Ycc); }
            else { return typeof(Emgu.CV.Structure.Rgb); }
        }
    }
}
