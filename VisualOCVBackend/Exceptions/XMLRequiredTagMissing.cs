﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class XMLRequiredTagMissing : VisualOCVException
    {
        public XMLRequiredTagMissing(string tag)
        {
            Message = "Missing required tag <" + tag + "/>";//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// Operator defined in an operator XML file is missing a tag
        /// </summary>
        /// <param name="operatorID"></param>
        /// <param name="module"></param>
        /// <param name="tag"></param>
        public XMLRequiredTagMissing(string operatorID, Operators.OperatorModule module, string tag)
        {
            Message = "No <"+tag+"/> tag given for operator " + operatorID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// Type to be managed is missing a tag
        /// </summary>
        /// <param name="AQN_ID">The ID or Assembly Qualified Name of the type</param>
        /// <param name="tag">The tag missing</param>
        public XMLRequiredTagMissing(string AQN_ID, string tag)
        {
            Message = "No <" + tag + "/> tag given for type with ID/Assembly Qualified Name" + AQN_ID;//todo: when there is documentation put a link to page documenting this
        }
    }
}
