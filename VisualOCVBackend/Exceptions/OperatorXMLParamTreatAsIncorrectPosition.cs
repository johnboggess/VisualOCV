﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class OperatorXMLParamTreatAsIncorrectPosition : VisualOCVException
    {
        public OperatorXMLParamTreatAsIncorrectPosition(string ID, Operators.OperatorModule module)
        {
            Message = "TreatAs attribute given before any types have been given for operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }
    }
}
