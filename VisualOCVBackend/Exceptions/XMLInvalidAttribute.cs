﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Exceptions
{
    class XMLInvalidAttribute : VisualOCVException
    {
        public XMLInvalidAttribute(string attribute, string tag, string value)
        {
            Message = "Invalid attribute \"" + attribute + " = " + value + "\" in tag <" + tag + "/>";//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// A type to be managed has an invalid attribute value
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <param name="tag"></param>
        public XMLInvalidAttribute(Type type, string attribute, string tag)
        {
            Message = "Invalid attribute \"" + attribute + "\" in tag <" + tag + "/> for type with assembly qualified name" + type.AssemblyQualifiedName;//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// A type could not be found
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attribute"></param>
        /// <param name="tag"></param>
        public XMLInvalidAttribute(string typeID)
        {
            Message = "Unable to load type " + typeID;//todo: when there is documentation put a link to page documenting this
        }
    }
}
