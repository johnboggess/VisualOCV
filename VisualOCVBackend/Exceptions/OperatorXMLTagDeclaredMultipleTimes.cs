﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class OperatorXMLTagDeclaredMultipleTimes : VisualOCVException
    {
        public OperatorXMLTagDeclaredMultipleTimes(string ID, Operators.OperatorModule module, string tag)
        {
            Message = "Tag <"+tag+"/> declared more than once for operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }
    }
}
