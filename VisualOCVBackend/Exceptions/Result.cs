﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Exceptions
{
    public class Result<T>
    {
        public VisualOCVException Exception = null;
        public T Value;
        public bool HasExceptionOccured { get { return this.Exception != null; } }

        public Result(T value, VisualOCVException exception)
        {
            Value = value;
            this.Exception = exception;
        }
    }
}
