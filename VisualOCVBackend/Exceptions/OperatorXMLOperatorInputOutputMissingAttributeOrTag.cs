﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class OperatorXMLOperatorInputOutputMissingAttributeOrTag : VisualOCVException
    {
        public OperatorXMLOperatorInputOutputMissingAttributeOrTag(string ID, Operators.OperatorModule module, string tagAttribute, int inputIndex, bool isInput)
        {
            Message = "Tag/Attribute " + tagAttribute + " missing for " + (isInput? "input "+ inputIndex : "output") + " of operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }
    }
}
