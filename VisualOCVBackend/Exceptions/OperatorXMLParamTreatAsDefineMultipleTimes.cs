﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class OperatorXMLParamTreatAsDefineMultipleTimes : VisualOCVException
    {
        public OperatorXMLParamTreatAsDefineMultipleTimes(string ID, Operators.OperatorModule module)
        {
            Message = "TreatAs has been defined multiple time for the same parameter type in operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }
    }
}
