﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class OperatorXMLInvalidType : VisualOCVException
    {
        public OperatorXMLInvalidType(string ID, Operators.OperatorModule module, string type)
        {
            Message = "Invalid type given : " + type + " (Operator " + ID + " in module " + module.FullName + ")";//todo: when there is documentation put a link to page documenting this
        }

        public OperatorXMLInvalidType(string ID, Operators.OperatorModule module, string type, int inputIndex)
        {
            Message = "Invalid type for input " + inputIndex + " given : " + type + " (Operator " + ID + " in module " + module.FullName + ")";//todo: when there is documentation put a link to page documenting this
        }
    }
}
