﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Exceptions
{
    public class VisualOCVException : Exception
    {
        public new string Message = "";
        public new Exception InnerException = null;
        public bool Fatal
        {
            get
            {
                return IsFatal(this);
            }

            set
            {
                SetIsFatal(this, value);
            }
        }

        public static bool IsFatal(Exception exception)
        {
            if(!exception.Data.Contains("IsFatal"))
            {
                exception.Data.Add("IsFatal", false);
            }
            return (bool)exception.Data["IsFatal"];
        }

        public static void SetIsFatal(Exception exception, bool fatal)
        {
            if (!exception.Data.Contains("IsFatal"))
            {
                exception.Data.Add("IsFatal", fatal);
                return;
            }
            exception.Data["IsFatal"] = fatal;
        }
    }
}
