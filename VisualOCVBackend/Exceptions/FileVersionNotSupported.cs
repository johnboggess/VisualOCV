﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Exceptions
{
    public class FileVersionNotSupported : VisualOCVException
    {
        public FileVersionNotSupported(string fileVersion, string moduleFileName)
        {
            Message = "Unsupported file version " + fileVersion + "for " + moduleFileName;
        }
    }
}
