﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend;
namespace VisualOCVBackend.Exceptions
{
    class XMLRequiredAttributeMissing : VisualOCVException
    {
        public XMLRequiredAttributeMissing(string attribute, string tag)
        {
            Message = "No " + attribute + " attribute in tag <" + tag + "/>";//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// Operator defined in an operator XML file is missing an attribute
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="module"></param>
        /// <param name="attribute"></param>
        /// <param name="tag"></param>
        public XMLRequiredAttributeMissing(string ID, Operators.OperatorModule module, string attribute, string  tag)
        {
            Message = "No " + attribute + " attribute in tag <" + tag + "/> given for operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// Input of an operator defined in an operator XML file is missing an attribute
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="module"></param>
        /// <param name="attribute"></param>
        /// <param name="tag"></param>
        /// <param name="inputIndex"></param>
        public XMLRequiredAttributeMissing(string ID, Operators.OperatorModule module, string attribute, string tag, int inputIndex)
        {
            Message = "No " + attribute + " attribute in tag <" + tag + "/> given for input " + inputIndex + " of operator " + ID + " in module " + module.FullName;//todo: when there is documentation put a link to page documenting this
        }

        /// <summary>
        /// Tag of a type to be managed is missing an attribute
        /// </summary>
        /// <param name="intanceID"></param>
        /// <param name="attribute"></param>
        /// <param name="tag"></param>
        public XMLRequiredAttributeMissing(Type type, string attribute, string tag)
        {
            Message = "No " + attribute + " attribute in tag <" + tag + "/> given for the type " + type.AssemblyQualifiedName;//todo: when there is documentation put a link to page documenting this
        }
    }
}
