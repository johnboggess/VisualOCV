﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace VisualOCVBackend.Types
{
    public class VisualOCVType
    {
        public Type ActualType
        {
            get { return _actualType; }
        }

        public string AssemblyQualifiedName
        {
            get { return _actualType.AssemblyQualifiedName; }
        }

        public bool IsClonable { get { return _cloneMethod!= null; } }
        public bool IsSavable { get { return _saveMethod != null; } }
        public bool IsLoadable { get { return _loadMethod != null; } }

        Type _actualType;
        private MethodInfo _saveMethod;
        private MethodInfo _loadMethod;
        private MethodInfo _cloneMethod;
        private Dictionary<Type, MethodInfo> _converters = new Dictionary<Type, MethodInfo>();

        public VisualOCVType(Type type, MethodInfo saveMethod, MethodInfo loadMethod, MethodInfo cloneMethod)
        {
            _actualType = type;
            _saveMethod = saveMethod;
            _loadMethod = loadMethod;
            _cloneMethod = cloneMethod;
        }

        public string Save(object instanceOfType)
        {
            if(instanceOfType.GetType() != _actualType)
            {
                return "";
            }
            
            MethodInfo methodInfo = _saveMethod;
            if (methodInfo.IsGenericMethodDefinition)
            {
                methodInfo = methodInfo.MakeGenericMethod(instanceOfType.GetType().GenericTypeArguments);
            }
            return (string)methodInfo.Invoke(null, new object[] { instanceOfType });

            //return (string)_saveMethod.Invoke(null, new object[] { instanceOfType });
        }

        public object Load(string saveData)
        {
            return (object)_loadMethod.Invoke(null, new object[] { saveData });
        }

        public object Clone(object instance)
        {
            MethodInfo methodInfo = _cloneMethod;
            if (methodInfo.IsGenericMethodDefinition)
            {
                methodInfo = methodInfo.MakeGenericMethod(instance.GetType().GenericTypeArguments);
            }
            return methodInfo.Invoke(null, new object[] { instance });

            //return _cloneMethod.Invoke(null, new object[] { instance });
        }

        public object TryConvertTo(object obj, Type convertTo)
        {
            if(_converters.ContainsKey(convertTo))
            {
                MethodInfo methodInfo = _converters[convertTo];
                if(methodInfo.IsGenericMethodDefinition)
                {
                    methodInfo = methodInfo.MakeGenericMethod(obj.GetType().GenericTypeArguments);
                }
                return methodInfo.Invoke(null, new object[] { obj });
            }
            return null;
        }

        public void AddConverters(Dictionary<Type, MethodInfo> converters)
        {
            if(converters == null) { return; }
            foreach(KeyValuePair<Type, MethodInfo> keyValue in converters)
            {
                if (_converters.ContainsKey(keyValue.Key))
                    _converters.Remove(keyValue.Key);
                _converters.Add(keyValue.Key, keyValue.Value);
            }
        }
    }
}
