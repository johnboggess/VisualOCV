﻿using Emgu.CV;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Types.Helpers
{
    class EMGUCVMatrixHelper
    {
        public static Matrix<TDepth> Clone<TDepth>(Matrix<TDepth> matrix) where TDepth : new()
        {
            return matrix.Clone();
        }

        public static Mat ConvertToMat<TDepth>(Matrix<TDepth> matrix) where TDepth : new()
        {
            return matrix.Mat;
        }
    }

    class EMGUCVVectorOfVectorOfPoint
    {
        public static VectorOfVectorOfPoint Clone(VectorOfVectorOfPoint vvp)
        {
            System.Drawing.Point[][] pts = vvp.ToArrayOfArray();
            return new VectorOfVectorOfPoint(pts);
        }
    }
}
