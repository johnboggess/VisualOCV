﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Types
{
    public class TypeLoadSaveMethods
    {
        #region bool
        public static string SaveBool(object instanceOfType)
        {
            return "" + ((bool)instanceOfType).ToString();
        }

        public static object LoadBool(string saveData)
        {
            return bool.Parse(saveData);
        }
        #endregion

        #region byte
        public static string SaveByte(object instanceOfType)
        {
            return "" + ((byte)instanceOfType).ToString();
        }

        public static object LoadByte(string saveData)
        {
            return byte.Parse(saveData);
        }
        #endregion

        #region sbyte
        public static string SaveSByte(object instanceOfType)
        {
            return "" + ((sbyte)instanceOfType).ToString();
        }

        public static object LoadSByte(string saveData)
        {
            return sbyte.Parse(saveData);
        }
        #endregion

        #region char
        public static string SaveChar(object instanceOfType)
        {
            return "" + ((char)instanceOfType).ToString();
        }

        public static object LoadChar(string saveData)
        {
            return char.Parse(saveData);
        }
        #endregion

        #region decimal
        public static string SaveDecimal(object instanceOfType)
        {
            return "" + ((decimal)instanceOfType).ToString();
        }

        public static object LoadDecimal(string saveData)
        {
            return decimal.Parse(saveData);
        }
        #endregion

        #region double
        public static string SaveDouble(object instanceOfType)
        {
            return "" + ((double)instanceOfType).ToString("G17");
        }

        public static object LoadDouble(string saveData)
        {
            return double.Parse(saveData);
        }
        #endregion

        #region float
        public static string SaveFloat(object instanceOfType)
        {
            return "" + ((float)instanceOfType).ToString("G9");
        }

        public static object LoadFloat(string saveData)
        {
            return float.Parse(saveData);
        }
        #endregion

        #region int
        public static string SaveInt(object instanceOfType)
        {
            return "" + (int)instanceOfType;
        }

        public static object LoadInt(string saveData)
        {
            return int.Parse(saveData);
        }
        #endregion

        #region uint
        public static string SaveUInt(object instanceOfType)
        {
            return "" + ((uint)instanceOfType).ToString();
        }

        public static object LoadUInt(string saveData)
        {
            return uint.Parse(saveData);
        }
        #endregion

        #region long
        public static string SaveLong(object instanceOfType)
        {
            return "" + ((long)instanceOfType).ToString();
        }

        public static object LoadLong(string saveData)
        {
            return long.Parse(saveData);
        }
        #endregion

        #region ulong
        public static string SaveULong(object instanceOfType)
        {
            return "" + ((ulong)instanceOfType).ToString();
        }

        public static object LoadULong(string saveData)
        {
            return ulong.Parse(saveData);
        }
        #endregion

        #region short
        public static string SaveShort(object instanceOfType)
        {
            return ((short)instanceOfType).ToString();
        }

        public static object LoadShort(string saveData)
        {
            return short.Parse(saveData);
        }
        #endregion

        #region ushort
        public static string SaveUShort(object instanceOfType)
        {
            return ((ushort)instanceOfType).ToString();
        }

        public static object LoadUShort(string saveData)
        {
            return ushort.Parse(saveData);
        }

        #endregion

        #region string
        public static string SaveString(object instanceOfType)
        {
            return ((string)instanceOfType);
        }

        public static object LoadString(string saveData)
        {
            return saveData;
        }
        
        public static object CloneString(object str)
        {
            //A copy of an output is made when passed to an input if the output is a refernce type
            //it is copied using a clone method specified for that object or using MemberwiseClone
            //string is considered a refernce type but cannot be cloned with MemberwiseClone (corrupts string) so a clone method must be provided
            //while string is considered a reference type it is immutable, and thus behaves like a value type when used (a new instance of string is create when modified)
            //A clone method needs to be specified for string but strings dont actually need to be cloned (thus this method simply passes out the string)
            return str;
        }
        #endregion
    }
}
