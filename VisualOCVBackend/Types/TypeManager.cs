﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Runtime.CompilerServices;

using VisualOCVBackend.Exceptions;

namespace VisualOCVBackend.Types
{
    public class TypeManager
    {
        public static string TypeLocation = AppDomain.CurrentDomain.BaseDirectory + "Types/";
        public static Workspace Workspace;

        static Dictionary<string, VisualOCVType> Types = new Dictionary<string, VisualOCVType>();
        static Dictionary<Type, VisualOCVType> ManagedTypes = new Dictionary<Type, VisualOCVType>();


        public TypeManager(Workspace workspace)
        {
            Workspace = workspace;
        }

        /// <summary>
        /// Retrive a managed type by alias.
        /// </summary>
        /// <param name="alias">The alias of the type</param>
        /// <returns>The VisualOCVType instance of the managed type, or null if it cannot be found.</returns>
        public static VisualOCVType GetVisualOCVTypeFromName(string alias)
        {
            if (Types.ContainsKey(alias))
            {
                return Types[alias];
            }
            
            return null;
        }

        /// <summary>
        /// Retrive a managed type by its type. If the type if generic and it cannot find a managed type with the given generic paramters it will instead attempt to get the generic type definition.
        /// </summary>
        /// <param name="type">The type of the managed type to retrive</param>
        /// <returns>The VisualOCVType instance of the managed type, or null if it cannot be found.</returns>
        public static VisualOCVType GetVisualOCVTypeFromType(Type type)
        {
            if (ManagedTypes.ContainsKey(type))
            {
                return ManagedTypes[type];
            }
            else if(type.IsGenericType && ManagedTypes.ContainsKey(type.GetGenericTypeDefinition()))
            {
                return ManagedTypes[type.GetGenericTypeDefinition()];
            }
            return null;
        }

        public static void ManageType(Type type, string name, MethodInfo saveMethod, MethodInfo loadMethod, MethodInfo cloneMethod, Dictionary<Type, MethodInfo> converters)
        {
            ManageType(type, new List<string> { name }, saveMethod, loadMethod, cloneMethod, converters);
        }

        public static void ManageType(Type type, List<string> names, MethodInfo saveMethod, MethodInfo loadMethod, MethodInfo cloneMethod, Dictionary<Type, MethodInfo> converters)
        {
            bool alreadyManaged = ManagedTypes.ContainsKey(type);
            VisualOCVType visualOCVType;

            if(alreadyManaged)
            {
                visualOCVType = ManagedTypes[type];
            }
            else
            {
                visualOCVType = new VisualOCVType(type, saveMethod, loadMethod, cloneMethod);
                ManagedTypes.Add(type, visualOCVType);
            }
            
            if(!Types.ContainsKey(type.AssemblyQualifiedName))
            {
                Types.Add(type.AssemblyQualifiedName, visualOCVType);
            }

            visualOCVType.AddConverters(converters);

            foreach(string name in names)
            {
                if (Types.ContainsKey(name))
                {
                    string errorMessage = "Unable to use type ID " + name + " for " + type.FullName +": Type ID " + name + " already used by "+ Types[name] +".";
                    Exception e = new Exception(errorMessage);
                    Workspace.GUIListeners.ExceptionOccured(e);
                    continue;
                }
                Types.Add(name, visualOCVType);
            }
        }

        public static object TryConvertTo(object obj, Type _convertTo)
        {
            VisualOCVType type = GetVisualOCVTypeFromType(obj.GetType());
            if (type != null)
            {
                return type.TryConvertTo(obj, _convertTo);
            }
            return null;
        }

        #region Load Files
        public static void LoadAllTypes()
        {
            List<Exception> exceptions = new List<Exception>();

            DirectoryInfo directoryInfo = new DirectoryInfo(TypeLocation);

            FileInfo[] xmls = directoryInfo.GetFiles();
            foreach (FileInfo file in xmls)
            {
                if (file.Extension == ".xml")
                {
                    exceptions.AddRange(LoadXml(file));
                }
            }

            if(exceptions.Count > 0)
            {
                Workspace.GUIListeners.ExceptionOccured(exceptions);
            }
        }

        public static List<Exception> LoadXml(FileInfo file)
        {
            FileStream fileStream = File.OpenRead(file.FullName);
            XDocument xDocument = XDocument.Load(file.FullName);

            int version = -1;
            List<XElement> XMLVerions =  xDocument.Descendants(XName.Get("Version")).ToList();

            if(XMLVerions.Count > 0)
            {
                if (!XMLVerions[0].HasAttributes)
                    throw new Exception("Missing Version"); //todo:missing version
                if (!int.TryParse(XMLVerions[0].FirstAttribute.Value, out version))
                    throw new Exception("Invalid Version"); //todo:invalid version
            }
            
            if (version == 1) { return ReadV1(xDocument); }
            return new List<Exception>();
        }

        public static List<Exception> ReadV1(XDocument xDocument)
        {
            List<Exception> result = new List<Exception>();

            LoadDllsV1(xDocument);

            List<XElement> types = xDocument.Descendants(XName.Get("Type")).ToList();
            foreach (XElement xmlType in types)
            {
                Result<int> parseTypeResult = ParseType(xmlType);
                if(parseTypeResult.HasExceptionOccured) { result.Add(parseTypeResult.Exception); }
            }
            return result;
        }

        public static Result<int> ParseType(XElement XMLType)
        {
            List<string> ids = new List<string>();
            Type type = null;
            MethodInfo saveMethod = null;
            MethodInfo loadMethod = null;
            MethodInfo cloneMethod = null;
            Dictionary<Type, MethodInfo> converters = new Dictionary<Type, MethodInfo>();


            #region Parse ID
            List<XElement> IDs = XMLType.Descendants(XName.Get("ID")).ToList();
            if (IDs.Count == 0)
                return new Result<int>(0, new XMLRequiredTagMissing("ID"));

            List<XAttribute> IDattribs = IDs[0].Attributes().ToList();
            for(int i = 0; i < IDattribs.Count; i++)
                ids.Add(IDattribs[i].Value);

            if (ids.Count == 0)
                return new Result<int>(0, new XMLRequiredAttributeMissing("id", "ID"));
            #endregion


            #region Parse Assembly Qualified Name
            List<XElement> XMLaqn = XMLType.Descendants(XName.Get("AssemblyQualifiedName")).ToList();
            if (IDs.Count == 0)
                return new Result<int>(0, new XMLRequiredTagMissing(ids[0], "AssemblyQualifiedName"));

            List<XAttribute> AQNattribs = XMLaqn[0].Attributes().ToList();
            for (int i = 0; i < AQNattribs.Count; i++)
            {
                Type x = Utilities.GetType(AQNattribs[i].Value);
                if(x != null)
                {
                    type = x;
                    break;
                }
            }
            if (type == null)
                return new Result<int>(0, new XMLInvalidAttribute(ids[0]));
            #endregion


            #region Parse Helper
            List<XElement> XMLhelper = XMLType.Descendants(XName.Get("HelperClass")).ToList();
            if(XMLhelper.Count > 0)
            {
                List<XAttribute> helperAttribs = XMLhelper[0].Attributes().ToList();
                if (helperAttribs.Count == 0)
                    return new Result<int>(0, new XMLRequiredAttributeMissing(type, "HelperAQN", "HelperClass"));
                Type helper = Utilities.GetType(helperAttribs[0].Value);
                if (helper == null)
                    return new Result<int>(0, new XMLInvalidAttribute(type, helperAttribs[0].Name.LocalName, helperAttribs[0].Value));
                loadMethod = helper.GetMethod("Load");
                saveMethod = helper.GetMethod("Save");
                cloneMethod = helper.GetMethod("Clone");

                List<XElement> XMLconverters = XMLType.Descendants(XName.Get("Converter")).ToList();
                foreach(XElement converter in XMLconverters)
                {
                    List<XAttribute> converterAttribs = converter.Attributes().ToList();
                    if (helperAttribs.Count == 0)
                        return new Result<int>(0, new XMLRequiredAttributeMissing(type, "Name", "Converter"));

                    foreach (XAttribute converterAttrib in converterAttribs)
                    {
                        MethodInfo method = helper.GetMethod(converterAttrib.Value);
                        if (method == null)
                            new Result<int>(0, new XMLInvalidAttribute(type, converterAttrib.Name.LocalName, converter.Name.LocalName));
                        Type convertsTo = method.ReturnType;
                        if (!converters.ContainsKey(convertsTo))
                            converters.Add(convertsTo, method);
                    }
                }
            }
            #endregion


            #region Parse Save Method
            List<XElement> XMLSave = XMLType.Descendants(XName.Get("SaveMethod")).ToList();
            if (XMLSave.Count > 0)
            {
                List<XAttribute> saveAttribs = XMLSave[0].Attributes().ToList();
                if (saveAttribs.Count < 2)
                    new Result<int>(0, new XMLRequiredAttributeMissing(type, "ContainingTypeID/Name", "SaveMethod"));
                Type containingType = Utilities.GetType(saveAttribs[0].Value);
                string methodName = saveAttribs[1].Value;

                if (containingType == null)
                    return new Result<int>(0, new XMLInvalidAttribute(type, saveAttribs[0].Name.LocalName, "SaveMethod"));

                saveMethod = Utilities.GetMethod(containingType, methodName, new Type[] { type }, false);
                if (saveMethod == null)
                    return new Result<int>(0, new XMLInvalidAttribute(type, saveAttribs[0].Name.LocalName+"/"+ saveAttribs[1].Name.LocalName, "SaveMethod"));
            }
            #endregion


            #region Parse Load Method
            List<XElement> XMLLoad = XMLType.Descendants(XName.Get("LoadMethod")).ToList();
            if (XMLLoad.Count > 0)
            {
                List<XAttribute> loadAttribs = XMLLoad[0].Attributes().ToList();
                if (loadAttribs.Count < 2)
                    new Result<int>(0, new XMLRequiredAttributeMissing(type, "ContainingTypeID/Name", "LoadMethod"));
                Type containingType = Utilities.GetType(loadAttribs[0].Value);
                string methodName = loadAttribs[1].Value;

                if (containingType == null)
                    new Result<int>(0, new XMLInvalidAttribute(type, loadAttribs[0].Name.LocalName, "LoadMethod"));

                loadMethod = Utilities.GetMethod(containingType, methodName, new Type[] { typeof(string) }, false);
                if (loadMethod == null)
                    new Result<int>(0, new XMLInvalidAttribute(type, loadAttribs[0].Name.LocalName + "/" + loadAttribs[1].Name.LocalName, "loadMethod"));
            }
            #endregion


            #region Parse Clone Method
            List<XElement> XMLClone = XMLType.Descendants(XName.Get("CloneMethod")).ToList();
            if (XMLClone.Count > 0)
            {
                List<XAttribute> cloneAttribs = XMLClone[0].Attributes().ToList();
                if (cloneAttribs.Count < 2)
                    new Result<int>(0, new XMLRequiredAttributeMissing(type, "ContainingTypeID/Name", "CloneMethod"));
                Type containingType = Utilities.GetType(cloneAttribs[0].Value);
                string methodName = cloneAttribs[1].Value;

                if (containingType == null)
                    return new Result<int>(0, new XMLInvalidAttribute(type, cloneAttribs[0].Name.LocalName, "CloneMethod"));

                cloneMethod = Utilities.GetMethod(containingType, methodName, new Type[] { typeof(object) }, false);
                if (cloneMethod == null)
                    return new Result<int>(0, new XMLInvalidAttribute(type, cloneAttribs[0].Name.LocalName + "/" + cloneAttribs[1].Name.LocalName, "CloneMethod"));
            }
            #endregion

            ManageType(type, ids, saveMethod, loadMethod, cloneMethod, converters);
            return new Result<int>(0, null);
        }

        private static void LoadDllsV1(XDocument xDocument)
        {
            IEnumerable<XElement> dlls = xDocument.Descendants(XName.Get("Dlls"));
            IEnumerable<XElement> XMLDll = dlls.Descendants(XName.Get("Dll"));

            foreach (XElement dll in XMLDll)
            {
                if (dll.Attribute(XName.Get("path")) != null)//todo: test if invalid path is given
                {
                    AssemblyManager.LoadAssembly(dll.Attribute(XName.Get("path")).Value, true);
                }
            }
        }

        #endregion
    }
}
