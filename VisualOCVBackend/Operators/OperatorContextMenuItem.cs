﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
namespace VisualOCVBackend.Operators
{
    public class OperatorContextMenuItem
    {
        public MethodInfo MethodInfo;
        public string Name;
        public OperatorType OperatorType; 

        public OperatorContextMenuItem(OperatorType operatorType, string name, MethodInfo methodInfo)
        {
            OperatorType = operatorType;
            Name = name;
            MethodInfo = methodInfo;
        }

        public void Clicked()
        {
            MethodInfo.Invoke(null, new object[] { this });
        }
    }
}
