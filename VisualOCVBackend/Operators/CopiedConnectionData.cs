﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Operators
{
    class CopiedConnectionData
    {
        public Guid ConnectedToID;
        public int TargetIndex;
        public int SourceIndex;

        public CopiedConnectionData(Guid ConnectedToID, int targetIndex, int sourceIndex)
        {
            this.ConnectedToID = ConnectedToID;
            TargetIndex = targetIndex;
            SourceIndex = sourceIndex;
        }
    }
}
