﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Linq;

using VisualOCVBackend.Types;
using System.Runtime.CompilerServices;

namespace VisualOCVBackend.Operators
{
    public class Operator
    {
        protected bool _kill = false;
        private int executing = 0;
        internal AlgorithmManager Manager;
        internal bool ShouldReprocess = true;
        protected Guid instanceID;
        public Guid InstanceID
        {
            get { return instanceID; }
        }
        
        public OperatorType OperatorType;

        public float X = 0;
        public float Y = 0;


        public float Width
        {
            get
            {
                return (this is Macro) ? ((Macro)this).MacroType.Width : OperatorType.Width;
            }
        }
        public float Height = 0;

        public RectangleF GetRectangle()
        {
            return new RectangleF(X, Y, Width, Height);
        }

        public string Name;

        public OperatorMethod Method = null;
        public ConstructorInfo Constructor = null;
        
        public List<OperatorInput> Inputs = new List<OperatorInput>();
        public List<OperatorOutput> Outputs = new List<OperatorOutput>();
        public Mutex DataMutex = new Mutex();

        public bool IsRootOperator { get { return Manager.IsRootOperator(this); } }
        public bool IsLeafOperator { get { return Manager.IsLeafOperator(this); } }
        public List<OperatorContextMenuItem> ContextMenuItems
        {
            get { return OperatorType.ContextMenuItems; }
        }
        
        internal Thread _processThread;
        protected EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

        public bool Error = false;
        public OperatorError OperatorError = null;
        
        public bool IsStaticMethod
        {
            get
            {
                if (IsMethod) { return Method.IsStatic; }
                return false;
            }
        }

        public bool IsMethod
        {
            get { return Method != null; }
        }

        public bool IsConstructor
        {
            get { return (Constructor != null); }
        }

        public Operator(AlgorithmManager manager, string name, OperatorType operatorType, OperatorMethod method, List<OperatorInput> inputs, List<OperatorOutput> outputs, Guid id)
        {
            Manager = manager;
            Name = name;
            Method = method;
            Inputs = inputs;
            Outputs = outputs;
            _processThread = new Thread(Exectue);
            _processThread.Start();

            OperatorType = operatorType;

            if (id == Guid.Empty || Manager.DoesOperatorExist(id))
            {
                instanceID = Guid.NewGuid();
            }
            else
            {
                instanceID = id;
            }
        }

        public Operator(AlgorithmManager manager, string name, OperatorType operatorType, ConstructorInfo constructorInfo, List<OperatorInput> inputs, List<OperatorOutput> outputs, Guid id)
        {
            Manager = manager;
            Name = name;
            Constructor = constructorInfo;
            Inputs = inputs;
            Outputs = outputs;
            _processThread = new Thread(Exectue);
            _processThread.Start();

            OperatorType = operatorType;

            if (id == Guid.Empty || Manager.DoesOperatorExist(id))
            {
                instanceID = Guid.NewGuid();
            }
            else
            {
                instanceID = id;
            }
        }

        /// <summary>
        /// Used only for marcos.
        /// </summary>
        internal Operator()
        {
        }
        
        public virtual bool Resizable()
        {
            return OperatorType.Resizable;
        }

        public void Move(float deltaX, float deltaY)
        {
            float oldX = X;
            float oldY = Y;
            X += deltaX;
            Y += deltaY;

            if(Manager.CallCallbacks)
            {
                Manager.Workspace.GUIListeners.OperatorMoved(this, X, Y, oldX, oldY);
            }
        }

        public void SetPosition(float x, float y)
        {
            float oldX = x;
            float oldY = y;

            X = x;
            Y = y;

            if (Manager.CallCallbacks)
            {
                Manager.Workspace.GUIListeners.OperatorMoved(this, X, Y, oldX, oldY);
            }
        }

        public void Delete()
        {
            Manager.DeleteOperator(this.InstanceID);
        }

        public XElement Save()
        {
            XElement op = new XElement("Operator");
            op.SetAttributeValue("Type", OperatorType.ID);
            op.SetAttributeValue("ID", InstanceID.ToString());
            op.SetAttributeValue("X", X.ToString("G9"));
            op.SetAttributeValue("Y", Y.ToString("G9"));

            XElement inputs = new XElement("Inputs");
            for(int i = 0; i < Inputs.Count; i++)
            {
                OperatorInput input = Inputs[i];
                XElement XMLinput = new XElement("Input");
                XMLinput.SetAttributeValue("Index", i);
                if(input.Connection != null)
                {
                    XMLinput.SetAttributeValue("ConnectedToID", input.Connection.Operator.InstanceID);
                    XMLinput.SetAttributeValue("ConnectedToIndex", input.Connection.Index);
                    inputs.Add(XMLinput);
                }
                else if(input.Value != null && input.Value.GetType().BaseType == typeof(Enum))
                {
                    XMLinput.SetAttributeValue("Value", input.Value.ToString());
                    inputs.Add(XMLinput);
                }
                else if(input.Value != null && TypeManager.GetVisualOCVTypeFromType(input.Value.GetType()) != null)
                {
                    XMLinput.SetAttributeValue("Value", TypeManager.GetVisualOCVTypeFromType(input.Value.GetType()).Save(input.Value));
                    inputs.Add(XMLinput);
                }
            }

            op.Add(inputs);
            return op;
        }

        public static AlgorithmXMLReaderWriter.LoadedOperator Load(XElement xElement, AlgorithmManager algorithmManager)
        {
            List<AlgorithmXMLReaderWriter.LoadedOperator.InputConnection> connectionList = new List<AlgorithmXMLReaderWriter.LoadedOperator.InputConnection>();
            List<XAttribute> attributes = xElement.Attributes().ToList();
            string operatorType = attributes[0].Value;
            string ID = attributes[1].Value;
            float x = float.Parse(attributes[2].Value);
            float y = float.Parse(attributes[3].Value);

            Operator op = algorithmManager.NewOperatorInstance(OperatorModule.AllOperatorTypes[operatorType], Guid.Parse(ID));
            op.SetPosition(x, y);

            List<XElement> inputs = xElement.Descendants("Inputs").ElementAt(0).Descendants("Input").ToList();

            for(int i = 0; i < inputs.Count; i++)
            {
                XElement input = inputs[i];
                List<XAttribute> inputAttributes = input.Attributes().ToList();
                int index = int.Parse(inputAttributes[0].Value);
                
                if(inputAttributes[1].Name.LocalName == "Value")
                {
                    if (op.Inputs[index]._valueType.BaseType == typeof(Enum))
                    {
                        op.Inputs[index].Value = Enum.Parse(op.Inputs[index]._valueType, inputAttributes[1].Value);
                    }
                    else
                    {
                        VisualOCVType visualOCVType = TypeManager.GetVisualOCVTypeFromType(op.Inputs[index].GetValueType(false));
                        if (visualOCVType == null || !visualOCVType.IsLoadable) { continue; }
                        op.Inputs[index].Value = visualOCVType.Load(inputAttributes[1].Value);
                    }
                    if (algorithmManager.CallCallbacks)
                    {
                        algorithmManager.Workspace.GUIListeners.ControlValueChanged(op.Inputs[index].Control, op.Inputs[index].Value);
                    }
                }
                else
                {
                    Guid connectedTo = Guid.Parse(inputAttributes[1].Value);
                    int connectedToIndex = int.Parse(inputAttributes[2].Value);
                    connectionList.Add( new AlgorithmXMLReaderWriter.LoadedOperator.InputConnection { InputIndex = index, ConnectedTo = connectedTo, ConnectedToIndex = connectedToIndex } );
                }
            }

            return new AlgorithmXMLReaderWriter.LoadedOperator { Operator = op, Inputs = connectionList };
        }

        public static AlgorithmXMLReaderWriter.LoadedOperator Load(string data, AlgorithmManager algorithmManager)
        {
            return Load(XElement.Parse(data), algorithmManager);
        }
        
        #region Connect/Disconnect
        public bool Connect(OperatorInput target, OperatorOutput source)
        {
            return target.Connect(source);
        }

        public void Disconnect(OperatorInput target, OperatorOutput source)
        {
            source.RemoveConnection(target);
        }

        public void DisconnectFromAll()
        {
            Manager.StopAlgorithm();

            foreach (OperatorInput input in Inputs)
            {
                input.RemoveConnection();
            }

            foreach (OperatorOutput output in Outputs)
            {
                output.RemoveAllConnections();
            }

            Manager.ResumeAlgorithm();
        }
        #endregion

        #region Send/Receive Data
        /// <summary>
        /// The input to the operaot has been updated. Flag the operator for reprocessing on its next execution iteration, inform the GUi the input has updated and wake the processing thread.
        /// </summary>
        /// <param name="input"></param>
        public virtual void InputUpdated(OperatorInput input)
        {
            ShouldReprocess = true;

            if (Manager.CallCallbacks)
            {
                Manager.Workspace.GUIListeners.ControlValueChanged(input.Control, input.Value);
            }

            if (!IsRootOperator)
            {
                WakeProcessingThread();
            }
        }

        public void SendOutputs()
        {
            foreach (OperatorOutput outputs in Outputs)
            {
                outputs.SendData();
            }
        }
        #endregion

        public virtual void WakeProcessingThread(bool postIteration = false)
        {
            if (Manager.IsAlgorithmStopping) { return; }
            if( !(this is Macro) && OperatorType._RunsPostIteration && !postIteration) 
            {
                Manager._QueueOperatorForPostIterationExecution(this);//todo: will this cause issues with post iteration operators inside macros?
                return; 
            }

            Manager._OperatorStarting(this);
            waitHandle.Set();
        }

        /// <summary>
        /// Terminates execution thread
        /// </summary>
        public virtual void Kill()
        {
            _kill = true;
            waitHandle.Set();
        }

        /// <summary>
        /// Runs on its own thread. Should not be called directly, use <see cref="WakeProcessingThread"/> to wake the thread.
        /// Detects if an error has occured and informs gui, invokes the operator's method, informs gui the output has updated and hands off data to connected operators.
        /// </summary>
        public virtual void Exectue()
        {
            #if (DEBUG)
            executing += 1;
            if(executing > 1) { throw new Exception(); }
            #endif

            while (!_kill)
            {
                waitHandle.WaitOne();
                DataMutex.WaitOne();
                if (_kill)
                    return;

                try
                {
                    if (ShouldReprocess || OperatorType.AlwaysReprocess)
                    {
                        ShouldReprocess = false;
                        Error = false;
                        OperatorError = null;

                        foreach (OperatorInput input in Inputs)
                        {
                            if (input.Error)
                            {
                                Error = true;
                                OperatorError = input.OperatorError;
                                break;
                            }
                        }
                        
                        if (!Error)
                        {
                            if (IsConstructor)
                            {
                                List<object> parameters = new List<object>();
                                foreach (OperatorInput input in Inputs)
                                {
                                    parameters.Add(input.Value);
                                }
                                object output = Constructor.Invoke(parameters.ToArray());
                                Outputs[0].Value = output;
                                if (Manager.CallCallbacks)
                                {
                                    Manager.Workspace.GUIListeners.ControlValueChanged(Outputs[0].Control, Outputs[0].Value);
                                }
                                SendOutputs();
                            }
                            else
                            {
                                object invokedObject = Inputs[0].Value;
                                List<object> parameters = new List<object>();
                                for (int i = 0; i < Inputs.Count; i++)
                                {
                                    parameters.Add(Inputs[i].Value);
                                }

                                if (invokedObject == null && !IsStaticMethod)
                                {
                                    Error = true;
                                    OperatorError = new RequiredInputNotDefined(Inputs[0].Name);
                                    Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                                }
                                else
                                {
                                    object result = Method.Invoke(parameters);

                                    for(int i = 0; i < Outputs.Count; i++)
                                    {
                                        if(Outputs[i].InputToOutput == null) { Outputs[i].Value = result; }
                                        else { Outputs[i].Value = Inputs[(int)Outputs[i].InputToOutput].Value; }
                                    }
                                    //Outputs[0].Value = Method.Invoke(parameters);
                                    
                                    if (Manager.CallCallbacks && Outputs.Count > 0)
                                    {
                                        Manager.Workspace.GUIListeners.ControlValueChanged(Outputs[0].Control, Outputs[0].Value);
                                    }
                                    //if (Outputs[0].Value != null)
                                    //{
                                        SendOutputs();
                                    //}
                                }
                            }
                        }
                        else if(Error && Manager.CallCallbacks)
                        {
                            Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                        }
                    }
                    else
                    {
                        if(!Error)
                        {
                            foreach(OperatorOutput output in Outputs)
                            {
                                foreach(OperatorInput input in output.Connections)
                                {
                                    input.Operator.WakeProcessingThread();
                                }
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    while (e.InnerException != null)
                    {
                        e = e.InnerException;
                    }
                    Error = true;
                    OperatorError = new UnkownError(e);
                    if(Manager.CallCallbacks)
                        Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                }

                if (Manager.CallCallbacks)
                    Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                waitHandle.Reset();
                Manager._OperatorFinished(this);
                DataMutex.ReleaseMutex();
            }
        }

#region CyclePrevention
        public static bool WillConnectionCauseCycle(Operator target, Operator source)
        {
            List<Operator> childern = FindChildern(target);
            childern.Add(target);
            return childern.Contains(source);
        }

        public static List<Operator> FindChildern(Operator op)
        {
            List<Operator> result = new List<Operator>();
            
            for(int i = 0; i < op.Outputs.Count; i++)
            {
                for (int j = 0; j < op.Outputs[i].Connections.Count; j++)
                {
                    if (op.Outputs[i].Connections[j] != null)
                    {
                        result.Add(op.Outputs[i].Connections[j].Operator);
                        result.AddRange(FindChildern(op.Outputs[i].Connections[j].Operator));
                    }
                }
            }

            return result;
        }
#endregion
        
    }
}
