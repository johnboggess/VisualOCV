﻿using VisualOCVBackend.Controls;
using System;
using System.Collections.Generic;
using System.Threading;
namespace VisualOCVBackend.Operators
{
    public class Macro : Operator
    {
        public MacroType MacroType;

        internal AlgorithmManager _AlgorithmManager;


        private List<Operator> macroInputs = new List<Operator>();
        private List<Operator> macroOutputs = new List<Operator>();
        private Dictionary<Operator, OperatorInput> opToInputMap = new Dictionary<Operator, OperatorInput>();
        private Dictionary<Operator, OperatorOutput> opToOutputMap = new Dictionary<Operator, OperatorOutput>();

        public Macro(AlgorithmManager manager, MacroType macroType, Guid guid)
        {
            Manager = manager;
            MacroType = macroType;
            _AlgorithmManager = new AlgorithmManager(false, Manager.Workspace);

            Name = macroType.Name;
            _processThread = new Thread(Exectue);
            _processThread.Start();

            if (guid == Guid.Empty || manager.DoesOperatorExist(guid))
            {
                instanceID = Guid.NewGuid();
            }
            else
            {
                instanceID = guid;
            }

            _AlgorithmManager.Load(macroType.LoadData);

            List<Operator> allOperators = _AlgorithmManager.GetListOfOperators();

            List<OperatorInput> inputs = new List<OperatorInput>();
            List<OperatorOutput> outputs = new List<OperatorOutput>();

            foreach (Operator op in allOperators)
                if (op.OperatorType == Operators.OperatorModule.AllOperatorTypes["VisualOCVMacroInput"])
                    macroInputs.Add(op);
                else if (op.OperatorType == Operators.OperatorModule.AllOperatorTypes["VisualOCVMacroOutput"])
                    macroOutputs.Add(op);

            List<Operator> opsBeforeInput = new List<Operator>();
            List<Operator> opsAfterOutput = new List<Operator>();

            foreach (Operator op in macroInputs)
            {
                OperatorInput input = new OperatorInput(this, typeof(object));
                input.Name = (string)op.Inputs[0].Value;
                input.ControlType = Controls.ControlType.None; 
                input.Control = new None(this, input);
                Inputs.Add(input);
                opToInputMap.Add(op, input);

                opsBeforeInput.AddRange(_operatorsBeforeMacroInput(op));
            }
            foreach (Operator op in macroOutputs)
            {
                OperatorOutput output = new OperatorOutput(this, typeof(object));
                output.Name = (string)op.Inputs[0].Value;
                output.ControlType = Controls.ControlType.None;
                output.Control = new None(this, output);
                Outputs.Add(output);
                opToOutputMap.Add(op, output);

                opsAfterOutput.AddRange(Operator.FindChildern(op));
            }

            foreach (Operator op in opsBeforeInput)
                op.Delete();
            foreach (Operator op in opsAfterOutput)
                op.Delete();
        }

        public override bool Resizable()
        {
            return false;
        }

        public Operator NewOperatorInstance(OperatorType opType, Guid guid)
        {
            return _AlgorithmManager.NewOperatorInstance(opType, guid);
        }

        public override void Kill()
        {
            _kill = true;
            _AlgorithmManager._ClearAlgorithm();
            waitHandle.Set();
        }

        public override void Exectue()
        {
            while (true)
            {
                if (_kill)
                    return;
                waitHandle.WaitOne();
                DataMutex.WaitOne();
                if (_kill)
                    return;

                if (ShouldReprocess)
                {
                    ShouldReprocess = false;//todo: macros should have this always true, or true if an operator inside needs to reprocess every iteration. However doing that leads to the macro constantly entering and leaving an error state if the inputs are not yet defined.
                    Error = false;
                    OperatorError = null;

                    foreach (OperatorInput input in Inputs)
                    {
                        if (input.Error)
                        {
                            Error = true;
                            OperatorError = input.OperatorError;
                            break;
                        }
                    }

                    if (Manager.CallCallbacks)
                    {
                        Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                    }

                    if (!Error)
                    {
                        try
                        {
                            MacroExecute();
                            if (Manager.CallCallbacks)
                            {
                                Manager.Workspace.GUIListeners.ControlValueChanged(Outputs[0].Control, Outputs[0].Value);
                            }
                            SendOutputs();
                        }
                        catch (Exception e)
                        {
                            Error = true;
                            OperatorError = new UnkownError(e);
                        }
                    }
                }
                else
                {
                    if (!Error)
                    {
                        foreach (OperatorOutput output in Outputs)
                        {
                            foreach (OperatorInput input in output.Connections)
                            {
                                input.Operator.WakeProcessingThread();
                            }
                        }
                    }
                }

                waitHandle.Reset();
                Manager._OperatorFinished(this);
                DataMutex.ReleaseMutex();
            }
        }

        private void MacroExecute()
        {
            foreach (Operator op in macroInputs)
            {
                op.ShouldReprocess = true;
                op.Inputs[1].Value = opToInputMap[op].Value;
            }

            _AlgorithmManager.BeginAlgorithm();
            while (!_AlgorithmManager.IsAlgorithmFinished) { }

            List<Operator> ops = _AlgorithmManager.GetListOfOperators();
            foreach (Operator op in ops)
            {
                if(op.Error && !(op.IsRootOperator && op.OperatorError is UnkownError))
                {
                    OperatorError = new MacroInternalError(op, op.OperatorError);
                    Error = true;

                    if (Manager.CallCallbacks)
                    {
                        Manager.Workspace.GUIListeners.OperatorErrorStateChanged(this, Error);
                    }

                    break;
                }
            }

            foreach (Operator op in macroOutputs)
                opToOutputMap[op].Value = op.Outputs[0].Value;

            return;
        }

        private List<Operator> _operatorsBeforeMacroInput(Operator macroInput)
        {
            List<Operator> result = new List<Operator>();
            foreach(OperatorInput input in macroInput.Inputs)
            {
                if (input.Connection != null)
                {
                    result.Add(input.Connection.Operator);
                    result.AddRange(_operatorsBeforeMacroInput(input.Connection.Operator));
                }
            }
            return result;
        }

        /*private List<Operator> _operatorsAfterMacroOutput(Operator macroOutput)
        {
            List<Operator> result = new List<Operator>();
            foreach (OperatorOutput output in macroOutput.Outputs)
            {
                foreach(OperatorInput input in output.Connections)
                {
                    result.Add(input.Operator);
                    result.AddRange(_operatorsBeforeMacroInput(input.Operator));
                }
            }
            return result;
        }*/
        /*private void _removeUneededOperatorsPriorToInput(OperatorOutput source, OperatorInput target)
        {
            source.Operator.Disconnect(target, source);
            foreach (OperatorInput input in source.Operator.Inputs)
                _removeUneededOperatorsPriorToInput(input.Connection, input);
            foreach (OperatorOutput output in source.Operator.Outputs)
                if (output.Connections.Count != 0)
                    return;
            source.Operator.Delete();
        }*/
    }
}
