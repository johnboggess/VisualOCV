﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;

using VisualOCVBackend.Controls;
using VisualOCVBackend.Types;
namespace VisualOCVBackend.Operators
{
    public class OperatorOutput : OperatorInputOutput
    {
        public List<OperatorInput> Connections = new List<OperatorInput>();
        public int? InputToOutput = null;
        public int Index
        {
            get { return Operator.Outputs.IndexOf(this); }
        }

        public OperatorOutput(Operator op, Type returnType, int? inputToOutput = null) : base(op)
        {
            SetValueType(returnType);
            InputToOutput = inputToOutput;
        }

        /// <summary>
        /// Attempts to clone the given object.
        /// </summary>
        /// <param name="obj">The object to clone.</param>
        /// <param name="visualOCVType">The VisualOCVType of the object if one exists (can be null)</param>
        /// <param name="shallowClone">MemberwiseClone method of the object</param>
        /// <returns></returns>
        public object CloneObject(object obj, VisualOCVType visualOCVType, MethodInfo shallowClone)
        {
            if (visualOCVType == null || !visualOCVType.IsClonable)
            {
                return shallowClone.Invoke(obj, new object[] { });
            }
            else
            {
                return visualOCVType.Clone(obj);
            }
        }

        /// <summary>
        /// Makes a copy of the value for each connected input and sends it to the inputs.
        /// </summary>
        public void SendData()
        {
            if(Value == null) { return; }

            MethodInfo shallowClone = Value.GetType().GetMethod("MemberwiseClone", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            VisualOCVType visualOCVType = TypeManager.GetVisualOCVTypeFromType(Value.GetType());

            if (Value.GetType().IsValueType)
            {
                foreach (OperatorInput inputs in Connections)
                {
                    inputs.ReceiveData(Value);
                }
            }
            else
            {
                foreach (OperatorInput inputs in Connections)
                {
                    if (Value.GetType().GetInterface("IEnumerable") == null || (Value.GetType().GetInterface("IEnumerable") != null && visualOCVType != null && visualOCVType.IsClonable))
                    {
                        inputs.ReceiveData(CloneObject(Value, visualOCVType, shallowClone));
                    }
                    else //clone array
                    {
                        //todo: does not work for multidimensional arrays, not sure how to get their size
                        //Should support heterogeneous arrays
                        int enumerableLength = Utilities.GetIEnumerableLength((IEnumerable)Value);
                        Array newArray = Array.CreateInstance(typeof(object), enumerableLength);

                        int i = 0;
                        if (enumerableLength > 0)
                        {
                            IEnumerator enumerator = ((IEnumerable)Value).GetEnumerator();
                            enumerator.MoveNext();
                            foreach (object obj in (IEnumerable)Value)
                            {
                                VisualOCVType arrayVisualOCVType = TypeManager.GetVisualOCVTypeFromType(enumerator.Current.GetType());
                                object clonedObjected = CloneObject(obj, arrayVisualOCVType, shallowClone);
                                newArray.SetValue(clonedObjected, i);
                                i++;
                                enumerator.MoveNext();
                            }
                        }
                        inputs.ReceiveData(newArray);
                    }
                }
            }
        }

        public void RemoveConnection(OperatorInput input)
        {
            Operator.Manager.StopAlgorithm();

            int i = Connections.IndexOf(input);
            input.Connection = null;
            Connections.RemoveAt(i);
            Operator.Manager._MakeRootOrLeaf(Operator);
            Operator.Manager._MakeRootOrLeaf(input.Operator);

            if (Operator.Manager.CallCallbacks)
            {
                Operator.Manager.Workspace.GUIListeners.OperatorConnectionChanged(input, this, false);
            }

            Operator.Manager.ResumeAlgorithm();
        }

        public void RemoveAllConnections()
        {
            Operator.Manager.StopAlgorithm();

            foreach (OperatorInput input in Connections)
            {
                input.Connection = null;
                Operator.Manager._MakeRootOrLeaf(input.Operator);

                if (Operator.Manager.CallCallbacks)
                {
                    Operator.Manager.Workspace.GUIListeners.OperatorConnectionChanged(input, this, false);
                }
            }
            Connections.Clear();
            Operator.Manager._MakeRootOrLeaf(Operator);

            Operator.Manager.ResumeAlgorithm();
        }
    }
}
