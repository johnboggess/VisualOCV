﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Runtime.CompilerServices;

using VisualOCVBackend.Validators;

namespace VisualOCVBackend.Operators
{
    public class OperatorType
    {
        private OperatorModule _module;
        private string _name;
        private string _id;

        #region Public
        public OperatorMethod Method = null;
        public ConstructorInfo Constructor = null;
        public List<OperatorInput> Inputs = new List<OperatorInput>();
        public List<OperatorOutput> Outputs = new List<OperatorOutput>();
        public List<OperatorContextMenuItem> ContextMenuItems = new List<OperatorContextMenuItem>();
        public int Width = 150;
        public bool Resizable = false;
        public bool AlwaysReprocess = false;
        internal bool _RunsPostIteration = false;
        #endregion

        #region Properties
        public OperatorModule Module
        {
            get { return _module; }
        }

        public string ID
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public bool IsStaticMethod
        {
            get
            {
                if (IsMethod) { return Method.IsStatic; }
                return false;
            }
        }

        public bool IsMethod
        {
            get { return Method != null; }
        }

        public bool IsConstructor
        {
            get { return (Constructor != null); }
        }
        #endregion

        #region Constructors
        public OperatorType(string name, string id, OperatorModule module, OperatorMethod method, List<OperatorInput> inputs, List<OperatorOutput> outputs)
        {
            _name = name;
            _id = id;
            _module = module;
            Method = method;
            Inputs = inputs;
            Outputs = outputs;
        }

        public OperatorType(string name, string id, OperatorModule module, ConstructorInfo constructorInfo, List<OperatorInput> inputs, List<OperatorOutput> outputs)
        {
            _name = name;
            _id = id;
            _module = module;
            Constructor = constructorInfo;
            Inputs = inputs;
            Outputs = outputs;
        }
        #endregion

        public Operator CreateInstance(AlgorithmManager manager)
        {
            return CreateInstance(manager, Guid.Empty);
        }

        public Operator CreateInstance(AlgorithmManager manager, Guid guid)
        {
            return manager.NewOperatorInstance(this, guid);
        }

        internal Operator NewInstance(AlgorithmManager manager)
        {
            return NewInstance(manager, Guid.Empty);
        }

        internal Operator NewInstance(AlgorithmManager manager, Guid guid)
        {
            Operator result = new Operator(manager, Name, this, Method, null, null, guid);
            result.Constructor = Constructor;

            List<OperatorInput> inputs = new List<OperatorInput>();
            foreach (OperatorInput input in Inputs)
            {
                OperatorInput _input = new OperatorInput(result, input.GetValueType(false));

                foreach (Validator validator in input.Validators)
                {
                    _input.Validators.Add(validator);
                }
                _input.Name = input.Name;
                _input.ControlType = input.ControlType;
                _input.Control = input.Control.CreateCopy(result, _input);
                _input.Value = input.Value;
                inputs.Add(_input);
            }

            List<OperatorOutput> outputs = new List<OperatorOutput>();
            foreach (OperatorOutput output in Outputs)
            {
                if(output == null) { continue; }
                OperatorOutput _output = new OperatorOutput(result, output.GetValueType(false));
                _output.Name = output.Name;
                _output.ControlType = output.ControlType;
                _output.Control = output.Control.CreateCopy(result, _output);
                _output.InputToOutput = output.InputToOutput;
                outputs.Add(_output);
            }
            result.Inputs = inputs;
            result.Outputs = outputs;

            return result;
        }
    }
}
