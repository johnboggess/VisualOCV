﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

namespace VisualOCVBackend.Operators
{
    public class OperatorMethod
    {
        public MethodInfo MethodInfo;
        public bool IsStatic { get { return MethodInfo.IsStatic; } }
        public bool IsGenericDefinition { get { return MethodInfo.IsGenericMethodDefinition; } }
        public Type DeclaringType { get { return MethodInfo.DeclaringType; } }
        public Type[] Parameters
        {
            get
            {
                Type[] result = new Type[_parameters.Length];
                for(int i = 0; i < _parameters.Length; i++)
                {
                    result[i] = _treatParameterAs.ContainsKey(_parameters[i]) ? _treatParameterAs[_parameters[i]] : _parameters[i];
                }
                return result;
            }
        }

        private Dictionary<Type, Type> _treatParameterAs = new Dictionary<Type, Type>();
        private Type[] _parameters;
        private Tuple<Type, Tuple<int,int>>[] _genericParams;
        private MethodInfo _invokedMethod = null;

        public OperatorMethod(string assemblyQualifiedName, string functionName, Type[] parameters, Dictionary<Type, Type> treatParameterAs, Tuple<Type, Tuple<int, int>>[] genericParams)
        {
            _treatParameterAs = treatParameterAs;
            Type declaringType = Utilities.GetType(assemblyQualifiedName);
            MethodInfo = Utilities.GetMethod(declaringType, functionName, parameters, genericParams != null);//todo: could return null, this needs to be checked and return the correct errors
            _parameters = parameters;
            _genericParams = genericParams;
        }

        public OperatorMethod(Type declaringType, string functionName, Type[] parameters, Dictionary<Type, Type> treatParameterAs, Tuple<Type, Tuple<int, int>>[] genericParams)
        {
            _treatParameterAs = treatParameterAs;
            MethodInfo = Utilities.GetMethod(declaringType, functionName, parameters, genericParams != null);//todo: could return null, this needs to be checked and return the correct errors
            _parameters = parameters;
            _genericParams = genericParams;
        }

        public object Invoke(List<object> args)
        {
            object invokedObject = args[0];
            _invokedMethod = MethodInfo;

            Type invokedType = _invokedMethod.DeclaringType;

            if (!IsStatic)
            {
                args.RemoveAt(0);
            }

            if (_invokedMethod.DeclaringType.IsGenericTypeDefinition)
            {
                invokedType = Utilities.GenericTypeFillParameters(invokedType, invokedObject.GetType());//todo: this assumes that the first argument contains the appropriate generic parameters. Add a flag to xml that allows the user to specifcy which input the generic parameters should be copied from.
                List<Type> paramTypes = new List<Type>();
                foreach (object arg in args) { paramTypes.Add(arg.GetType()); }

                MethodInfo methodInfo = Utilities.GetMethod(invokedType, _invokedMethod.Name, paramTypes.ToArray(), IsGenericDefinition);
                
                if (methodInfo == null)
                {
                    methodInfo = Utilities.GetMethod(invokedType, _invokedMethod.Name, _parameters, IsGenericDefinition);
                    if (methodInfo == null)
                        throw new Exception("Cannot perform " + _invokedMethod.Name + " on the given inputs");
                }
                _invokedMethod = methodInfo;
            }
            
            if (IsGenericDefinition && _invokedMethod != null)
            {
                Type[] genericTypes = new Type[_genericParams.Length];
                for (int i = 0; i < genericTypes.Length; i++)
                {
                    if (_genericParams[i].Item1 != null) { genericTypes[i] = _genericParams[i].Item1; }
                    else
                    {
                        genericTypes[i] = args[_genericParams[i].Item2.Item1].GetType().GenericTypeArguments[_genericParams[i].Item2.Item2];
                    }
                }
                _invokedMethod = _invokedMethod.MakeGenericMethod(genericTypes);
            }

            if (_invokedMethod == null) { throw new Exception(_invokedMethod.Name + " cannot be performed on the given objects."); }
            return _invokedMethod.Invoke(invokedObject, args.ToArray());
        }
    }
}
