﻿using VisualOCVBackend.Controls;
using VisualOCVBackend.Types;
using VisualOCVBackend.Validators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Drawing;

using VisualOCVBackend.Exceptions;

namespace VisualOCVBackend.Operators
{
    public class OperatorXmlReader
    {
        static Workspace Workspace;

        public static Tuple<List<OperatorType>, List<Exception>> Read(Workspace workspace, string filePath, OperatorModule module)
        {
            Workspace = workspace;
            FileStream fileStream = File.OpenRead(filePath);
            XmlReader xmlReader = XmlReader.Create(fileStream);//todo: remove
            XDocument xDocument = XDocument.Load(filePath);

            IEnumerable<XElement> xes = xDocument.Descendants(XName.Get("Operator"));
            int version = -1;

            IEnumerable<XElement> versionXml = xDocument.Descendants(XName.Get("Version"));
            int.TryParse(versionXml.First().Attribute(XName.Get("version")).Value, out version);

            if (version == 1)
            {
                return ReadV1(workspace, xDocument, module);
            }

            FileVersionNotSupported exception = new FileVersionNotSupported(xmlReader.GetAttribute("version"), module.FullName); 
            return new Tuple<List<OperatorType>, List<Exception>>(new List<OperatorType>(), new List<Exception> { exception });
        }
        
        private static Tuple<List<OperatorType>, List<Exception>> ReadV1(Workspace workspace, XDocument xDocument, OperatorModule module)
        {
            ParseDlls(xDocument);
            List<OperatorType> ops = new List<OperatorType>();
            IEnumerable<XElement> XMLOperators = xDocument.Descendants(XName.Get("Operator"));

            List<Exception> exceptions = new List<Exception>();

            foreach(XElement xElement in XMLOperators)
            {
                Result<OperatorType> result = ParseOperator(xElement, module);
                if (!result.HasExceptionOccured)
                {
                    ops.Add(result.Value);
                }
                else
                {
                    exceptions.Add(result.Exception);
                }
            }
            return new Tuple<List<OperatorType>, List<Exception>>(ops, exceptions);
        }
        
        private static void ParseDlls(XDocument xDocument)
        {
            IEnumerable<XElement> dlls = xDocument.Descendants(XName.Get("Dlls"));
            IEnumerable<XElement> XMLDll = dlls.Descendants(XName.Get("Dll"));

            foreach(XElement dll in XMLDll)
            {
                if (dll.Attribute(XName.Get("path")) != null)
                {
                    AssemblyManager.LoadAssembly(dll.Attribute(XName.Get("path")).Value, true);
                }
            }
        }

        private static Result<OperatorType> ParseOperator(XElement XMLOperator, OperatorModule module)
        {
            #if (DEBUG == false)
            try
            {
            #endif
            IEnumerable<XElement> opids = XMLOperator.Descendants(XName.Get("ID"));
            IEnumerable<XAttribute> attribs = opids.Count() > 0 ? opids.Attributes() : null;
            string opID = (attribs != null && attribs.Count() > 0) ? attribs.ElementAt(0).Value : null;

            IEnumerable<XElement> XMLTypes = XMLOperator.Descendants(XName.Get("Type"));

            if (XMLTypes.Count() == 0)
            {
                XMLRequiredTagMissing exception = new XMLRequiredTagMissing(opID == null ? "no ID given" : opID, module, "Type");
                return new Result<OperatorType>(null, exception);
            }

            XElement XMLType = XMLTypes.ElementAt(0);
            XAttribute XMLTypeAQN = XMLType.Attribute(XName.Get("AssemblyQualifiedName"));
            string assemblyQualifiedName = "";
            if (XMLTypeAQN != null)
            {
                assemblyQualifiedName = XMLTypeAQN.Value;
            }
            else
            {
                XMLRequiredAttributeMissing exception = new XMLRequiredAttributeMissing(opID == null ? "no ID given" : opID, module, "AssemblyQualifiedName", "Type");
                return new Result<OperatorType>(null, exception);
            }

            Type type = Utilities.GetType(assemblyQualifiedName);
            if (type == null)
            {
                OperatorXMLInvalidType exception = new OperatorXMLInvalidType(opID == null ? "no ID given" : opID, module, assemblyQualifiedName);
                return new Result<OperatorType>(null, exception);
            }

            string operatorName = "";
            if (XMLOperator.Descendants(XName.Get("OperatorName")).Count() == 1)
            {
                XElement xElement = XMLOperator.Descendants(XName.Get("OperatorName")).ElementAt(0);
                if (xElement.Attributes().Count() == 1)
                {
                    operatorName = xElement.Attributes().ElementAt(0).Value;
                }
                else
                {
                    XMLRequiredAttributeMissing exception = new XMLRequiredAttributeMissing(opID == null ? "no ID given" : opID, module, "attribute", "OperatorName");
                    return new Result<OperatorType>(null, exception);
                }
            }
            else if (XMLOperator.Descendants(XName.Get("OperatorName")).Count() > 1)
            {
                OperatorXMLTagDeclaredMultipleTimes exception = new OperatorXMLTagDeclaredMultipleTimes(opID == null ? "no ID given" : opID, module, "OperatorName");
                return new Result<OperatorType>(null, exception);
            }
            else
            {
                XMLRequiredTagMissing exception = new XMLRequiredTagMissing(opID == null ? "no ID given" : opID, module, "OperatorName");
                return new Result<OperatorType>(null, exception);
            }

            string operatorID = "";
            if (XMLOperator.Descendants(XName.Get("ID")).Count() == 1)
            {
                XElement xElement = XMLOperator.Descendants(XName.Get("ID")).ElementAt(0);
                if (xElement.Attributes().Count() == 1)
                {
                    operatorID = xElement.Attributes().ElementAt(0).Value;
                }
                else
                {
                    XMLRequiredAttributeMissing exception = new XMLRequiredAttributeMissing(opID == null ? "no ID given" : opID, module, "attribute", "ID");
                    return new Result<OperatorType>(null, exception);
                }
            }
            else if (XMLOperator.Descendants(XName.Get("ID")).Count() > 1)
            {
                OperatorXMLTagDeclaredMultipleTimes exception = new OperatorXMLTagDeclaredMultipleTimes(opID == null ? "no ID given" : opID, module, "ID");
                return new Result<OperatorType>(null, exception);
            }
            else
            {
                XMLRequiredTagMissing exception = new XMLRequiredTagMissing(opID == null ? "no ID given" : opID, module, "ID");
                return new Result<OperatorType>(null, exception);
            }

            int opWidth = -1;
            IEnumerable<XElement> XMLWidth = XMLOperator.Descendants(XName.Get("Width"));
            if (XMLWidth.Count() > 0)
            {
                XElement width = XMLWidth.ElementAt(0);
                if (width.Attributes().Count() > 0)
                {
                    int w = 0;
                    if (int.TryParse(width.Attributes().ElementAt(0).Value, out w))
                    {
                        opWidth = w;
                    }
                }
            }

            bool runsPostIteration = XMLOperator.Descendants(XName.Get("RunsPostIteration")).Count() > 0;
            bool resizable = XMLOperator.Descendants(XName.Get("Resizable")).Count() > 0;
            bool alwaysReprocess = XMLOperator.Descendants(XName.Get("AlwaysReprocess")).Count() > 0;
            
            Result<object> operatorFunction = ParseMethodOrConstructor(XMLOperator, type, operatorID, module);
            if (operatorFunction.HasExceptionOccured) { return new Result<OperatorType>(null, operatorFunction.Exception); }

            Result<List<OperatorInput>> operatorInputs = ParseOperatorInputs(XMLOperator, operatorFunction.Value, operatorID, module);
            if (operatorInputs.HasExceptionOccured) { return new Result<OperatorType>(null, operatorInputs.Exception); }
            Result<List<OperatorOutput>> operatorOutput = ParseOperatorOutputs(XMLOperator, operatorFunction.Value, operatorID, module);
            if (operatorOutput.HasExceptionOccured) { return new Result<OperatorType>(null, operatorOutput.Exception); }

            if (operatorFunction.Value is OperatorMethod)
            {
                OperatorType operatorType = new OperatorType(operatorName, operatorID, module, (OperatorMethod)operatorFunction.Value, operatorInputs.Value, operatorOutput.Value);
                operatorType.Resizable = resizable;
                operatorType.AlwaysReprocess = alwaysReprocess;
                operatorType._RunsPostIteration = runsPostIteration;
                if (opWidth > 0) { operatorType.Width = opWidth; }
                return new Result<OperatorType>(operatorType, null);
            }
            else
            {
                OperatorType operatorType = new OperatorType(operatorName, operatorID, module, (ConstructorInfo)operatorFunction.Value, operatorInputs.Value, operatorOutput.Value);
                operatorType.Resizable = resizable;
                operatorType.AlwaysReprocess = alwaysReprocess;
                operatorType._RunsPostIteration = runsPostIteration;
                if (opWidth > 0) { operatorType.Width = opWidth; }
                return new Result<OperatorType>(operatorType, null);
            }

            #if (DEBUG == false)
            }
            catch(Exception e)
            {
                VisualOCVException exception = new VisualOCVException();
                exception.InnerException = e;
                return new Result<OperatorType>(null, exception);
            }
            #endif
        }

        private static Result<object> ParseMethodOrConstructor(XElement XMLOperator, Type type, string operatorID, OperatorModule module)
        {
            IEnumerable<XElement> XMLconstructor = XMLOperator.Descendants(XName.Get("Constructor"));
            IEnumerable<XElement> XMLmethod = XMLOperator.Descendants(XName.Get("Method"));

            IEnumerable<XElement> XMLFunction = XMLconstructor.Count() == 0 ? XMLmethod : XMLconstructor;
            if (XMLFunction.Count() == 0)
            {
                XMLRequiredTagMissing exception = new XMLRequiredTagMissing(operatorID, module, "Constructor/Method");
                return new Result<object>(null, exception);
            }
            else if(XMLFunction.Count() > 1)
            {
                OperatorXMLTagDeclaredMultipleTimes exception = new OperatorXMLTagDeclaredMultipleTimes(operatorID, module, "Constructor/Method");
                return new Result<object>(null, exception);
            }

            IEnumerable<XAttribute> xAttributes = XMLFunction.Attributes();
            string functionName = XMLFunction.ElementAt(0).Attribute("Name") != null? XMLFunction.ElementAt(0).Attribute("Name").Value : "";

            Dictionary<Type, Type> _treatTypeAs = new Dictionary<Type, Type>();
            List<Type> _params = new List<Type>();

            for (int i = 0; i < xAttributes.Count(); i++)
            {

                XAttribute attribute = xAttributes.ElementAt(i);
                if(attribute.Name == "Name") { continue; }
                if (attribute.Name.LocalName.ToLower().Contains("treatas"))
                {
                    Type treatAs = Utilities.GetType(attribute.Value, type);
                    if (treatAs == null)
                    {
                        OperatorXMLInvalidType exception = new OperatorXMLInvalidType(operatorID, module, attribute.Value);
                        return new Result<object>(null, exception);
                    }

                    if (_params.Count == 0)
                    {
                        OperatorXMLParamTreatAsIncorrectPosition exception = new OperatorXMLParamTreatAsIncorrectPosition(operatorID, module);
                        return new Result<object>(null, exception);
                    }
                    if (_treatTypeAs.ContainsKey(_params[_params.Count - 1]))
                    {
                        OperatorXMLParamTreatAsDefineMultipleTimes exception = new OperatorXMLParamTreatAsDefineMultipleTimes(operatorID, module);
                        return new Result<object>(null, exception);
                    }

                    _treatTypeAs.Add(_params[_params.Count-1], treatAs);
                    continue;
                }

                Type param = Utilities.GetType(attribute.Value, type);
                if (param == null)
                {
                    OperatorXMLInvalidType exception = new OperatorXMLInvalidType(operatorID, module, attribute.Value);
                    return new Result<object>(null, exception);
                }
                _params.Add(param);
            }

            IEnumerable<XElement> generic = XMLOperator.Descendants(XName.Get("Generic"));
            Result<Tuple<Type, Tuple<int, int>>[]> genericTypes = new Result<Tuple<Type, Tuple<int, int>>[]>(null, null);
            if(generic.Count() > 0)
            {
                genericTypes = ParseMethodGenerics(generic.ElementAt(0), operatorID, module);
                if (genericTypes.HasExceptionOccured) { return new Result<object>(null, genericTypes.Exception); }
            }
            
            if (XMLmethod.Count() == 1)
            {
                return new Result<object>(new OperatorMethod(type, functionName, _params.ToArray(), _treatTypeAs, genericTypes.Value), null);
            }
            else if (XMLconstructor.Count() == 1)
            {
                return new Result<object> (type.GetConstructor(_params.ToArray()), null);
            }

            XMLRequiredTagMissing _exception = new XMLRequiredTagMissing(operatorID, module, "Constructor/Method");
            return new Result<object>(null, _exception);
        }

        private static Result<Tuple<Type, Tuple<int,int>>[]> ParseMethodGenerics(XElement XMLGenerics, string operatorID, OperatorModule module)
        {
            List<Tuple<Type, Tuple<int, int>>> result = new List<Tuple<Type, Tuple<int, int>>>();
            IEnumerable<XElement> _params = XMLGenerics.Descendants();

            foreach(XElement element in _params)
            {
                if (!element.HasAttributes) { return new Result<Tuple<Type, Tuple<int, int>>[]>(null, new XMLRequiredAttributeMissing("Type/CopyInputGeneric", "Param")); }

                Type type = Utilities.GetType(element.Attributes().ElementAt(0).Value);
                Tuple<int, int> genericMap = null;

                string[] ints = element.Attributes().ElementAt(0).Value.Split(',');
                if (ints.Length >= 2)
                {
                    int i1 = 0;
                    int i2 = 0;
                    bool b1 = int.TryParse(ints[0], out i1);
                    bool b2 = int.TryParse(ints[1], out i2);
                    if(b1 && b2) { genericMap = new Tuple<int, int>(i1, i2); }
                }

                if(type == null && genericMap == null) { return new Result<Tuple<Type, Tuple<int, int>>[]>(null, new XMLInvalidAttribute("Type/CopyInputGeneric", "Param", element.Attributes().ElementAt(0).Value)); }

                result.Add(new Tuple<Type, Tuple<int, int>>(type, genericMap));
            }
            return new Result<Tuple<Type, Tuple<int, int>>[]>(result.ToArray(), null);
        }

        private static Result<List<OperatorInput>> ParseOperatorInputs(XElement XMLOperator, object operatorFunction, string operatorID, OperatorModule module)
        {
            List<OperatorInput> operatorInputs = new List<OperatorInput>();
            IEnumerable<XElement> XMLOperatorInputs = XMLOperator.Descendants(XName.Get("OperatorInputs"));

            if (XMLOperatorInputs.Count() != 1)
            {
                XMLRequiredTagMissing exception = new XMLRequiredTagMissing(operatorID, module, "OperatorInputs");
                return new Result<List<OperatorInput>>(null, exception);
            }

            IEnumerable<XElement> XMLInputs = XMLOperatorInputs.Descendants(XName.Get("Input"));

            for(int inputIndex = 0; inputIndex < XMLInputs.Count(); inputIndex++)
            {
                XElement XMLInput = XMLInputs.ElementAt(inputIndex);
                OperatorInput operatorInput = new OperatorInput(null, null);

                XElement XMLInputName;
                string InputName = "";
                XElement XMLInputMethodParamIndex;
                XElement XMLInputType;
                XElement XMLInputControl;
                IEnumerable<XElement> XMLValidators;

                XMLInputName = XMLInput.Descendants(XName.Get("Name")).Count() == 1 ? XMLInput.Descendants(XName.Get("Name")).ElementAt(0) : null;
                XMLInputMethodParamIndex = XMLInput.Descendants(XName.Get("MethodParamIndex")).Count() == 1 ? XMLInput.Descendants(XName.Get("MethodParamIndex")).ElementAt(0) : null;
                XMLInputType = XMLInput.Descendants(XName.Get("Type")).Count() == 1 ? XMLInput.Descendants(XName.Get("Type")).ElementAt(0) : null;
                XMLInputControl = XMLInput.Descendants(XName.Get("Control")).Count() == 1 ? XMLInput.Descendants(XName.Get("Control")).ElementAt(0) : null;
                XMLValidators = XMLInput.Descendants(XName.Get("Validator"));

                if (XMLInputName == null || XMLInputName.Attributes().Count() == 0)
                {
                    OperatorXMLOperatorInputOutputMissingAttributeOrTag exception = new OperatorXMLOperatorInputOutputMissingAttributeOrTag(operatorID, module, "Name", inputIndex, true);
                    return new Result<List<OperatorInput>>(null, exception);
                }
                if (XMLInputMethodParamIndex == null && XMLInputType == null)
                {
                    OperatorXMLOperatorInputOutputMissingAttributeOrTag exception = new OperatorXMLOperatorInputOutputMissingAttributeOrTag(operatorID, module, "MethodParamIndex/Type", inputIndex, true);
                    return new Result<List<OperatorInput>>(null, exception);
                }
                if (XMLInputControl == null || XMLInputControl.Attributes().Count() == 0)
                {
                    OperatorXMLOperatorInputOutputMissingAttributeOrTag exception = new OperatorXMLOperatorInputOutputMissingAttributeOrTag(operatorID, module, "Control", inputIndex, true);
                    return new Result<List<OperatorInput>>(null, exception);
                }

                InputName = XMLInputName.Attributes().ElementAt(0).Value;

                if (XMLInputMethodParamIndex != null)
                {
                    Type[] _params = operatorFunction is OperatorMethod? ((OperatorMethod)operatorFunction).Parameters : Array.ConvertAll(((ConstructorInfo)operatorFunction).GetParameters(), item => item.ParameterType);
                    int index = 0;
                    if (XMLInputMethodParamIndex.Attributes().Count() == 0)
                    {
                        XMLRequiredAttributeMissing exception = new XMLRequiredAttributeMissing(operatorID, module, "Index", "MethodParamIndex", inputIndex);
                        return new Result<List<OperatorInput>>(null, exception);
                    }

                    if (int.TryParse(XMLInputMethodParamIndex.Attributes().ElementAt(0).Value, out index))
                    {
                        operatorInput.SetValueType(_params[index]);
                    }
                    else
                    {
                        VisualOCVException exception = new VisualOCVException();
                        exception.Message = XMLInputMethodParamIndex.Attributes().ElementAt(0).Value + " is an invalid index for MethodParamIndex of input " + inputIndex + " of operator " + operatorID + " in module " + module.FullName;
                        return new Result<List<OperatorInput>>(null, exception);
                    }
                }
                else if (XMLInputType != null)
                {
                    if (XMLInputType.Attributes().Count() == 0)
                    {
                        XMLRequiredAttributeMissing exception = new XMLRequiredAttributeMissing(operatorID, module, "AssemblyQualifiedName", "Type", inputIndex);
                        return new Result<List<OperatorInput>>(null, exception);
                    }

                    string typeString = XMLInputType.Attributes().ElementAt(0).Value;
                    Type inputType;
                    
                    inputType = Utilities.GetType(typeString);

                    if (inputType == null)
                    {
                        OperatorXMLInvalidType exception = new OperatorXMLInvalidType(operatorID, module, typeString, inputIndex);
                        return new Result<List<OperatorInput>>(null, exception);
                    }
                    operatorInput.SetValueType(inputType);
                }


                operatorInput.ControlType = stringToControlType(XMLInputControl.Attribute("Control").Value);
                Dictionary<string, string> paramsForControl = new Dictionary<string, string>();
                for (int i = 1; i < XMLInputControl.Attributes().Count(); i++)
                {
                    paramsForControl.Add(XMLInputControl.Attributes().ElementAt(i).Name.LocalName.ToLower(), XMLInputControl.Attributes().ElementAt(i).Value);
                }
                createControl(operatorInput, paramsForControl);

                List<Validator> validators = new List<Validator>();
                foreach (XElement XMLValidator in XMLValidators)
                {
                    IEnumerable<XAttribute> xAttributes = XMLValidator.Attributes();
                    string validatorName = "";
                    List<object> _params = new List<object>();
                    ParameterInfo[] validatorParams = null;
                    for (int i = 0; i < xAttributes.Count(); i++)
                    {
                        if (i == 0)
                        {
                            validatorName = xAttributes.ElementAt(0).Value;
                            validatorParams = ValidatorManager.GetValidatorParameterInfo(validatorName);
                        }
                        else
                        {
                            _params.Add(Convert.ChangeType(xAttributes.ElementAt(i).Value, validatorParams[i - 1].ParameterType));
                        }
                    }
                    validators.Add(ValidatorManager.CreateValidator(validatorName, _params));
                }

                operatorInput.Name = InputName;
                operatorInput.Validators = validators;
                operatorInputs.Add(operatorInput);
            }
            return new Result<List<OperatorInput>>(operatorInputs, null);
        }

        private static Result<List<OperatorOutput>> ParseOperatorOutputs(XElement XMLOperator, object operatorFunction, string operatorID, OperatorModule module)
        {
            List<OperatorOutput> result = new List<OperatorOutput>();
            IEnumerable<XElement> XMLOutputs = XMLOperator.Descendants(XName.Get("OperatorOutputs"));
            

            List<XElement> outputs = XMLOutputs.Descendants(XName.Get("Output")).ToList();

            for(int outputIndex = 0; outputIndex < outputs.Count; outputIndex++)
            {
                OperatorOutput operatorOutput = new OperatorOutput(null, null);
                XElement XMLOutput = outputs[outputIndex];

                XElement XMLOutputName;
                XElement XMLOutputControl;
                XElement XMLReturnInput;
                string OutputName = "";

                XMLOutputName = GetSingleXElement(XMLOutput, "Name", 1);
                XMLOutputControl = GetSingleXElement(XMLOutput, "Control", 1);
                XMLReturnInput = GetSingleXElement(XMLOutput, "ReturnInput", 1);

                if (XMLOutputName == null)
                {
                    OperatorXMLOperatorInputOutputMissingAttributeOrTag exception = new OperatorXMLOperatorInputOutputMissingAttributeOrTag(operatorID, module, "Name", 0, false);
                    return new Result<List<OperatorOutput>>(null, exception);
                }
                if (XMLOutputControl == null)
                {
                    OperatorXMLOperatorInputOutputMissingAttributeOrTag exception = new OperatorXMLOperatorInputOutputMissingAttributeOrTag(operatorID, module, "Control", 0, false);
                    return new Result<List<OperatorOutput>>(null, exception);
                }

                if (XMLReturnInput != null)
                {
                    int index = 0;
                    if (!int.TryParse(XMLReturnInput.FirstAttribute.Value, out index))
                    {
                        return new Result<List<OperatorOutput>>(null, new XMLInvalidAttribute(XMLReturnInput.FirstAttribute.Name.LocalName, XMLReturnInput.Name.LocalName, XMLReturnInput.FirstAttribute.Value));
                    }
                    operatorOutput.SetValueType(operatorFunction is OperatorMethod ? ((OperatorMethod)operatorFunction).MethodInfo.GetParameters()[index].ParameterType : ((ConstructorInfo)operatorFunction).GetParameters()[index].ParameterType);
                    operatorOutput.InputToOutput = index;
                }
                else
                {
                    operatorOutput.SetValueType(operatorFunction is OperatorMethod ? ((OperatorMethod)operatorFunction).MethodInfo.ReturnType : ((ConstructorInfo)operatorFunction).DeclaringType);
                }

                OutputName = XMLOutputName.Attributes().ElementAt(0).Value;
                operatorOutput.Name = OutputName;
                
                operatorOutput.ControlType = stringToControlType(XMLOutputControl.Attribute("Control").Value);
                Dictionary<string, string> paramsForControl = new Dictionary<string, string>();
                for (int i = 1; i < XMLOutputControl.Attributes().Count(); i++)
                {
                    paramsForControl.Add(XMLOutputControl.Attributes().ElementAt(i).Name.LocalName.ToLower(), XMLOutputControl.Attributes().ElementAt(i).Value);
                }
                createControl(operatorOutput, paramsForControl);

                result.Add(operatorOutput);
            }

            return new Result<List<OperatorOutput>>(result, null);
        }


#region control parsing
        private static string[] controlParams(XmlReader xmlReader)
        {
            List<string> result = new List<string>();

            for(int i = 1; i < xmlReader.AttributeCount; i++)
            {
                result.Add(xmlReader.GetAttribute(i));
            }
            return result.ToArray();
        }

        ///TODO: hanle unable to parse params 
        private static void createControl(OperatorInputOutput inputOutput, Dictionary<string, string> controlParams)
        {
            if (inputOutput is OperatorInput && controlParams.ContainsKey("default"))
            {//Sets the default value of an input if the input is a C# primitive, otherwise set the default value in the if case for that control
                inputOutput.Value = defaultControlValueStringToType(controlParams["default"], inputOutput.GetValueType(false));
            }

            if(inputOutput.ControlType == ControlType.Label)
            {
                inputOutput.Control = new Label(inputOutput.Operator, inputOutput, "");
            }
            else if(inputOutput.ControlType == ControlType.Checkbox)
            {
                inputOutput.Control = new Checkbox(inputOutput.Operator, inputOutput, controlParams["default"].ToLower() == "true");
            }
            else if (inputOutput.ControlType == ControlType.Slider)
            {
                //https://stackoverflow.com/questions/2078914/creating-a-generict-type-instance-with-a-variable-containing-the-type
                Type genericSlider = typeof(Slider<>);
                Type specificSlider = genericSlider.MakeGenericType(inputOutput.GetValueType(false));
                inputOutput.Control = (Control)Activator.CreateInstance(specificSlider, inputOutput.Operator, inputOutput, controlParams["default"], controlParams["min"], controlParams["max"]); //new Slider(inputOutput.Operator, inputOutput, controlParams[0], controlParams[1], controlParams[2]);
            }
            else if (inputOutput.ControlType == ControlType.SliderWithTextbox)
            {
                //https://stackoverflow.com/questions/2078914/creating-a-generict-type-instance-with-a-variable-containing-the-type
                Type genericSlider = typeof(SliderWithTextbox<>);
                Type specificSlider = genericSlider.MakeGenericType(inputOutput.GetValueType(false));
                inputOutput.Control = (Control)Activator.CreateInstance(specificSlider, inputOutput.Operator, inputOutput, controlParams["default"], controlParams["min"], controlParams["max"]);
                
                //inputOutput.Control = new SliderWithTextbox(inputOutput.Operator, inputOutput, controlParams[0], controlParams[1], controlParams[2]);
            }
            else if (inputOutput.ControlType == ControlType.Textbox)
            {
                inputOutput.Control = new Textbox(inputOutput.Operator, inputOutput, controlParams["default"]);
            }
            else if (inputOutput.ControlType == ControlType.MultilineTextbox)
            {
                inputOutput.Control = new MultilineTextbox(inputOutput.Operator, inputOutput, controlParams["default"]);
            }
            else if(inputOutput.ControlType == ControlType.Image)
            {
                inputOutput.Control = new ImageControl(inputOutput.Operator, inputOutput);
            }
            else if (inputOutput.ControlType == ControlType.Dropdown)
            {
                string enumAQN = controlParams["enumaqn"];
                string defaultOptions = controlParams["default"];
                Dictionary<string, string> options = new Dictionary<string, string>();

                List<string> values = controlParams.Values.ToList();
                for(int i = 2; i < values.Count; i+=2)
                {
                    options.Add(values[i], values[i + 1]);
                }
                inputOutput.Control = new Dropdown(inputOutput.Operator, inputOutput, enumAQN, defaultOptions, options);
                inputOutput.Value = ((Dropdown)inputOutput.Control).ParseStringToEnum(defaultOptions);
            }
            else if(inputOutput.ControlType == ControlType.None)
            {
                inputOutput.Control = new None(inputOutput.Operator, inputOutput);
            }
            #if(DEBUG)
            else
            {
                throw new Exception("Unknown control");
            }
            #else
            else
            {
                inputOutput.Control = new None(inputOutput.Operator, inputOutput);
            }
            #endif
        }

        private static ControlType stringToControlType(string control)
        {
            control = control.ToLower();
            switch(control)
            {
                case "label":
                    return ControlType.Label;
                case "checkbox":
                    return ControlType.Checkbox;
                case "slider":
                    return ControlType.Slider;
                case "sliderwithtextbox":
                    return ControlType.SliderWithTextbox;
                case "textbox":
                    return ControlType.Textbox;
                case "multilinetextbox":
                    return ControlType.MultilineTextbox;
                case "image":
                    return ControlType.Image;
                case "dropdown":
                    return ControlType.Dropdown;
                default:
                    return ControlType.None;
            }
        }

        private static object defaultControlValueStringToType(string value, Type type)
        {
            if (type == typeof(bool))
            {
                return bool.Parse(value);
            }
            else if(type == typeof(byte))
            {
                byte i;
                if (byte.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(sbyte))
            {
                sbyte i;
                if (sbyte.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(char))
            {
                return value.Length > 0? value[0] : ' ';
            }
            else if (type == typeof(decimal))
            {
                decimal i;
                if (decimal.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(double))
            {
                double i;
                if (double.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(float))
            {
                float i;
                if (float.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(int))
            {
                int i;
                if (int.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(uint))
            {
                uint i;
                if (uint.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(long))
            {
                long i;
                if (long.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(ulong))
            {
                ulong i;
                if (ulong.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(short))
            {
                short i;
                if (short.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(ushort))
            {
                ushort i;
                if (ushort.TryParse(value, out i))
                {
                    return i;
                }
                return 0;
            }
            else if (type == typeof(string))
            {
                return value;
            }
            else
            {
                return null;
            }
        }
        #endregion

        /// <summary>
        /// Get a list of XElements with the given name. Returns null if it finds more or less then the expected number of elements (set to -1 for 1 or more elements), or if there is more or less than the expected number of attributes for each element (set to -1 for 1 or more attributes).
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="elementName"></param>
        /// <param name="expectedNumberOfElements"></param>
        /// <param name="expectedNumberOfAttributes"></param>
        /// <returns></returns>
        private static List<XElement> GetXElements(XElement parent, string elementName, int expectedNumberOfElements, int expectedNumberOfAttributes)
        {
            List<XElement> xElements = parent.Descendants(XName.Get(elementName)).ToList();
            if(xElements.Count < 1 && expectedNumberOfElements >= 0)
                if(xElements.Count() != expectedNumberOfElements)
                    return null;

            if (xElements.Attributes().Count() < 1 && expectedNumberOfAttributes >= 0)
                if (xElements.Attributes().Count() != expectedNumberOfAttributes)
                    return null;

            return xElements;
        }

        private static XElement GetSingleXElement(XElement parent, string elementName, int expectedNumberOfAttributes)
        {
            List<XElement> result = GetXElements(parent, elementName, 1, expectedNumberOfAttributes);
            if(result == null) { return null; }
            return result[0];
        }
    }
}
