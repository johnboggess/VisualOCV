﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NUnitTests")]
namespace VisualOCVBackend.Operators
{
    public class OperatorModule
    {
        #region Static
        public static string ModuleLocation = AppDomain.CurrentDomain.BaseDirectory + "Operators/";
        public static OperatorModule RootModule;
        public static List<string> DisabledModules = new List<string>();
        public static Dictionary<string, OperatorType> AllOperatorTypes = new Dictionary<string, OperatorType>();


        static Workspace Workspace;
        //Errors that occured while the modules where loading
        static List<Exception> Exceptions = new List<Exception>();

        public static void LoadModules(Workspace workspace)
        {
            Exceptions.Clear();

            Workspace = workspace;
            DisabledModules = File.ReadAllLines("DisabledModules.txt").ToList();

            for(int i = 0; i < DisabledModules.Count; i++)
            {
                DisabledModules[i] = ModuleLocation + DisabledModules[i];
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(ModuleLocation);
            RootModule = _LoadModulesFromDirectory(directoryInfo, null);
            Workspace.GUIListeners.ExceptionOccured(Exceptions);
            Exceptions.Clear();
        }
        
        public static OperatorModule FindModule(string Name)
        {
            return RootModule.FindSubmodule(Name);
        }

        public static OperatorType FindOperatorType(string moduleName, string opTypeID)
        {
            OperatorModule module = FindModule(moduleName);
            return FindOperatorType(module, opTypeID);
        }

        public static OperatorType FindOperatorType(OperatorModule module, string opTypeID)
        {
            return module.FindOperatorType(opTypeID);
        }

        internal static OperatorModule _LoadModulesFromDirectory(DirectoryInfo directoryInfo, OperatorModule parent)
        {
            FileInfo[] xmls = directoryInfo.GetFiles();

            OperatorModule module = new OperatorModule(directoryInfo.Name, parent != null ? parent.FullName + "." + directoryInfo.Name : directoryInfo.Name, parent);
            
            foreach(FileInfo file in xmls)
            {
                if(file.Extension == ".xml")
                {
                    Tuple<List<OperatorType>, List<Exception>> readResult = OperatorXmlReader.Read(Workspace, file.FullName, module);
                    List<OperatorType> operatorsToAdd = readResult.Item1;
                    Exceptions.AddRange(readResult.Item2);

                    foreach (OperatorType op in operatorsToAdd)
                    {
                        if (!AllOperatorTypes.ContainsKey(op.ID))
                        {
                            module.OperatorTypes.Add(op.ID, op);
                            AllOperatorTypes.Add(op.ID, op);
                        }
                        else
                        {
                            string errorMessage = "Unable to load operator " + op.Name +" from module " + module.FullName +": An operator with an id of " + op.ID + " already exists.";
                            Exception e = new Exception(errorMessage);
                            Workspace.GUIListeners.ExceptionOccured(e);
                            continue;
                        }
                    }
                }
            }

            DirectoryInfo[] directories = directoryInfo.GetDirectories();

            foreach(DirectoryInfo directory in directories)
            {
                if(DisabledModules.Contains(directory.FullName)) { continue; }
                module.SubModules.Add(_LoadModulesFromDirectory(directory, module));
            }
            return module;
        }
        #endregion

        public OperatorModule Parent;
        public List<OperatorModule> SubModules = new List<OperatorModule>();
        public Dictionary<string, OperatorType> OperatorTypes = new Dictionary<string, OperatorType>();
        public string Name;
        public string FullName;

        public OperatorModule(string name, string FullName, OperatorModule parent)
        {
            Parent = parent;
            Name = name;
            this.FullName = FullName;
        }

        public int OperatorTypeCount()
        {
            return OperatorTypes.Count;
        }

        public OperatorModule FindSubmodule(string Name)
        {
            int index = Name.IndexOf('.');
            if(index < 0)
            {
                if(Name == this.Name) { return this; }
                return null;
            }
            
            for(int i = 0; i < SubModules.Count; i++)
            {
                OperatorModule module = SubModules[i].FindSubmodule(Name.Substring(index+1, Name.Length - index-1));
                if(module!=null)
                {
                    return module;
                }
            }
            return null;
        }

        public OperatorType FindOperatorType(string opTypeID, bool recursive = true)
        {
            if(OperatorTypes.ContainsKey(opTypeID))
            {
                return OperatorTypes[opTypeID];
            }
            else if (recursive)
            {
                foreach(OperatorModule module in SubModules)
                {
                    OperatorType type = module.FindOperatorType(opTypeID, recursive);
                    if (type != null)
                        return type;
                }
                return null;
            }
            return null;
        }

        public Dictionary<string, OperatorType> OperatorTypesRecursive()
        {
            Dictionary<string, OperatorType> result = new Dictionary<string, OperatorType>();
            return _OperatorTypesRecursive(ref result);
        }

        internal Dictionary<string, OperatorType> _OperatorTypesRecursive(ref Dictionary<string, OperatorType> result)
        {
            foreach (KeyValuePair<string, OperatorType> keyValuePair in OperatorTypes)
                result.Add(keyValuePair.Key, keyValuePair.Value);

            foreach (OperatorModule module in SubModules)
                module._OperatorTypesRecursive(ref result);
            return result;
        }
    }
}
