﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualOCVBackend.Operators
{
    public class OperatorList
    {
        public static Tuple<OperatorModule, MacroModule> Search(string searchTerm)
        {
            OperatorModule opRoot = OperatorModule.RootModule;
            MacroModule macroRoot = MacroModule.RootModule;
            OperatorModule opModules = Search(null, opRoot, searchTerm);
            MacroModule macroModules = Search(null, macroRoot, searchTerm);

            if (opModules == null)
                opModules = new OperatorModule(opRoot.Name, opRoot.FullName, opRoot.Parent);
            if (macroModules == null)
                macroModules = new MacroModule(macroRoot.Name, macroRoot.FullName, macroRoot.Parent);

            return Tuple.Create(opModules, macroModules);
        }

        private static OperatorModule Search(OperatorModule parent, OperatorModule current, string searchTerm)
        {
            searchTerm = searchTerm.ToLower();
            bool foundSearchTerm = false;

            Dictionary<string, OperatorType> matchingOpTypes = new Dictionary<string, OperatorType>();

            Dictionary<string, OperatorType> moduleOpTypes = current.OperatorTypes;

            foreach (KeyValuePair<string, OperatorType> opType in moduleOpTypes)
            {
                if (opType.Value.Name.ToLower().Contains(searchTerm))
                {
                    matchingOpTypes.Add(opType.Key,opType.Value);
                }
            }

            foundSearchTerm = matchingOpTypes.Count > 0;

            OperatorModule moduleResult = new OperatorModule(current.Name, current.FullName, parent);
            if (matchingOpTypes.Count > 0)
            {
                moduleResult.OperatorTypes = matchingOpTypes;
                foundSearchTerm = true;
            }

            foreach(OperatorModule child in current.SubModules)
            {
                OperatorModule module = Search(current, child, searchTerm);
                if(module != null)
                {
                    moduleResult.SubModules.Add(module);
                    foundSearchTerm = true;
                }
            }

            if (foundSearchTerm)
            {
                return moduleResult;
            }
            else
            {
                return null;
            }
        }
        
        private static MacroModule Search(MacroModule parent, MacroModule current, string searchTerm)
        {
            searchTerm = searchTerm.ToLower();
            bool foundSearchTerm = false;

            Dictionary<string, MacroType> matchingMacroTypes = new Dictionary<string, MacroType>();

            Dictionary<string, MacroType> moduleMacroTypes = current.MacroTypes;

            foreach (KeyValuePair<string, MacroType> macroType in moduleMacroTypes)
            {
                if (macroType.Value.Name.ToLower().Contains(searchTerm))
                {
                    matchingMacroTypes.Add(macroType.Key, macroType.Value);
                }
            }

            foundSearchTerm = matchingMacroTypes.Count > 0;

            MacroModule moduleResult = new MacroModule(current.Name, current.FullName, parent);
            if (matchingMacroTypes.Count > 0)
            {
                moduleResult.MacroTypes = matchingMacroTypes;
                foundSearchTerm = true;
            }

            foreach (MacroModule child in current.SubModules)
            {
                MacroModule module = Search(current, child, searchTerm);
                if (module != null)
                {
                    moduleResult.SubModules.Add(module);
                    foundSearchTerm = true;
                }
            }

            if (foundSearchTerm)
            {
                return moduleResult;
            }
            else
            {
                return null;
            }
        }
    }
}
