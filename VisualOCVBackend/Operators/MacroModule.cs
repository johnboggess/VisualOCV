﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace VisualOCVBackend.Operators
{
    public class MacroModule
    {
        #region Static
        public static string ModuleLocation = AppDomain.CurrentDomain.BaseDirectory + "Macros/";
        public static MacroModule RootModule;
        public static List<string> DisabledModules = new List<string>();
        public static Dictionary<string, MacroType> AllMacroTypes = new Dictionary<string, MacroType>();

        public static void LoadModules(Workspace workspace)
        {
            RootModule = null;
            DisabledModules.Clear();
            AllMacroTypes.Clear();

            DisabledModules = File.ReadAllLines("DisabledModules.txt").ToList();

            for (int i = 0; i < DisabledModules.Count; i++)
            {
                DisabledModules[i] = ModuleLocation + DisabledModules[i];
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(ModuleLocation);
            RootModule = LoadModulesFromDirectory(directoryInfo, null, workspace);
        }

        public static MacroModule FindModule(string Name)
        {
            return RootModule.FindSubmodule(Name);
        }

        public static MacroType FindMacroType(string moduleName, string  macroTypeID)
        {
            MacroModule module = FindModule(moduleName);
            return FindMacroType(module, macroTypeID);
        }

        public static MacroType FindMacroType(MacroModule module, string macroTypeID)
        {
            return module.FindMacroType(macroTypeID);
        }

        private static MacroModule LoadModulesFromDirectory(DirectoryInfo directoryInfo, MacroModule parent, Workspace workspace)
        {
            FileInfo[] xmls = directoryInfo.GetFiles();

            MacroModule module = new MacroModule(directoryInfo.Name, parent != null ? parent.FullName + "." + directoryInfo.Name : directoryInfo.Name, parent);

            foreach (FileInfo file in xmls)
            {
                if (file.Extension == ".xml")
                {
                    MacroType macroToAdd = new MacroType(module, file.Name.Split('.')[0]);//OperatorXmlReader.Read(workspace, file.FullName, module);

                    if (!AllMacroTypes.ContainsKey(macroToAdd.ID))
                    {
                        module.MacroTypes.Add(macroToAdd.ID, macroToAdd);
                        AllMacroTypes.Add(macroToAdd.ID, macroToAdd);
                    }
                    else
                    {
                        string errorMessage = "Unable to load operator " + macroToAdd.Name + " from module " + module.FullName + ": An operator with an id of " + macroToAdd.ID + " already exists.";
                        Exception e = new Exception(errorMessage);
                        workspace.GUIListeners.ExceptionOccured(e);
                        continue;
                    }
                }
            }

            DirectoryInfo[] directories = directoryInfo.GetDirectories();

            foreach (DirectoryInfo directory in directories)
            {
                if (DisabledModules.Contains(directory.FullName)) { continue; }
                module.SubModules.Add(LoadModulesFromDirectory(directory, module, workspace));
            }
            return module;
        }
        #endregion

        public MacroModule Parent;
        public List<MacroModule> SubModules = new List<MacroModule>();
        public Dictionary<string, MacroType> MacroTypes = new Dictionary<string, MacroType>();
        public string Name;
        public string FullName;
        
        public MacroModule(string name, string FullName, MacroModule parent)
        {
            Parent = parent;
            Name = name;
            this.FullName = FullName;
        }

        public MacroModule FindSubmodule(string Name)
        {
            int index = Name.IndexOf('.');
            if (index < 0)
            {
                if (Name == this.Name) { return this; }
                return null;
            }

            for (int i = 0; i < SubModules.Count; i++)
            {
                MacroModule module = SubModules[i].FindSubmodule(Name.Substring(index + 1, Name.Length - index - 1));
                if (module != null)
                {
                    return module;
                }
            }
            return null;
        }
        
        public MacroType FindMacroType(string opTypeID, bool recursive = true)
        {
            if (MacroTypes.ContainsKey(opTypeID))
            {
                return MacroTypes[opTypeID];
            }
            else if(recursive)
            {
                foreach (MacroModule module in SubModules)
                {
                    MacroType type = module.FindMacroType(opTypeID, recursive);
                    if (type != null)
                        return type;
                }
                return null;
            }
            return null;
        }

        public Dictionary<string, MacroType> MacroTypesRecursive()
        {
            Dictionary<string, MacroType> result = new Dictionary<string, MacroType>();
            return _MacroTypesRecursive(ref result);
        }

        internal Dictionary<string, MacroType> _MacroTypesRecursive(ref Dictionary<string, MacroType> result)
        {
            foreach (KeyValuePair<string, MacroType> keyValuePair in MacroTypes)
                result.Add(keyValuePair.Key, keyValuePair.Value);

            foreach (MacroModule module in SubModules)
                module._MacroTypesRecursive(ref result);
            return result;
        }
    }
}
