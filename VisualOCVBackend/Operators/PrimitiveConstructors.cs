﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Operators
{
    /// <summary>
    /// Some primitives have no constructors, e.g. Boolean
    /// This provides some methods for operators to invoke to create these types
    /// </summary>
    public class PrimitiveConstructors
    {
        public static bool CreateBool(bool v)
        {
            return v;
        }

        public static byte CreateByte(byte v)
        {
            return v;
        }

        public static sbyte CreateSByte(sbyte v)
        {
            return v;
        }

        public static char CreateChar(char v)
        {
            return v;
        }

        public static double CreateDouble(double v)
        {
            return v;
        }

        public static float CreateFloat(float v)
        {
            return v;
        }

        public static int CreateInt(int v)
        {
            return v;
        }

        public static uint CreateUInt(uint v)
        {
            return v;
        }

        public static long CreateLong(long v)
        {
            return v;
        }

        public static ulong CreateULong(ulong v)
        {
            return v;
        }

        public static short CreateShort(short v)
        {
            return v;
        }

        public static ushort CreateUShort(ushort v)
        {
            return v;
        }

        public static string CreateString(string v)
        {
            return v;
        }
    }
}
