﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualOCVBackend.Operators
{
    public abstract class OperatorError
    {
        public string Error
        {
            get { return Title() + ": " + ErrorMessage(); }
        }
        public abstract string Title();
        public abstract string ErrorMessage();
        
    }

    public class UnkownError : OperatorError
    {
        string error;

        public UnkownError(Exception e)
        {
            error = e.Message; ;
        }

        public override string Title()
        {
            return "Unkown Error";
        }

        public override string ErrorMessage()
        {
            return error;
        }
    }

    public class NotAllInputAssigned : OperatorError
    {
        public override string Title()
        {
            return "Unassigned Input";
        }

        public override string ErrorMessage()
        {
            return "One for more inputs is not assigned a value.";
        }
    }

    public class OperatorTypeMismatch : OperatorError
    {
        Type ExpectedType;
        Type ReceviedType;
        string InputName;

        public OperatorTypeMismatch(Type expectedType, Type receviedType, OperatorInput operatorInput)
        {
            ExpectedType = expectedType;
            ReceviedType = receviedType;
            InputName = operatorInput.Name;
        }

        public override string Title()
        {
            return "Type Mismatch";
        }

        public override string ErrorMessage()
        {

            return "Operator input " + InputName + " received " + _getName(ReceviedType) + " but expects a " + _getName(ExpectedType);
        }

        string _getName(Type type)
        {
            string result = "";
            Type[] _params = type.GetGenericArguments();
            result += type.Name;
            if(_params.Count() > 0)
            {
                result += "<";
                foreach (Type param in _params)
                    result += _getName(param) + ",";
                result = result.Remove(result.Length-1, 1) + ">";
            }
            return result;
        }
    }

    public class OperatorInputNotValid : OperatorError
    {
        string InputName;
        string errorMessage;

        public OperatorInputNotValid(string inputName, string errorMessage)
        {
            this.errorMessage = errorMessage;
            InputName = inputName;
        }

        public override string Title()
        {
            return "Invalid Input";
        }

        public override string ErrorMessage()
        {
            return InputName+" - "+errorMessage;
        }
    }

    public class RequiredInputNotDefined : OperatorError
    {
        string InputName;

        public RequiredInputNotDefined(string inputName)
        {
            InputName = inputName;
        }

        public override string Title()
        {
            return "Required Input Is Undefined";
        }

        public override string ErrorMessage()
        {
            return "Input " + InputName + " is undefined";
        }
    }

    public class MacroInternalError : OperatorError
    {
        OperatorError internalError;
        Operator op;
        public MacroInternalError(Operator op, OperatorError internalError)
        {
            this.op = op;
            this.internalError = internalError;
        }

        public override string Title()
        {
            return "Internal Error";
        }

        public override string ErrorMessage()
        {
            return "Internal Macro Error for "+op.Name+": " + internalError.Error;
        }


    }
}
