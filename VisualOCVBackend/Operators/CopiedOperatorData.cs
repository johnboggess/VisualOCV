﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Operators
{
    class CopiedOperatorData
    {
        public List<object> Values = new List<object>();
        public Guid CopiedInstanceID;
        public OperatorType CopiedOperatorTypeID;
        public MacroType CopiedMacroTypeID;
        public List<CopiedConnectionData> Connections = new List<CopiedConnectionData>();
        public float X;
        public float Y;

        public void Copy(Operator op)
        {
            CopiedInstanceID = op.InstanceID;
            if (!(op is Macro))
            {
                CopiedOperatorTypeID = op.OperatorType;
            }
            else if(op is Macro)
            {
                CopiedMacroTypeID = ((Macro)op).MacroType;
            }

            X = op.X;
            Y = op.Y;

            for(int i = 0; i < op.Inputs.Count; i++)
            {
                if(op.Inputs[i].Connection == null)
                {
                    Values.Add(op.Inputs[i].Value);
                }
                else
                { 
                    Values.Add(null);
                }
            }

            for (int i = 0; i < op.Outputs.Count; i++)
            {
                for (int j = 0; j < op.Outputs[i].Connections.Count; j++)
                {
                    OperatorInput target = op.Outputs[i].Connections[j];
                    CopiedConnectionData connectionData = new CopiedConnectionData(target.Operator.InstanceID, target.Index , i);
                    Connections.Add(connectionData);
                }
            }
        }

        public void Paste(Operator op, float xOffset, float yOffset)
        {
            if(op.OperatorType != CopiedOperatorTypeID) { return; }

            for(int i = 0; i < Values.Count; i++)
            {
                if(Values[i] != null)
                {
                    op.Inputs[i].Value = Values[i];
                    op.Manager.Workspace.GUIListeners.ControlValueChanged(op.Inputs[i].Control, op.Inputs[i].Value);
                }
            }

            op.Move(xOffset, yOffset);
        }
    }
}
