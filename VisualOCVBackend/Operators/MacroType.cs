﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VisualOCVBackend.Operators
{
    public class MacroType
    {
        public MacroModule Module;
        public string Name
        {
            get { return _name; }
        }
        public string ID
        {
            get { return Module.FullName + "/" + Name; }
        }
        public string LoadData
        {
            get { return _loadData; }
        }
        public int Width = 150;

        private string _name;
        private string _loadData;

        public MacroType(MacroModule module, string fileName)
        {
            Module = module;
            _name = fileName;
            _loadData = File.ReadAllText(module.FullName + "/" + fileName+".xml");
        }

        public Operator CreateInstance(AlgorithmManager manager)
        {
            return CreateInstance(manager, Guid.Empty);
        }

        public Operator CreateInstance(AlgorithmManager manager, Guid guid)
        {
            return manager.NewMacroInstance(this,guid);
        }
    }
}
