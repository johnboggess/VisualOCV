﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Controls;

namespace VisualOCVBackend.Operators
{
    public class OperatorInputOutput
    {
        public bool IsGenericDefinition { get { return _valueType.IsGenericTypeDefinition; } }
        public Type SpecificValueType { get { return _specificValueType; } }

        public ControlType ControlType;
        public Control Control;
        public string Name;
        public Operator Operator;
        

        public bool Error;
        public OperatorError OperatorError;

        private object _value = null;
        public Type _specificValueType;
        public Type _valueType;

        /// <summary>
        /// Value of the Input/Output. Do not set directly if the value being assigned is not confirmed to be the correct type.
        /// </summary>
        public object Value
        {
            get { return _value; }
            set
            {
                if (value == null) { return; }
                _value = value;
            }
        }

        public Type GetValueType()
        {
            return GetValueType(IsGenericDefinition);
        }

        public Type GetValueType(bool getSpecificValueType)
        {
            if(getSpecificValueType) { return _specificValueType; }
            else { return _valueType; }
        }

        public void SetValueType(Type type)
        {
            _valueType = type;
        }

        public void SpecifyGenericDefinition(Type definition)
        {
            if (IsGenericDefinition)
            {
                _specificValueType = Utilities.GenericTypeFillParameters(_valueType, definition);
            }
        }

        public void ClearGenericDefinition(Type definition)
        {
            _specificValueType = null;
        }

        public OperatorInputOutput(Operator op)
        {
            Operator = op;
        }

        public void ClearValue()
        {
            //IsValueNull = true;
            Error = false;
        }
    }
}
