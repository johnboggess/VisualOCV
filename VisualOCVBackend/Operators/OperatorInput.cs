﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using VisualOCVBackend.Controls;
using VisualOCVBackend.Types;
using VisualOCVBackend.Validators;

namespace VisualOCVBackend.Operators
{
    public class OperatorInput : OperatorInputOutput
    {
        public OperatorOutput Connection;
        public List<Validator> Validators = new List<Validator>();
        public string MaxValue;
        public string MinValue;

        public int Index
        {
            get { return Operator.Inputs.IndexOf(this); }
        }

        public OperatorInput(Operator op, Type paramType) : base(op)
        {
            SetValueType(paramType);
        }

        public void SetValue(object value, bool informListeners)
        {
            value = _castData(value);

            Operator.DataMutex.WaitOne();
            Operator.ShouldReprocess = true;
            if (!Error)
            {
                value = _validateData(value);
                this.Value = value;
            }
            Operator.DataMutex.ReleaseMutex();

            if (informListeners && Operator.Manager.CallCallbacks)
            {
                Operator.Manager.Workspace.GUIListeners.ControlValueChanged(Control, Value);
            }
        }

        /// <summary>
        /// Casts the input to the expceted type, validates it, then informs the operator the input has updated.
        /// </summary>
        /// <param name="value"></param>
        internal void ReceiveData(object value)
        {
            Operator.DataMutex.WaitOne();
            value = _castData(value);
            if (!Error)
            {
                value = _validateData(value);
                this.Value = value;
            }
            Operator.InputUpdated(this);
            Operator.DataMutex.ReleaseMutex();
        }

        /// <summary>
        /// Convert the given object to the type the input expects.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private object _castData(object value)
        {
            Error = false;
            OperatorError = null;

            if (!(value.GetType() == GetValueType(false)))
            {
                object converted = TypeManager.TryConvertTo(value, _valueType);
                if (converted != null)
                {
                    value = converted;
                }
                else
                {
                    if (IsGenericDefinition)
                    {
                        SpecifyGenericDefinition(value.GetType());
                    }

                    try
                    {
                        if (GetValueType(false) != typeof(object))
                        {
                            if (value.GetType().GetInterface("IEnumerable") == null || (value is string))
                            {
                                if (!GetValueType().IsInterface)
                                {
                                    object castValue = Convert.ChangeType(value, GetValueType());
                                    value = castValue;
                                }
                                else if (!Utilities.DoesTypeImplementInterface(value.GetType(), GetValueType()))
                                {
                                    throw new Exception();
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Error = true;
                        OperatorError = new OperatorTypeMismatch(GetValueType(), value.GetType(), this);
                    }
                }
            }
            return value;
        }

        /// <summary>
        /// Is the input valid?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private object _validateData(object value)
        {
            Error = false;
            OperatorError = null;

            if (!Error)
            {
                foreach (Validator validator in Validators)
                {
                    Tuple<bool, string> valid = (Tuple<bool, string>)validator.Validate(value);

                    if (valid.Item1 == false)
                    {
                        Error = true;
                        OperatorError = new OperatorInputNotValid(Name, valid.Item2);
                        break;
                    }
                }
            }
            return value;
        }

        public bool Connect(OperatorOutput output)
        {
            Operator.Manager.StopAlgorithm();

            if(Connection == output)
            {
                Operator.Manager.ResumeAlgorithm();
                return true;
            }

            if (!Operator.WillConnectionCauseCycle(Operator, output.Operator))
            {
                RemoveConnection();
                Connection = output;
                output.Connections.Add(this);
                Operator.Manager._MakeRootOrLeaf(Operator);
                Operator.Manager._MakeRootOrLeaf(output.Operator);

                if (Operator.Manager.CallCallbacks)
                {
                    Operator.Manager.Workspace.GUIListeners.OperatorConnectionChanged(this, output, true);
                }

                output.Operator.ShouldReprocess = true;

                Operator.Manager.ResumeAlgorithm();
                return true;
            }
            Operator.Manager.ResumeAlgorithm();
            return false;
        }

        public void RemoveConnection()
        {
            if(Connection == null) { return; }
            Connection.RemoveConnection(this);
            Operator.Manager._MakeRootOrLeaf(Operator);
        }
    }
}
