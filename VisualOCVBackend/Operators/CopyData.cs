﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.Operators
{
    public class CopyData
    {
        List<CopiedOperatorData> copiedOperators = new List<CopiedOperatorData>();
        Dictionary<CopiedOperatorData, Operator> DataToNewOperator = new Dictionary<CopiedOperatorData, Operator>();
        Workspace Workspace;

        float _originX;
        float _originY;

        public CopyData(Workspace workspace)
        {
            Workspace = workspace;
        }

        public void CopyOperators(List<Operator> operators, float originX, float originY)
        {
            _originX = originX;
            _originY = originY;

            DataToNewOperator = new Dictionary<CopiedOperatorData, Operator>();
            copiedOperators = new List<CopiedOperatorData>();
            foreach (Operator op in operators)
            {
                CopiedOperatorData data = new CopiedOperatorData();
                data.Copy(op);
                copiedOperators.Add(data);
            }
        }

        public List<Operator> Paste(float newOriginX, float newOriginY)
        {
            List<Operator> PastedOperators = new List<Operator>();
            for (int i = 0; i < copiedOperators.Count; i++)
            {
                CopiedOperatorData opData = copiedOperators[i];
                Operator newOp = null;
                if (opData.CopiedOperatorTypeID != null) { newOp = opData.CopiedOperatorTypeID.CreateInstance(Workspace.AlgorithmManager); }
                else if (opData.CopiedMacroTypeID != null) { newOp = opData.CopiedMacroTypeID.CreateInstance(Workspace.AlgorithmManager); }
                PastedOperators.Add(newOp);
                DataToNewOperator.Add(opData, newOp);
                newOp.SetPosition(opData.X, opData.Y);
                opData.Paste(newOp, newOriginX - _originX, newOriginY - _originY);
            }

            for (int i = 0; i < copiedOperators.Count; i++)
            {
                CopiedOperatorData opData = copiedOperators[i];
                Operator op = DataToNewOperator[opData];
                for (int j = 0; j < copiedOperators.Count; j++)
                {
                    CopiedOperatorData otherOpData = copiedOperators[j];
                    if(otherOpData == opData) { continue; }
                    Operator otherOp = DataToNewOperator[otherOpData];

                    for (int connection = 0; connection < opData.Connections.Count; connection++)
                    {
                        if(opData.Connections[connection].ConnectedToID == otherOpData.CopiedInstanceID)
                        {
                            op.Connect(otherOp.Inputs[opData.Connections[connection].TargetIndex], op.Outputs[opData.Connections[connection].SourceIndex]);
                        }
                    }
                }
            }
            
            DataToNewOperator = new Dictionary<CopiedOperatorData, Operator>();
            
            return PastedOperators;
        }
    }
}
