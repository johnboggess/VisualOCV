﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;

namespace VisualOCVBackend.HelperFunctions
{
    public class EmguImageHelpers
    {
        public static Image<Rgb, byte> ImageFromFile(string filePath)
        {
            return new Image<Rgb, byte>(filePath);
        }

        public static object ImageOperatorPassThrough(object image)
        {
            return image;
        }

        public static object CloneImage(object image)
        {

            if (image == null) { return null; }
            Type[] genericTypes = image.GetType().GenericTypeArguments;
            MethodInfo methodInfo = typeof(EmguImageHelpers).GetMethod(nameof(CloneImageGeneric));
            return methodInfo.MakeGenericMethod(genericTypes).Invoke(null, new object[] { image });
        }

        public static Image<Rgb, TDepth> Merge<TDepth>(Image<Gray, TDepth> Red, Image<Gray, TDepth> Green, Image<Gray, TDepth> Blue) where TDepth : new()
        {
            return new Image<Rgb, TDepth>(new Image<Gray, TDepth>[] { Red, Green, Blue });
        }

        public static Image<TColor, TDepth> CloneImageGeneric<TColor, TDepth>(Image<TColor, TDepth> image) where TColor : struct, IColor where TDepth : new()
        {
            return image.Clone();
        }

        public static object Convert<TColor, TDepth>(Image<TColor, TDepth> image, VisualOCVOperatorFunctions.TColorEnum color, VisualOCVOperatorFunctions.TDepthEnum depth) where TColor : struct, IColor where TDepth : new()
        {
            Type colorType = Utilities.TColorEnumToType(color);
            Type depthType = Utilities.TDepthEnumToType(depth);
            Type[] genericParams = new Type[] { colorType, depthType };
            MethodInfo info = image.GetType().GetMethod("Convert", new Type[] { }).MakeGenericMethod(genericParams);
            return info.Invoke(image, new object[] { });
        }

        public static IOutputArray FindContours(IInputOutputArray image, Emgu.CV.CvEnum.RetrType retrType, Emgu.CV.CvEnum.ChainApproxMethod chainApproxMethod, System.Drawing.Point offset)
        {
            Emgu.CV.Util.VectorOfVectorOfPoint output = new Emgu.CV.Util.VectorOfVectorOfPoint();
            Mat heirarchy = new Mat();
            Emgu.CV.CvInvoke.FindContours((IInputOutputArray)image, output, heirarchy, Emgu.CV.CvEnum.RetrType.Ccomp, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxNone, offset);
            return output;
        }

        public static IOutputArray DrawContours(IInputOutputArray image, IInputArrayOfArrays contours, int contourIndex, IColor color)
        {
            Emgu.CV.CvInvoke.DrawContours(image, contours, contourIndex, color.MCvScalar);
            return image;
        }

        public static IOutputArray DrawContoursExt(IInputOutputArray image, IInputArrayOfArrays contours, int contourIndex, IColor color, int thickness, Emgu.CV.CvEnum.LineType lineType, int maxLevel, System.Drawing.Point offset)
        {
            Emgu.CV.CvInvoke.DrawContours(image, contours, contourIndex, color.MCvScalar, thickness, lineType, null, maxLevel, offset);
            return image;
        }

        public static IOutputArray ApproxPolyDP(IInputArray input, double epsilon, bool closed)
        {

            Emgu.CV.Mat output = new Mat();
            Emgu.CV.CvInvoke.ApproxPolyDP(input, output, epsilon, closed);
            return output;
        }

        public static object Anaglyph<TColor, TDepth>(Image<TColor, TDepth> imageLeft, Image<TColor, TDepth> imageRight) where TColor : struct, IColor where TDepth : new()
        {
            Image<Rgb, TDepth> imageLeftGray = imageLeft.Convert<Rgb, TDepth>();
            Image<Rgb, TDepth> imageRightGray = imageRight.Convert<Rgb, TDepth>();
            Emgu.CV.Util.VectorOfMat mats = new Emgu.CV.Util.VectorOfMat(new Mat[] { imageLeftGray[0].Mat, imageRightGray[1].Mat, imageRightGray[2].Mat });
            Mat result = new Mat();
            CvInvoke.Merge(mats, result);
            return result.ToImage<Rgb, TDepth>();
        }

        public static object ConvertScale<TColor, TDepth>(Image<TColor, TDepth> image, double scale, double shift, VisualOCVOperatorFunctions.TDepthEnum depthEnum) where TColor : struct, IColor where TDepth : new()
        {
            Type type = Utilities.TDepthEnumToType(depthEnum);
            MethodInfo method = image.GetType().GetMethod("ConvertScale", new Type[] { typeof(double), typeof(double) });
            method = method.MakeGenericMethod(new Type[] { type });
            return method.Invoke(image, new object[] { scale, shift });
        }

        public static Image<TColor, TDepth> DrawCircles<TColor, TDepth>(Image<TColor, TDepth> image, object[] circles, IColor color) where TColor : struct, IColor where TDepth : new()
        {
            Image<TColor, TDepth> result = image.Clone();
            foreach(CircleF circle in circles)
            {
                System.Drawing.Point center = new System.Drawing.Point((int)circle.Center.X, (int)circle.Center.Y);
                CvInvoke.Circle(result, center, (int)circle.Radius, color.MCvScalar);
            }
            return result;
        }

        public static Image<TColor, TDepth> DrawLineSegments<TColor, TDepth>(Image<TColor, TDepth> image, object[] lines, IColor color) where TColor : struct, IColor where TDepth : new()
        {
            Image<TColor, TDepth> result = image.Clone();
            foreach (LineSegment2D[] _lines in lines)
            {
                foreach (LineSegment2D line in _lines)
                {
                    CvInvoke.Line(image, line.P1, line.P2, color.MCvScalar);
                }
            }
            return result;
        }
    }
}
