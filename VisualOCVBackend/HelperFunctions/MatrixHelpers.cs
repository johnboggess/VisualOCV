﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;
namespace VisualOCVBackend.HelperFunctions
{
    class MatrixHelpers
    {
        public static object CreateMatrix(VisualOCVOperatorFunctions.TDepthEnum depth, int rows, int columns)
        {
            Type depthType = Utilities.TDepthEnumToType(depth);
            Type  matrix = typeof(Matrix<byte>).GetGenericTypeDefinition().MakeGenericType(depthType);
            return matrix.GetConstructor(new Type[] { typeof(int), typeof(int) }).Invoke(new object[] { rows, columns });
        }

        public static object CreateMatrix(Array array)
        {
            Type matrix = typeof(Matrix<byte>).GetGenericTypeDefinition().MakeGenericType(array.GetType().GetElementType());
            return matrix.GetConstructor(new Type[] { array.GetType() }).Invoke(new object[] { array });
        }
    }
}
