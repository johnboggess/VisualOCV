﻿using System;
using System.Collections.Generic;
using System.Text;

using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;
namespace VisualOCVBackend.HelperFunctions
{
    class BlobDetectorWrapper
    {
        internal CvBlobDetector _CvBlobDetector;
        CvBlobs _cvBlobs;
        uint _numberOfLabelPixels = 0;

        public BlobDetectorWrapper()
        {
            _CvBlobDetector = new CvBlobDetector();
        }

        public BlobDetectorWrapper Detect(Image<Gray, byte> img)
        {
            _cvBlobs = new CvBlobs();
            _numberOfLabelPixels = _CvBlobDetector.Detect(img, _cvBlobs);
            return this;
        }

        public Image<Bgr, byte> DrawBlobs(Image<Gray, byte> img, CvBlobDetector.BlobRenderType blobRenderType, double alpha)
        {
            return _CvBlobDetector.DrawBlobs(img, _cvBlobs, blobRenderType, alpha);
        }

        public CvBlob GetBlob(uint i)
        {
            return _cvBlobs[i];
        }
    }

    static class Blob
    {
        public static int Area(this CvBlob blob) { return blob.Area; }
        public static System.Drawing.Rectangle BoundingBox(this CvBlob blob) { return blob.BoundingBox; }
        public static System.Drawing.PointF Centroid(this CvBlob blob) { return blob.Centroid; }
        public static uint Label(this CvBlob blob) { return blob.Label; }
        public static double[] GetMoments(this CvBlob blob)
        {
            CvBlob.Moments moments = blob.BlobMoments;
            return new double[] { moments.M00, moments.M01, moments.M02, moments.M10, moments.M11, moments.M20 };
        }
        public static double[] GetNormalizedCentralMoments(this CvBlob blob)
        {
            CvBlob.Moments moments = blob.BlobMoments;
            return new double[] { moments.N02, moments.N11, moments.N20 };
        }
        public static double[] GetCentralMoments(this CvBlob blob)
        {
            CvBlob.Moments moments = blob.BlobMoments;
            return new double[] { moments.U02, moments.U11, moments.U20 };
        }
        public static double[] GetHuMoments(this CvBlob blob)
        {
            CvBlob.Moments moments = blob.BlobMoments;
            return new double[] { moments.P1, moments.P2 };
        }
    }
}
