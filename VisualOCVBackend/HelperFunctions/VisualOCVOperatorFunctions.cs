﻿using System;
using System.Collections.Generic;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;
namespace VisualOCVBackend.HelperFunctions
{
    public class VisualOCVImage<TColor, TDepth> : Image<TColor, TDepth> where TColor : struct, IColor where TDepth : new()
    {
        public static Histogram CalcHistogram<TColor, TDepth>(Image<TColor, TDepth> image) where TColor : struct, IColor where TDepth : new()
        {
            int numberOfChannels = image.NumberOfChannels;
            float[][] hists = new float[numberOfChannels][];
            DenseHistogram[] denseHistograms = new DenseHistogram[numberOfChannels];

            for(int i = 0; i < numberOfChannels; i++)
            {
                DenseHistogram histogram = new DenseHistogram(255, new RangeF(0, 255));
                Image<Gray, byte> channel = image[i].Convert<Gray, byte>();
                histogram.Calculate<byte>(new Image<Gray, byte>[] { channel }, false, null);
                float[] hist = histogram.GetBinValues();
                hists[i] = hist;
                denseHistograms[i] = histogram;
            }
            return new Histogram(hists, denseHistograms);
        }

        public static object HistEqualize<TColor, TDepth>(Image<TColor, TDepth> image) where TColor : struct, IColor where TDepth : new()
        {
            Image<TColor, TDepth> result = image.Clone();
            result._EqualizeHist();
            return result;
        }
    }

    public class VisualOCVOperatorFunctions
    {
        public static object MacroInput(string Name, object Value)
        {
            return Value;
        }

        public static object MacroOutput(string Name, object value) { return value; }
        
        public static Mat CreateMatrix(Emgu.CV.CvEnum.ElementShape shape, int size)
        {
            return CvInvoke.GetStructuringElement(shape, new System.Drawing.Size(size, size), new System.Drawing.Point(size/2, size/2));
        }

        public static Image<Rgb, byte> Camera(int cameraIndex)
        {
            if (!VisualOCVBackend.VideoManager.IsWebCamRunning(cameraIndex)) { VideoManager.StartWebCam(cameraIndex); }
            return VideoManager.QueryWebCamImage(cameraIndex);
        }

        public static Image<Rgb, byte> VideoFile(string filepath)
        {
            return VideoManager.QueryVideoImage(filepath);
        }

        public static IColor CreateIColor(double r, double g, double b, double a, TColorEnum colorType)
        {
            if (colorType == TColorEnum.Rgb) { return new Rgb(r, g, b); }
            else if (colorType == TColorEnum.Rgba) { return new Rgba(r, g, b, a); }
            else if (colorType == TColorEnum.Bgr) { return new Bgr(b, g, r); }
            else if (colorType == TColorEnum.Bgra) { return new Bgra(b, g, r, a); }
            else if (colorType == TColorEnum.Bgr565) { return new Bgra(b, g, r, a); }
            throw new Exception("Invalid Color Type");//todo: support all color types
        }


        public static void ThrowException(string message, bool throwExpresion)
        {
            if(throwExpresion) { throw new Exception(message); }
        }

        public enum TColorEnum
        {
            Bgr,
            Bgr565,
            Bgra,
            Gray,
            Hls,
            Hsv,
            Rgb,
            Rgba,
            Luv,
            Lab,
            Ycc,
            Xyz
        }

        public enum TDepthEnum
        {
            SByte,
            Byte,
            Short,
            UShort,
            Int,
            UInt,
            Long,
            ULong,
            Float,
            Decimal,
            Double
        }
    }
}
