﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.HelperFunctions
{
    class CSharpOperators
    {
        public static bool LogicalNegation(bool b)
        {
            return !b;
        }

        public static double Multiply(double left, double right)
        {
            return left * right;
        }

        public static double Division(double left, double right)
        {
            return left / right;
        }

        public static int Modulo(double left, double right)
        {
            return (int)(left % right);
        }

        public static double Add(double left, double right)
        {
            return left + right;
        }

        public static double Subtract(double left, double right)
        {
            return left - right;
        }

        public static bool LessThan(double left, double right)
        {
            return left < right;
        }

        public static bool GreaterThan(double left, double right)
        {
            return left > right;
        }

        public static bool LessThanOrEqual(double left, double right)
        {
            return left <= right;
        }

        public static bool GreaterThanOrEqual(double left, double right)
        {
            return left >= right;
        }

        public static bool Equal(double left, double right)
        {
            return left == right;
        }

        public static bool NotEqual(double left, double right)
        {
            return left != right;
        }

        public static bool LogicalAND(bool left, bool right)
        {
            return left & right;
        }

        public static bool LogicalXOR(bool left, bool right)
        {
            return left ^ right;
        }

        public static bool LogicalOR(bool left, bool right)
        {
            return left | right;
        }
    }
}
