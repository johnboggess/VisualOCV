﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Emgu.CV;
using Emgu.CV.Structure;
namespace VisualOCVBackend.HelperFunctions
{
    public class Histogram
    {
        internal float[][] _Histograms;
        internal DenseHistogram[] _DenseHistograms;
        public Histogram(float[][] histograms, DenseHistogram[] denseHistograms)
        {
            _Histograms = histograms;
            _DenseHistograms = denseHistograms;
        }

        public Image<Gray, float> Draw(uint channel)
        {
            Matrix<byte> matrix = new Matrix<byte>(256, _Histograms[0].Length);
            float max = _Histograms[channel].Max();

            for(int i = 0; i < matrix.Width; i++)
            {
                float height = (_Histograms[channel][i] / max) * (float)matrix.Height;
                for (int h = 1; h < height; h++)
                {
                    matrix[matrix.Height-h, i] = 255;
                }
            }
            return matrix.ToUMat().ToImage<Gray, float>();
        }

        public Image<Gray, TDepth> BackProject<TColor, TDepth>(Image<TColor, TDepth> source, uint channel) where TColor : struct, IColor where TDepth : new()
        {
            return _DenseHistograms[channel].BackProject<TDepth>(new Image<Gray, TDepth>[] { source.Convert<Gray, TDepth>() });
        }

        public static Histogram Copy(object obj)
        {
            Histogram histogram = (Histogram)obj;
            float[][] histogramCopy = (float[][])histogram._Histograms.Clone();
            DenseHistogram[] denseHistograms = new DenseHistogram[histogram._DenseHistograms.Length];
            for(int i = 0; i < histogram._DenseHistograms.Length; i++)
            {
                byte[] data = new byte[histogram._DenseHistograms[0].Height];
                float[] scrData = histogram._DenseHistograms[i].GetBinValues();
                for (int j = 0; j < scrData.Length; j++)
                    data[i] = (byte)scrData[i];

                Matrix<byte>[] datas = new Matrix<byte>[] { new Matrix<byte>(data) };

                denseHistograms[i] = new DenseHistogram(255, new RangeF(0, 255));
                denseHistograms[i].Calculate<byte>(datas, false, null);
            }
            return new Histogram(histogramCopy, denseHistograms);
        }
    }
}
