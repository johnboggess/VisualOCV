﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.HelperFunctions
{
    class PersistentData
    {
        static Dictionary<string, object> _storage = new Dictionary<string, object>();

        public static object StoreRecall(string varName, object newValue)
        {
            if (!_storage.ContainsKey(varName))
                _storage.Add(varName, null);
            object result = _storage[varName];
            _storage[varName] = newValue;
            return result;
        }

        public static void Store(string varName, object newValue)
        {
            if (!_storage.ContainsKey(varName))
                _storage.Add(varName, null);
            _storage[varName] = newValue;
        }

        public static object Recall(string varName)
        {
            if (!_storage.ContainsKey(varName))
                return null;
            return _storage[varName];
        }
    }
}
