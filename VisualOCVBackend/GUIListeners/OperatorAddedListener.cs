﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.GUIListeners
{
    /// <summary>
    /// An operator has been added to the workspace
    /// </summary>
    public interface OperatorAddedListener
    {
        void OperatorAdded(Operator op);
    }
}
