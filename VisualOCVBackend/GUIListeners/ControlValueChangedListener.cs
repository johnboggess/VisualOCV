﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisualOCVBackend.GUIListeners
{
    /// <summary>
    /// A control's value has changed
    /// </summary>
    public interface ControlValueChangedListener
    {
        bool ControlValueChanged(Controls.Control control, object value);
    }
}
