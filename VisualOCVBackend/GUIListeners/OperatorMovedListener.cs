﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.GUIListeners
{
    public interface OperatorMovedListener
    {
        bool OperatorMoved(Operator op, float newX, float newY, float oldX, float oldY);
    }
}
