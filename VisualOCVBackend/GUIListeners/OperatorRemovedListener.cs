﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.GUIListeners
{
    public interface OperatorRemovedListener
    {
        void OperatorRemoved(Operator op);
    }
}
