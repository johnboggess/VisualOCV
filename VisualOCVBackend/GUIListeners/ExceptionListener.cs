﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Exceptions;

namespace VisualOCVBackend.GUIListeners
{
    /// <summary>
    /// An exception as occured. These exceptions may not be fatal, just something the user needs to be informed of. Use <see cref="VisualOCVException.IsFatal(Exception)"/> to check if the excpetion is fatal.
    /// </summary>
    public interface ExceptionListener
    {
        bool ExceptionOccured(Exception e);
        bool ExceptionOccured(List<Exception> e);
    }
}
