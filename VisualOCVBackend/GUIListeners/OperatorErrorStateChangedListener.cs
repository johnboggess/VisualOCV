﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.GUIListeners
{
    public interface OperatorErrorStateChangedListener
    {
        bool OperatorErrorStateChanged(Operator op, bool error);
    }
}
