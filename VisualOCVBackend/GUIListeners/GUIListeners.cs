﻿using System;
using System.Collections.Generic;
using System.Threading;

using VisualOCVBackend.Operators;
using VisualOCVBackend.Exceptions;

namespace VisualOCVBackend.GUIListeners
{
    public class GUIListeners
    {
        #region Operator Moved
        internal Mutex OpMovedMutex = new Mutex();
        List<OperatorMovedListener> operatorMovedListeners = new List<OperatorMovedListener>();

        public void AddOperatorMovedListener(OperatorMovedListener operatorMovedListener)
        {
            OpMovedMutex.WaitOne();
            if(!operatorMovedListeners.Contains(operatorMovedListener))
            {
                operatorMovedListeners.Add(operatorMovedListener);
            }
            OpMovedMutex.ReleaseMutex();
        }
        
        public void RemoveOperatorMovedListener(OperatorMovedListener operatorMovedListener)
        {
            OpMovedMutex.WaitOne();
            operatorMovedListeners.Remove(operatorMovedListener);
            OpMovedMutex.ReleaseMutex();
        }
        
        public void OperatorMoved(Operator op, float newX, float newY, float oldX, float oldY)
        {
            OpMovedMutex.WaitOne();
            foreach (OperatorMovedListener operatorMovedListener in operatorMovedListeners)
            {
                if(operatorMovedListener.OperatorMoved(op, newX, newY, oldX, oldY))
                {
                    OpMovedMutex.ReleaseMutex();
                    return;
                }
            }
            OpMovedMutex.ReleaseMutex();
        }
        #endregion

        #region Control Value Changed
        internal Mutex ControlValueChangedMutex = new Mutex();
        List<ControlValueChangedListener> controlValueChangedListeners = new List<ControlValueChangedListener>();

        public void AddControlValueChangedListeners(ControlValueChangedListener controlValueChangedListener)
        {
            ControlValueChangedMutex.WaitOne();
            if (!controlValueChangedListeners.Contains(controlValueChangedListener))
            {
                controlValueChangedListeners.Add(controlValueChangedListener);
            }
            ControlValueChangedMutex.ReleaseMutex();
        }

        public void RemoveControlValueChangedListeners(ControlValueChangedListener controlValueChangedListener)
        {
            ControlValueChangedMutex.WaitOne();
            controlValueChangedListeners.Remove(controlValueChangedListener);
            ControlValueChangedMutex.ReleaseMutex();
        }

        public void ControlValueChanged(Controls.Control control, object value)
        {
            try
            {
                ControlValueChangedMutex.WaitOne();
                foreach (ControlValueChangedListener controlValueChangedListener in controlValueChangedListeners)
                {
                    if (controlValueChangedListener.ControlValueChanged(control, value))
                    {
                        ControlValueChangedMutex.ReleaseMutex();
                        return;
                    }
                }
                ControlValueChangedMutex.ReleaseMutex();
            }
            catch { ControlValueChangedMutex.ReleaseMutex(); }
        }
        #endregion

        #region Operator Error State Changed
        internal Mutex OperatorErrorStateChangedMutex = new Mutex();
        List<OperatorErrorStateChangedListener> operatorErrorStateChangedListeners = new List<OperatorErrorStateChangedListener>();

        public void AddOperatorErrorStateChangedListeners(OperatorErrorStateChangedListener operatorErrorStateChangedListener)
        {
            OperatorErrorStateChangedMutex.WaitOne();
            if (!operatorErrorStateChangedListeners.Contains(operatorErrorStateChangedListener))
            {
                operatorErrorStateChangedListeners.Add(operatorErrorStateChangedListener);
            }
            OperatorErrorStateChangedMutex.ReleaseMutex();
        }

        public void RemoveOperatorErrorStateChangedListeners(OperatorErrorStateChangedListener operatorErrorStateChangedListener)
        {
            OperatorErrorStateChangedMutex.WaitOne();
            operatorErrorStateChangedListeners.Remove(operatorErrorStateChangedListener);
            OperatorErrorStateChangedMutex.ReleaseMutex();
        }

        public void OperatorErrorStateChanged(Operator op, bool error)
        {
            try
            {
                OperatorErrorStateChangedMutex.WaitOne();
                foreach (OperatorErrorStateChangedListener operatorErrorStateChangedListener in operatorErrorStateChangedListeners)
                {
                    if (operatorErrorStateChangedListener.OperatorErrorStateChanged(op, error))
                    {
                        OperatorErrorStateChangedMutex.ReleaseMutex();
                        return;
                    }
                }
                OperatorErrorStateChangedMutex.ReleaseMutex();
            }
            catch { OperatorErrorStateChangedMutex.ReleaseMutex(); }
        }
        #endregion

        #region Operator Connection Changed
        internal Mutex OperatorConnectionChangedMutex = new Mutex();
        List<OperatorConnectionChangedListener> operatorConnectionChangedListeners = new List<OperatorConnectionChangedListener>();

        public void AddOperatorConnectionChangedListeners(OperatorConnectionChangedListener operatorConnectionChangedListener)
        {
            OperatorConnectionChangedMutex.WaitOne();
            if (!operatorConnectionChangedListeners.Contains(operatorConnectionChangedListener))
            {
                operatorConnectionChangedListeners.Add(operatorConnectionChangedListener);
            }
            OperatorConnectionChangedMutex.ReleaseMutex();
        }

        public void RemoveOperatorConnectionChangedListeners(OperatorConnectionChangedListener operatorConnectionChangedListener)
        {
            OperatorConnectionChangedMutex.WaitOne();
            operatorConnectionChangedListeners.Remove(operatorConnectionChangedListener);
            OperatorConnectionChangedMutex.ReleaseMutex();
        }

        public void OperatorConnectionChanged(OperatorInput target, OperatorOutput source, bool connecitonCreated)
        {
            try
            {
                OperatorConnectionChangedMutex.WaitOne();
                foreach (OperatorConnectionChangedListener operatorConnectionChangedListener in operatorConnectionChangedListeners)
                {
                    operatorConnectionChangedListener.OperatorConnectionChanged(target, source, connecitonCreated);
                }
                OperatorConnectionChangedMutex.ReleaseMutex();
            }
            catch { OperatorConnectionChangedMutex.ReleaseMutex(); }
        }
        #endregion

        #region Operator Removed
        Mutex OperatorRemovedMutex = new Mutex();
        List<OperatorRemovedListener> operatorRemovedListeners = new List<OperatorRemovedListener>();

        public void AddOperatorRemovedListener(OperatorRemovedListener operatorRemovedListener)
        {
            OperatorRemovedMutex.WaitOne();
            if (!operatorRemovedListeners.Contains(operatorRemovedListener))
            {
                operatorRemovedListeners.Add(operatorRemovedListener);
            }
            OperatorRemovedMutex.ReleaseMutex();
        }

        public void RemoveOperatorRemovedListener(OperatorRemovedListener operatorRemovedListener)
        {
            OperatorRemovedMutex.WaitOne();
            operatorRemovedListeners.Remove(operatorRemovedListener);
            OperatorRemovedMutex.ReleaseMutex();
        }

        public void OperatorRemoved(Operator op)
        {
            try
            {
                OperatorRemovedMutex.WaitOne();
                foreach (OperatorRemovedListener operatorRemovedListener in operatorRemovedListeners)
                {
                    operatorRemovedListener.OperatorRemoved(op);
                }
                OperatorRemovedMutex.ReleaseMutex();
            }
            catch { OperatorRemovedMutex.ReleaseMutex(); }
        }
        #endregion

        #region Operator Added
        Mutex OperatorAddedMutex = new Mutex();
        List<OperatorAddedListener> operatorAddedListeners = new List<OperatorAddedListener>();

        public void AddOperatorAddedListener(OperatorAddedListener operatorAddListener)
        {
            OperatorAddedMutex.WaitOne();
            if (!operatorAddedListeners.Contains(operatorAddListener))
            {
                operatorAddedListeners.Add(operatorAddListener);
            }
            OperatorAddedMutex.ReleaseMutex();
        }

        public void RemoveOperatorAddedListener(OperatorAddedListener operatorAddListener)
        {
            OperatorAddedMutex.WaitOne();
            operatorAddedListeners.Remove(operatorAddListener);
            OperatorAddedMutex.ReleaseMutex();
        }

        public void OperatorAdded(Operator op)
        {
            try
            {
                OperatorAddedMutex.WaitOne();
                foreach (OperatorAddedListener operatorAddedListener in operatorAddedListeners)
                {
                    operatorAddedListener.OperatorAdded(op);
                }
                OperatorAddedMutex.ReleaseMutex();
            }
            catch { OperatorAddedMutex.ReleaseMutex(); }//todo: handle
        }
        #endregion

        #region Exception Occured
        Mutex ExceptionOccuredMutex = new Mutex();
        List<ExceptionListener> ExceptionOccuredListeners = new List<ExceptionListener>();

        public void AddExceptionOccuredListener(ExceptionListener exceptionOccuredListener)
        {
            ExceptionOccuredMutex.WaitOne();
            if (!ExceptionOccuredListeners.Contains(exceptionOccuredListener))
            {
                ExceptionOccuredListeners.Add(exceptionOccuredListener);
            }
            ExceptionOccuredMutex.ReleaseMutex();
        }

        public void RemoveExceptionOccuredListener(ExceptionListener exceptionOccuredListener)
        {
            ExceptionOccuredMutex.WaitOne();
            ExceptionOccuredListeners.Remove(exceptionOccuredListener);
            ExceptionOccuredMutex.ReleaseMutex();
        }

        public void ExceptionOccured(Exception e)
        {
            try
            {
                ExceptionOccuredMutex.WaitOne();
                bool exceptionHandled = false;
                foreach (ExceptionListener exceptionOccuredListener in ExceptionOccuredListeners)
                {
                    exceptionHandled = exceptionHandled || exceptionOccuredListener.ExceptionOccured(e);
                }

                if (!exceptionHandled)
                {
                    throw e;
                }

                ExceptionOccuredMutex.ReleaseMutex();
            }
            catch { ExceptionOccuredMutex.ReleaseMutex(); }
        }

        public void ExceptionOccured(List<Exception> e)
        {
            if(e.Count == 0) { return; }

            ExceptionOccuredMutex.WaitOne();
            bool exceptionHandled = false;
            foreach (ExceptionListener exceptionOccuredListener in ExceptionOccuredListeners)
            {
                exceptionHandled = exceptionHandled || exceptionOccuredListener.ExceptionOccured(e);
            }

            if (!exceptionHandled)
            {
                throw e[0];
            }

            ExceptionOccuredMutex.ReleaseMutex();
        }
        #endregion
        
    }
}
