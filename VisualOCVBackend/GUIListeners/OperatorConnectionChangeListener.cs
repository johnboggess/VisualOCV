﻿using System;
using System.Collections.Generic;
using System.Text;

using VisualOCVBackend.Operators;
namespace VisualOCVBackend.GUIListeners
{
    public interface OperatorConnectionChangedListener
    {
        void OperatorConnectionChanged(OperatorInput target, OperatorOutput source, bool connectionCreated);
    }
}
