﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Reflection;

using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;
using VisualOCVBackend.Operators;
using VisualOCVBackend.HelperFunctions;
using VisualOCVBackend.Types.Helpers;
using System.Linq;
using Emgu.CV.Cvb;

namespace VisualOCVBackend.Tests
{
    public class OperatorProduceCorrectResults
    {
        public static bool LoadImage(Workspace workspace)
        {
            Image<Rgb, byte> image = new Image<Rgb, byte>("Background.jpg");

            Operator op = workspace.NewInstance("EmguCVImageConstructor");
            op.Inputs[0].ReceiveData("Background.jpg");

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Rgb, byte> operatorResult = (Image<Rgb, byte>)op.Outputs[0].Value;

            return operatorResult.Equals(image);
        }

        public static bool Canny(Workspace workspace)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int i1 = random.Next(0, 100);
            int i2 = random.Next(0, 100);

            Image<Rgb, byte> image = new Image<Rgb, byte>("TestImage.jpg");
            Image<Gray, byte> emguCVResult = image.Canny(i1, i2);

            Operator op = workspace.NewInstance("EMGUCVCanny");
            op.Inputs[0].ReceiveData(image);
            op.Inputs[1].ReceiveData(i1);
            op.Inputs[2].ReceiveData(i2);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Gray, byte> operatorResult = (Image<Gray, byte>)op.Outputs[0].Value;

            workspace.Clear();

            return operatorResult.Equals(emguCVResult);
        }

        public static bool Sobel(Workspace workspace)
        {
            Image<Rgb, byte> image = new Image<Rgb, byte>("TestImage.jpg");
            Image<Rgb, float> emguCVResult = image.Sobel(1, 1, 1);

            Operator op = workspace.NewInstance("EMGUCVSobel");
            op.Inputs[0].ReceiveData(image);
            op.Inputs[1].ReceiveData(1);
            op.Inputs[2].ReceiveData(1);
            op.Inputs[3].ReceiveData(1);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Rgb, float> operatorResult = (Image<Rgb, float>)op.Outputs[0].Value;

            return operatorResult.Equals(emguCVResult);
        }

        public static bool CannyDilate(Workspace workspace)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int i1 = random.Next(0, 100);
            int i2 = random.Next(0, 100);
            int i3 = random.Next(0, 3);

            Image<Rgb, byte> image = new Image<Rgb, byte>("TestImage.jpg");
            Image<Gray, byte> emguCVResult = image.Canny(i1,i2).Dilate(i3);

            Operator canny = workspace.NewInstance("EMGUCVCanny");
            Operator dilate = workspace.NewInstance("EMGUCVDilate");
            canny.Inputs[0].ReceiveData(image);
            canny.Inputs[1].ReceiveData(i1);
            canny.Inputs[2].ReceiveData(i2);

            dilate.Inputs[1].ReceiveData(i3);

            canny.Connect(dilate.Inputs[0], canny.Outputs[0]);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Gray, byte> operatorResult = (Image<Gray, byte>)dilate.Outputs[0].Value;

            return operatorResult.Equals(emguCVResult);
        }
    }

    public class OperatorAlgorithmProducesCorrectResults
    {
        public static bool Test(Workspace workspace)
        {
            Image<Rgb, byte> image = new Image<Rgb, byte>("TestImage.jpg");
            Rgb lower = new Rgb(0, 160.415, 125.407);
            Rgb higher = new Rgb(158.55, 255, 255);
            image = image.Resize(200, 200, Emgu.CV.CvEnum.Inter.Area);
            Image<Gray, byte> grayImage = image.InRange(lower, higher); 
            grayImage = grayImage.Dilate(2);

            Operator imageOp = workspace.NewInstance("EmguCVImageConstructor");
            Operator resizeOp = workspace.NewInstance("ResizeImage");
            Operator inRangeOp = workspace.NewInstance("InRangeColor");
            Operator lowerOp = workspace.NewInstance("EmguCVCreateIColor");
            Operator higherOp = workspace.NewInstance("EmguCVCreateIColor");
            Operator dilateOp = workspace.NewInstance("EMGUCVDilate");

            imageOp.Inputs[0].Value = "TestImage.jpg";
            resizeOp.Inputs[1].Value = 200;
            resizeOp.Inputs[2].Value = 200;
            resizeOp.Inputs[3].Value = Emgu.CV.CvEnum.Inter.Area;
            lowerOp.Inputs[0].Value = lower.Red;
            lowerOp.Inputs[1].Value = lower.Green;
            lowerOp.Inputs[2].Value = lower.Blue;
            higherOp.Inputs[0].Value = higher.Red;
            higherOp.Inputs[1].Value = higher.Green;
            higherOp.Inputs[2].Value = higher.Blue;
            dilateOp.Inputs[1].Value = 2;

            imageOp.Connect(resizeOp.Inputs[0], imageOp.Outputs[0]);
            inRangeOp.Connect(inRangeOp.Inputs[0], resizeOp.Outputs[0]);
            lowerOp.Connect(inRangeOp.Inputs[1], lowerOp.Outputs[0]);
            higherOp.Connect(inRangeOp.Inputs[2], higherOp.Outputs[0]);

            dilateOp.Connect(dilateOp.Inputs[0], inRangeOp.Outputs[0]);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            return grayImage.Equals((Image<Gray,byte>)dilateOp.Outputs[0].Value);
        }
    }

    public class GreenScreenTest
    {
        public static bool SpeedTest(Workspace workspace)
        {

            long EMGUCvTime = 0;
            long VisualOCVTime = 0;
            Stopwatch stopwatch = new Stopwatch();

            //load images into cache
            Image<Rgb, byte> cache1 = new Image<Rgb, byte>("Foreground.jpg");
            Image<Rgb, byte> cache2 = new Image<Rgb, byte>("Background.jpg");

            stopwatch.Start();

            Image<Rgb, byte> greenScreen = new Image<Rgb, byte>("Foreground.jpg");
            Image<Rgb, byte> background = new Image<Rgb, byte>("Background.jpg");
            Rgb lower = new Rgb(53.518, 0, 0);
            Rgb higher = new Rgb(255, 255, 255);

            greenScreen = greenScreen.Resize(300, 175, Emgu.CV.CvEnum.Inter.Linear);
            background = background.Resize(300, 175, Emgu.CV.CvEnum.Inter.Linear);
            Image<Gray, byte> personMask = greenScreen.InRange(lower, higher);
            Image<Gray, byte> backgroundMask = personMask.Not();

            Image<Rgb, float> coloredPersonMask = personMask.Convert<Rgb, float>();
            Image<Rgb, float> coloredBackgroundMask = backgroundMask.Convert<Rgb, float>();
            Image<Rgb, float> greenScreenFloat = greenScreen.Convert<Rgb, float>();
            Image<Rgb, float> backgroundFloat = background.Convert<Rgb, float>();

            greenScreenFloat = greenScreenFloat.Mul(coloredPersonMask);
            backgroundFloat = backgroundFloat.Mul(coloredBackgroundMask);

            Image<Rgb, float> result = greenScreenFloat.Add(backgroundFloat);
            stopwatch.Stop();
            EMGUCvTime = stopwatch.ElapsedMilliseconds;
            stopwatch.Reset();

            CreateGreenScreenAlgorithm(workspace);

            stopwatch = new Stopwatch();
            stopwatch.Start();
            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }
            stopwatch.Stop();
            VisualOCVTime = stopwatch.ElapsedMilliseconds;

            double ratio = (double)VisualOCVTime / (double)EMGUCvTime;

            return ratio < 1.5;
        }

        public static void CreateGreenScreenAlgorithm(Workspace workspace)
        {
            Operator greenScreenOp = workspace.NewInstance("EmguCVImageConstructor");
            Operator backgroundOp = workspace.NewInstance("EmguCVImageConstructor");
            Operator greenScreenResizeOp = workspace.NewInstance("ResizeImage");
            Operator backgroundResizeOp = workspace.NewInstance("ResizeImage");
            Operator lowerOp = workspace.NewInstance("EmguCVCreateIColor");
            Operator higherOp = workspace.NewInstance("EmguCVCreateIColor");
            Operator inRangeOp = workspace.NewInstance("InRangeColor");
            Operator backgroundMaskOp = workspace.NewInstance("NotImage");
            Operator greenScreenToFloatOp = workspace.NewInstance("EMGUConvertImage");
            Operator personMaskToRGBFloatOp = workspace.NewInstance("EMGUConvertImage");
            Operator backgroundMaskToColoredFloatOp = workspace.NewInstance("EMGUConvertImage");
            Operator backgroundToFloatOp = workspace.NewInstance("EMGUConvertImage");
            Operator colorPersonMaskOp = workspace.NewInstance("MultiplyImages");
            Operator colorBackgroundMaskOp = workspace.NewInstance("MultiplyImages");
            Operator resultOp = workspace.NewInstance("AddImages");

            greenScreenOp.Inputs[0].Value = "Foreground.jpg";

            lowerOp.Inputs[0].Value = 53.518;
            lowerOp.Inputs[1].Value = 0;
            lowerOp.Inputs[2].Value = 0;
            higherOp.Inputs[0].Value = 255;
            higherOp.Inputs[1].Value = 255;
            higherOp.Inputs[2].Value = 255;
            greenScreenResizeOp.Inputs[1].Value = 300;
            greenScreenResizeOp.Inputs[2].Value = 175;
            backgroundResizeOp.Inputs[1].Value = 300;
            backgroundResizeOp.Inputs[2].Value = 175;
            personMaskToRGBFloatOp.Inputs[2].Value = VisualOCVOperatorFunctions.TDepthEnum.Float;
            greenScreenToFloatOp.Inputs[2].Value = VisualOCVOperatorFunctions.TDepthEnum.Float;
            backgroundMaskToColoredFloatOp.Inputs[2].Value = VisualOCVOperatorFunctions.TDepthEnum.Float;
            backgroundToFloatOp.Inputs[2].Value = VisualOCVOperatorFunctions.TDepthEnum.Float;

            greenScreenResizeOp.Connect(greenScreenResizeOp.Inputs[0], greenScreenOp.Outputs[0]);
            inRangeOp.Connect(inRangeOp.Inputs[0], greenScreenResizeOp.Outputs[0]);
            inRangeOp.Connect(inRangeOp.Inputs[1], lowerOp.Outputs[0]);
            inRangeOp.Connect(inRangeOp.Inputs[2], higherOp.Outputs[0]);

            personMaskToRGBFloatOp.Connect(personMaskToRGBFloatOp.Inputs[0], inRangeOp.Outputs[0]);
            greenScreenToFloatOp.Connect(greenScreenToFloatOp.Inputs[0], greenScreenResizeOp.Outputs[0]);
            colorPersonMaskOp.Connect(colorPersonMaskOp.Inputs[0], personMaskToRGBFloatOp.Outputs[0]);
            colorPersonMaskOp.Connect(colorPersonMaskOp.Inputs[1], greenScreenToFloatOp.Outputs[0]);

            backgroundOp.Inputs[0].Value = "Background.jpg";
            backgroundResizeOp.Connect(backgroundResizeOp.Inputs[0], backgroundOp.Outputs[0]);
            backgroundToFloatOp.Connect(backgroundToFloatOp.Inputs[0], backgroundResizeOp.Outputs[0]);
            backgroundMaskOp.Connect(backgroundMaskOp.Inputs[0], inRangeOp.Outputs[0]);
            backgroundMaskToColoredFloatOp.Connect(backgroundMaskToColoredFloatOp.Inputs[0], backgroundMaskOp.Outputs[0]);
            colorBackgroundMaskOp.Connect(colorBackgroundMaskOp.Inputs[0], backgroundMaskToColoredFloatOp.Outputs[0]);
            colorBackgroundMaskOp.Connect(colorBackgroundMaskOp.Inputs[1], backgroundToFloatOp.Outputs[0]);

            resultOp.Connect(resultOp.Inputs[0], colorPersonMaskOp.Outputs[0]);
            resultOp.Connect(resultOp.Inputs[1], colorBackgroundMaskOp.Outputs[0]);
        }
    }

    public class UtilitiesTest
    {
        public static bool TColorEnumToType()
        {
            bool Bgr = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Bgr) == typeof(Bgr));
            bool Bgr565 = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Bgr565) == typeof(Bgr565));
            bool Bgra = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Bgra) == typeof(Bgra));
            bool Gray = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Gray) == typeof(Gray));
            bool Hls = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Hls) == typeof(Hls));
            bool Hsv = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Hsv) == typeof(Hsv));
            bool Lab = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Lab) == typeof(Lab));
            bool Luv = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Luv) == typeof(Luv));
            bool Rgb = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Rgb) == typeof(Rgb));
            bool Rgba = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Rgba) == typeof(Rgba));
            bool Xyz = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Xyz) == typeof(Xyz));
            bool Ycc = (Utilities.TColorEnumToType(VisualOCVOperatorFunctions.TColorEnum.Ycc) == typeof(Ycc));
            return Bgr && Bgr565 && Bgra && Gray && Hls && Hsv && Lab && Luv && Rgb && Rgba && Xyz && Ycc;
        }
    }

    public class VideoManagerTest
    {
        public static bool QueryWebcamImage()
        {
            bool imageCaptured = VideoManager.QueryWebCamImage(0) != null;
            bool exceptionOccured = false;

            try { VideoManager.QueryWebCamImage(-1); }
            catch { exceptionOccured = true; }
            return imageCaptured && exceptionOccured;
        }

        public static bool QueryVideoImage(string filePath)
        {
            return VideoManager.QueryVideoImage(filePath) != null;
        }
    }

    public class TypeHelperTests
    {
        public static bool CloneAndConversionTest()
        {
            Matrix<int> matrix = new Matrix<int>(new int[,] { { 1, 2 }, { 3, 4 } });
            Matrix<int> matrixClone = EMGUCVMatrixHelper.Clone(matrix);
            bool b1 = matrix[0, 0] == matrixClone[0, 0] && matrix[1, 0] == matrixClone[1, 0] && matrix[0, 1] == matrixClone[0, 1] && matrix[1, 1] == matrixClone[1, 1] &&  matrix.Mat != matrixClone.Mat;

            Emgu.CV.Util.VectorOfVectorOfPoint vector = new Emgu.CV.Util.VectorOfVectorOfPoint(new Point[][] 
            { 
                new Point[] { new Point(0,1), new Point(2,3) },
                new Point[] { new Point(4,5), new Point(6,7) },
            });
            Emgu.CV.Util.VectorOfVectorOfPoint vectorClone = EMGUCVVectorOfVectorOfPoint.Clone(vector);
            bool b2 = vector[0][0] == vectorClone[0][0] && vector[1][0] == vectorClone[1][0] && vector[0][1] == vectorClone[0][1] && vector[1][1] == vectorClone[1][1];


            bool b3 = EMGUCVMatrixHelper.ConvertToMat(matrix) == matrix.Mat;

            return b1 && b2 && b3;
        }

        public static bool CloneAndConversionInAlgorithmTest(Workspace workspace)
        {
            Operator matrix = workspace.NewInstance("EMGUCVCreateMatrixFromArray");
            Operator invert = workspace.NewInstance("EMGUCVInvert");

            matrix.Connect(invert.Inputs[0], matrix.Outputs[0]);
            matrix.Connect(invert.Inputs[1], matrix.Outputs[0]);

            matrix.Inputs[0].Value = new float[,] { { 1.432f, 2.58f }, { 3.18f, 4.47f } };

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Matrix<float> original = (Matrix<float>)matrix.Outputs[0].Value;
            Matrix<float> clone = (Matrix<float>)invert.Inputs[0].Value;
            bool b1 =  original[0, 0] == clone[0, 0] && original[1, 0] == clone[1, 0] && original[0, 1] == clone[0, 1] && original[1, 1] == clone[1, 1] && original.Mat != clone.Mat;

            workspace.Clear(); 
            matrix = workspace.NewInstance("EMGUCVCreateMatrixFromArray");
            Operator warpAffine = workspace.NewInstance("EMGUCVWarpAffine");

            matrix.Connect(warpAffine.Inputs[1], matrix.Outputs[0]);

            matrix.Inputs[0].Value = new float[,] { { 1.432f, 2.58f }, { 3.18f, 4.47f } };

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            bool b2 = (warpAffine.Inputs[1].Value is Mat) && ((Matrix<float>)matrix.Outputs[0].Value).Mat != ((Mat)warpAffine.Inputs[1].Value);

            return b1 && b2;
        }
    }

    public class LoadOperatorsTest
    {
        public static bool ConvertImage(Workspace workspace)
        {
            Operator op = workspace.NewInstance("EMGUConvertImage");
            MethodInfo expectedMethod = RuntimeReflectionExtensions.GetRuntimeMethods(typeof(EmguImageHelpers)).Where(m=>m.Name == "Convert").First();
            bool b1 = !op.IsConstructor;
            bool b2 = op.IsMethod;
            bool b3 = op.IsStaticMethod;
            bool b4 = op.Method.MethodInfo == expectedMethod;
            bool b5 = op.Outputs[0].GetValueType(false) == typeof(object);
            bool b6 = op.Inputs[0].ControlType == VisualOCVBackend.Controls.ControlType.None;
            bool b7 = op.Inputs[1].ControlType == VisualOCVBackend.Controls.ControlType.Dropdown;
            bool b8 = op.Inputs[2].ControlType == VisualOCVBackend.Controls.ControlType.Dropdown;
            bool b9 = op.Outputs[0].ControlType == VisualOCVBackend.Controls.ControlType.None;
            bool b10 = ((Controls.Dropdown)op.Inputs[1].Control).DefaultOption == VisualOCVOperatorFunctions.TColorEnum.Rgb.ToString();
            bool b11 = ((Controls.Dropdown)op.Inputs[2].Control).DefaultOption == VisualOCVOperatorFunctions.TDepthEnum.Byte.ToString();

            return b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8 && b9 && b10 && b11;
        }
    }

    public class HelperFunctionTests
    {
        public static bool Blobs(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator cannyOp = workspace.NewInstance("EMGUCVCanny");
            Operator blobDetectorOp = workspace.NewInstance("VisualOCVBlobDetector");
            Operator detectBlobsOp = workspace.NewInstance("VisualOCVDetectBlob");
            Operator getBlobOp = workspace.NewInstance("VisualOCVGetBlob");
            Operator areaOp = workspace.NewInstance("EmguBlobGetArea");
            Operator boundingBoxOp = workspace.NewInstance("EmguBlobGetBoundingBox");
            Operator centroid = workspace.NewInstance("EmguBlobGetCentroid");
            Operator moments = workspace.NewInstance("EmguBlobGetMoments");
            Operator centralMoments = workspace.NewInstance("EmguBlobGetCentralMoments");
            Operator normalizedCentralMoments = workspace.NewInstance("EmguBlobGetNormalizedCentralMoments");
            Operator huMoments = workspace.NewInstance("EmguBlobGetHuMoments");

            imageOp.Outputs[0].Value = new Image<Rgb, byte>("Foreground.jpg");
            cannyOp.Inputs[0].Value = 80;

            imageOp.Connect(cannyOp.Inputs[0], imageOp.Outputs[0]);
            detectBlobsOp.Connect(detectBlobsOp.Inputs[0], blobDetectorOp.Outputs[0]);
            detectBlobsOp.Connect(detectBlobsOp.Inputs[1], cannyOp.Outputs[0]);
            getBlobOp.Connect(getBlobOp.Inputs[0], detectBlobsOp.Outputs[0]);

            areaOp.Connect(areaOp.Inputs[0], getBlobOp.Outputs[0]);
            boundingBoxOp.Connect(boundingBoxOp.Inputs[0], getBlobOp.Outputs[0]);
            centroid.Connect(centroid.Inputs[0], getBlobOp.Outputs[0]);
            moments.Connect(moments.Inputs[0], getBlobOp.Outputs[0]);
            normalizedCentralMoments.Connect(normalizedCentralMoments.Inputs[0], getBlobOp.Outputs[0]);
            centralMoments.Connect(centralMoments.Inputs[0], getBlobOp.Outputs[0]);
            huMoments.Connect(huMoments.Inputs[0], getBlobOp.Outputs[0]);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            CvBlobDetector blobDetector = ((BlobDetectorWrapper)detectBlobsOp.Inputs[0].Value)._CvBlobDetector;
            CvBlob blob = (CvBlob)getBlobOp.Outputs[0].Value;
            
            bool b1 = (int)areaOp.Outputs[0].Value == blob.Area;
            bool b2 = (Rectangle)boundingBoxOp.Outputs[0].Value == blob.BoundingBox;
            bool b3 = (PointF)centroid.Outputs[0].Value == blob.Centroid;
            bool b4 = ((double[])moments.Outputs[0].Value).SequenceEqual(blob.GetMoments());
            bool b5 = ((double[])normalizedCentralMoments.Outputs[0].Value).SequenceEqual(blob.GetNormalizedCentralMoments());
            bool b6 = ((double[])centralMoments.Outputs[0].Value).SequenceEqual(blob.GetCentralMoments());
            bool b7 = ((double[])huMoments.Outputs[0].Value).SequenceEqual(blob.GetHuMoments());
            return b1 && b2 && b3 && b4 && b5 && b6 && b7;
        }


        public static bool CSharpOperators(Workspace workspace)
        {
            Operator LogicalNegation = workspace.NewInstance("LogicalNegation");
            Operator Multiply = workspace.NewInstance("Multiply");
            Operator Division = workspace.NewInstance("Division");
            Operator Add = workspace.NewInstance("Add");
            Operator Subtract = workspace.NewInstance("Subtract");
            Operator LessThan = workspace.NewInstance("LessThan");
            Operator GreaterThan = workspace.NewInstance("GreaterThan");
            Operator LessThanOrEqual = workspace.NewInstance("LessThanOrEqual");
            Operator GreaterThanOrEqual = workspace.NewInstance("GreaterThanOrEqual");
            Operator Equal = workspace.NewInstance("Equal");
            Operator NotEqual = workspace.NewInstance("NotEqual");
            Operator LogicalAND = workspace.NewInstance("LogicalAND");
            Operator LogicalXOR = workspace.NewInstance("LogicalXOR");
            Operator LogicalOR = workspace.NewInstance("LogicalOR");

            LogicalNegation.Inputs[0].Value = true;

            Multiply.Inputs[0].Value = 123;
            Multiply.Inputs[1].Value = 23;

            Division.Inputs[0].Value = 10;
            Division.Inputs[1].Value = 4;

            Add.Inputs[0].Value = 32;
            Add.Inputs[1].Value = 54;

            Subtract.Inputs[0].Value = 100;
            Subtract.Inputs[1].Value = -9;

            LessThan.Inputs[0].Value = 5;
            LessThan.Inputs[1].Value = 10;

            GreaterThan.Inputs[0].Value = 5;
            GreaterThan.Inputs[1].Value = 10;

            LessThanOrEqual.Inputs[0].Value = 5;
            LessThanOrEqual.Inputs[1].Value = 10;

            GreaterThanOrEqual.Inputs[0].Value = 5;
            GreaterThanOrEqual.Inputs[1].Value = 10;

            Equal.Inputs[0].Value = 5;
            Equal.Inputs[1].Value = 10;

            NotEqual.Inputs[0].Value = 5;
            NotEqual.Inputs[1].Value = 10;

            LogicalAND.Inputs[0].Value = true;
            LogicalAND.Inputs[1].Value = false;

            LogicalXOR.Inputs[0].Value = true;
            LogicalXOR.Inputs[1].Value = false;

            LogicalOR.Inputs[0].Value = true;
            LogicalOR.Inputs[1].Value = false;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            bool b1 = (bool)LogicalNegation.Outputs[0].Value == false;
            bool b2 = (double)Multiply.Outputs[0].Value == 123*23;
            bool b3 = (double)Division.Outputs[0].Value == 10d/4d;
            bool b4 = (double)Add.Outputs[0].Value == 32+54;
            bool b5 = (double)Subtract.Outputs[0].Value == 100-(-9);
            bool b6 = (bool)LessThan.Outputs[0].Value == true;
            bool b7 = (bool)GreaterThan.Outputs[0].Value == false;
            bool b8 = (bool)LessThanOrEqual.Outputs[0].Value == true;
            bool b9 = (bool)GreaterThanOrEqual.Outputs[0].Value == false;
            bool b10 = (bool)Equal.Outputs[0].Value == false;
            bool b11 = (bool)NotEqual.Outputs[0].Value == true;
            bool b12 = (bool)LogicalAND.Outputs[0].Value == false;
            bool b13 = (bool)LogicalXOR.Outputs[0].Value == true;
            bool b14 = (bool)LogicalOR.Outputs[0].Value == true;

            return b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8 && b9 && b10 && b11 && b12 && b13 && b14;
        }

        public static bool Histogram(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator EMGUHistEqualize = workspace.NewInstance("EMGUHistEqualize");
            Operator EmguCVCalcHist = workspace.NewInstance("EmguCVCalcHist");
            Operator BackProject = workspace.NewInstance("VisualOCVBackProject");
            Operator EmguCVDrawHist = workspace.NewInstance("EmguCVDrawHist");

            Image<Rgb, byte> image = new Image<Rgb, byte>("Foreground.jpg");

            imageOp.Outputs[0].Value = image;

            EmguCVCalcHist.Connect(EmguCVCalcHist.Inputs[0], imageOp.Outputs[0]);
            EMGUHistEqualize.Connect(EMGUHistEqualize.Inputs[0], imageOp.Outputs[0]);
            BackProject.Connect(BackProject.Inputs[0], EmguCVCalcHist.Outputs[0]);
            BackProject.Connect(BackProject.Inputs[1], imageOp.Outputs[0]);
            EmguCVDrawHist.Connect(EmguCVDrawHist.Inputs[0], imageOp.Outputs[0]);

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }


            Image<Rgb, byte> output = image.Clone();
            output._EqualizeHist();

            bool b1 = !imageOp.Error;
            bool b2 = !imageOp.Error;
            bool b3 = !imageOp.Error;
            bool b4 = !imageOp.Error;


            return b1 && b2 && b3 && b4 && output.Equals((Image<Rgb, byte>)EMGUHistEqualize.Outputs[0].Value);
        }

        public static bool Merge(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator EMGUMergeRGB = workspace.NewInstance("EMGUMergeRGB");
            Operator SplitImage = workspace.NewInstance("SplitImage");
            Operator IndexArray0 = workspace.NewInstance("IndexArray");
            Operator IndexArray1 = workspace.NewInstance("IndexArray");
            Operator IndexArray2 = workspace.NewInstance("IndexArray");

            SplitImage.Connect(SplitImage.Inputs[0], imageOp.Outputs[0]);
            IndexArray0.Connect(IndexArray0.Inputs[0], SplitImage.Outputs[0]);
            IndexArray1.Connect(IndexArray1.Inputs[0], SplitImage.Outputs[0]);
            IndexArray2.Connect(IndexArray2.Inputs[0], SplitImage.Outputs[0]);
            EMGUMergeRGB.Connect(EMGUMergeRGB.Inputs[0], IndexArray0.Outputs[0]);
            EMGUMergeRGB.Connect(EMGUMergeRGB.Inputs[1], IndexArray1.Outputs[0]);
            EMGUMergeRGB.Connect(EMGUMergeRGB.Inputs[2], IndexArray2.Outputs[0]);

            Image<Rgb, byte> image = new Image<Rgb, byte>("Foreground.jpg");

            imageOp.Inputs[0].Value = image;
            IndexArray1.Inputs[1].Value = 1;
            IndexArray2.Inputs[1].Value = 2;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Gray, byte>[] images = image.Split();
            Image<Rgb, byte> outImage = new Image<Rgb, byte>(images);

            bool b1 = image.Equals((Image<Rgb, byte>)EMGUMergeRGB.Outputs[0].Value);
            bool b2 = image.Equals(outImage);
            bool b3 = outImage.Equals((Image<Rgb, byte>)EMGUMergeRGB.Outputs[0].Value);

            return b1 && b2 && b3;
        }

        public static bool Contours(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator EMGUCVCanny = workspace.NewInstance("EMGUCVCanny");
            Operator VisualOCVFindContours = workspace.NewInstance("VisualOCVFindContours");
            Operator EmguCVCreateIColor = workspace.NewInstance("EmguCVCreateIColor");
            Operator VisualOCVDrawContours = workspace.NewInstance("VisualOCVDrawContours");

            EMGUCVCanny.Connect(EMGUCVCanny.Inputs[0], imageOp.Outputs[0]);
            VisualOCVFindContours.Connect(VisualOCVFindContours.Inputs[0], EMGUCVCanny.Outputs[0]);
            VisualOCVDrawContours.Connect(VisualOCVDrawContours.Inputs[0], imageOp.Outputs[0]);
            VisualOCVDrawContours.Connect(VisualOCVDrawContours.Inputs[1], VisualOCVFindContours.Outputs[0]);
            VisualOCVDrawContours.Connect(VisualOCVDrawContours.Inputs[3], EmguCVCreateIColor.Outputs[0]);

            Image<Rgb, byte> image = new Image<Rgb, byte>("Foreground.jpg");
            Image<Rgb, byte> output = image.Clone();

            imageOp.Inputs[0].Value = image;
            EMGUCVCanny.Inputs[1].Value = 80;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Gray, byte> canny = image.Canny(80, 0);
            Emgu.CV.Util.VectorOfVectorOfPoint contours = new Emgu.CV.Util.VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();

            Emgu.CV.CvInvoke.FindContours(canny, contours, hierarchy, Emgu.CV.CvEnum.RetrType.Ccomp, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxNone);
            Emgu.CV.CvInvoke.DrawContours(output, contours, -1, new MCvScalar(0, 0, 0));

            return output.Equals(((Image<Rgb, byte>)VisualOCVDrawContours.Outputs[0].Value));
        }

        public static bool HuLinesAndHuCircles(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator HoughLines = workspace.NewInstance("HoughLines");
            Operator EmguCVCreateIColor = workspace.NewInstance("EmguCVCreateIColor");
            Operator EMGUCVDrawLineSegments = workspace.NewInstance("EMGUCVDrawLineSegments");

            HoughLines.Connect(HoughLines.Inputs[0], imageOp.Outputs[0]);
            EMGUCVDrawLineSegments.Connect(EMGUCVDrawLineSegments.Inputs[0], imageOp.Outputs[0]);
            EMGUCVDrawLineSegments.Connect(EMGUCVDrawLineSegments.Inputs[1], HoughLines.Outputs[0]);
            EMGUCVDrawLineSegments.Connect(EMGUCVDrawLineSegments.Inputs[2], EmguCVCreateIColor.Outputs[0]);

            Image<Rgb, byte> image = new Image<Rgb, byte>("Foreground.jpg");
            Image<Rgb, byte> output = image.Clone();

            imageOp.Inputs[0].Value = image;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            LineSegment2D[][] lines = image.HoughLines(1, 1, 1, 1, 1, 1, 1);

            return lines[0].SequenceEqual(((LineSegment2D[][])HoughLines.Outputs[0].Value)[0]);//todo: include hu circles
        }

        public static bool ConvertScale(Workspace workspace)
        {
            Operator imageOp = workspace.NewInstance("EmguCVImage");
            Operator EMGUCVConvertScale = workspace.NewInstance("EMGUCVConvertScale");

            EMGUCVConvertScale.Connect(EMGUCVConvertScale.Inputs[0], imageOp.Outputs[0]);

            Image<Rgb, byte> image = new Image<Rgb, byte>("Foreground.jpg");

            imageOp.Inputs[0].Value = image;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Image<Rgb, byte> output = image.ConvertScale<byte>(1, 0);

            return output.Equals((Image<Rgb, byte>)EMGUCVConvertScale.Outputs[0].Value);
        }

        public static bool Anaglyph(Workspace workspace)
        {
            Operator leftImageOp = workspace.NewInstance("EmguCVImage");
            Operator rightImageOp = workspace.NewInstance("EmguCVImage");
            Operator EMGUCVAnaglyph = workspace.NewInstance("EMGUCVAnaglyph");

            EMGUCVAnaglyph.Connect(EMGUCVAnaglyph.Inputs[0], leftImageOp.Outputs[0]);
            EMGUCVAnaglyph.Connect(EMGUCVAnaglyph.Inputs[1], rightImageOp.Outputs[0]);

            Image<Rgb, byte> leftImage = new Image<Rgb, byte>("Foreground.jpg");
            Image<Rgb, byte> rightImage = new Image<Rgb, byte>("Background.jpg");

            leftImageOp.Inputs[0].Value = leftImage;
            rightImageOp.Inputs[0].Value = rightImage;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Emgu.CV.Util.VectorOfMat mats = new Emgu.CV.Util.VectorOfMat(new Mat[] { leftImage[0].Mat, rightImage[1].Mat, rightImage[2].Mat });
            Mat result = new Mat();
            CvInvoke.Merge(mats, result);

            return result.ToImage<Rgb, byte>().Equals((Image<Rgb, byte>)EMGUCVAnaglyph.Outputs[0].Value);
        }

        public static bool CreateMatrix(Workspace workspace)
        {
            Operator EmguCreateMatrix = workspace.NewInstance("EmguCreateMatrix");

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Mat mat = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(1,1), new Point(0, 0));

            return mat.Equals((Mat)EmguCreateMatrix.Outputs[0].Value);
        }

        public static bool Camera(Workspace workspace)
        {
            Operator EMGUCamera = workspace.NewInstance("EMGUCamera");

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            return (Image<Rgb, byte>)EMGUCamera.Outputs[0].Value != null;
        }

        public static bool VideoFile(Workspace workspace)
        {
            Operator VisualOCVVideoFile = workspace.NewInstance("VisualOCVVideoFile");

            VisualOCVVideoFile.Inputs[0].Value = "TestVideo.mp4";

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            return (Image<Rgb, byte>)VisualOCVVideoFile.Outputs[0].Value != null;
        }

        public static bool ThrowException(Workspace workspace)
        {
            Operator VisualOCVMacroThrowException = workspace.NewInstance("VisualOCVMacroThrowException");

            VisualOCVMacroThrowException.Inputs[0].Value = "An Error Message";
            VisualOCVMacroThrowException.Inputs[1].Value = true;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            return VisualOCVMacroThrowException.Error && VisualOCVMacroThrowException.OperatorError.ErrorMessage()  == "An Error Message";
        }


        public static bool CreateEmptyMatrix(Workspace workspace)
        {
            Operator EMGUCVCreateEmptyMatrix = workspace.NewInstance("EMGUCVCreateEmptyMatrix");

            EMGUCVCreateEmptyMatrix.Inputs[1].Value = 5;
            EMGUCVCreateEmptyMatrix.Inputs[2].Value = 4;

            workspace.AlgorithmManager.BeginAlgorithm();
            while (!workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Matrix<byte> output = (Matrix<byte>)EMGUCVCreateEmptyMatrix.Outputs[0].Value;
            return output.Size.Equals(new Size(4, 5));
        }
    }
}
