﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace VisualOCVBackend
{
    public class AssemblyManager
    {
        public static string DllLocation = AppDomain.CurrentDomain.BaseDirectory + "Dlls\\";
        internal static Dictionary<string, LoadedAssembly> Assemblies = new Dictionary<string, LoadedAssembly>();

        public static void Init()
        {
            Assembly[] activeAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach(Assembly assembly in activeAssemblies)
            {
                if (assembly.IsDynamic) { Console.WriteLine("Dynamic Assembly"); continue; } ///todo: figure out what to do about dynamic assemblies
                ManageAssembly(assembly);
            }
        }

        public static Assembly GetAssemby(string fullNameOrAbsoluteFilePath)
        {
            if (Assemblies.ContainsKey(fullNameOrAbsoluteFilePath))
            {
                return Assemblies[fullNameOrAbsoluteFilePath].Assembly;
            }
            return null;
        }

        public static void LoadAssembly(string filePath, bool relative)
        {
            string path = filePath;

            if(relative)
            {
                path = DllLocation + filePath;
            }
            Assembly asm = Assembly.LoadFrom(path);
            ManageAssembly(asm);
        }

        public static void ManageAssembly(Assembly assembly)
        {
            if (IsAssemblyLoaded(assembly)) {  return; }

            LoadedAssembly loadedAssembly = new LoadedAssembly(assembly);
            Assemblies.Add(loadedAssembly.FilePath, loadedAssembly);
            Assemblies.Add(loadedAssembly.FullName, loadedAssembly);
        }

        public static bool IsAssemblyLoaded(Assembly assembly)
        {
            return (Assemblies.ContainsKey(assembly.Location) || Assemblies.ContainsKey(assembly.FullName));
        }

        internal class LoadedAssembly
        {
            public string FilePath;
            public string FullName;
            public Assembly Assembly;

            public LoadedAssembly(Assembly assembly)
            {
                FilePath = assembly.Location;
                FullName = assembly.FullName;
                Assembly = assembly;
            }
        }
    }
}
