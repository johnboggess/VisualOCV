﻿/* Created by John Bogggess 
 * Created to fullfill masters degree requirements at Southern Adventist Univeristy
 * 8-4-2020
 * johnboggess@jboggess.net
 */
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using VisualOCVBackend.Operators;
using VisualOCVBackend.Types;
using VisualOCVBackend.Exceptions;
namespace VisualOCVBackend
{
    public class AlgorithmManager
    {
        public static int SaveVersion = 1;

        public bool CallCallbacks;
        public Workspace Workspace;
        public bool IsAlgorithmStopping { get { return _algorithmStopCounter != 0; } }
        public bool IsAlgorithmFinished { get { return _runningOperators.Count == 0; } }

        internal List<Operator> _RootOperators = new List<Operator>();
        internal List<Operator> _LeafOperators = new List<Operator>();

        private Dictionary<Guid, Operator> _operators = new Dictionary<Guid, Operator>();
        private ConcurrentDictionary<Guid, Operator> _runningOperators = new ConcurrentDictionary<Guid, Operator>();
        private ConcurrentQueue<Operator> _queuedPostIterationOperators = new ConcurrentQueue<Operator>();
        private int _algorithmStopCounter = 0;
        private Mutex _rootOperatorsMutex = new Mutex();
        private Mutex _leafOperatorsMutex = new Mutex();

        public AlgorithmManager(bool callCallbacks, Workspace workspace)
        {
            CallCallbacks = callCallbacks;
            Workspace = workspace;
        }
        public int OperatorCount()
        {
            return _operators.Count;
        }

        public Operator NewOperatorInstance(OperatorType opType, Guid guid)
        {
            StopAlgorithm();

            Operator result = opType.NewInstance(this, guid);

            _rootOperatorsMutex.WaitOne();
            _RootOperators.Add(result);
            _rootOperatorsMutex.ReleaseMutex();

            _leafOperatorsMutex.WaitOne();
            _LeafOperators.Add(result);
            _leafOperatorsMutex.ReleaseMutex();

            _operators.Add(result.InstanceID, result);

            if (CallCallbacks)
            {
                Workspace.GUIListeners.OperatorAdded(result);
            }

            ResumeAlgorithm();

            return result;
        }

        public Operator NewMacroInstance(MacroType macroType, Guid guid)
        {
            StopAlgorithm();

            Macro result = new Macro(this, macroType, guid);

            _rootOperatorsMutex.WaitOne();
            _RootOperators.Add(result);
            _rootOperatorsMutex.ReleaseMutex();

            _leafOperatorsMutex.WaitOne();
            _LeafOperators.Add(result);
            _leafOperatorsMutex.ReleaseMutex();

            _operators.Add(result.InstanceID, result);

            if (CallCallbacks)
            {
                Workspace.GUIListeners.OperatorAdded(result);
            }

            ResumeAlgorithm();

            return result;

        }
        public void DeleteOperator(Guid guid)
        {
            if (DoesOperatorExist(guid))
            {
                Operator op = _operators[guid];
                StopAlgorithm();

                if (Workspace.SelectedOperators.Contains(_operators[guid]))
                {
                    Workspace.SelectedOperators.Remove(_operators[guid]);
                }

                if (_operators.ContainsKey(guid))
                {
                    _operators.Remove(guid);
                }
                
                op.DisconnectFromAll();

                if (CallCallbacks)
                {
                    Workspace.GUIListeners.OperatorRemoved(op);
                }

                _rootOperatorsMutex.WaitOne();
                _RootOperators.Remove(op);
                _rootOperatorsMutex.ReleaseMutex();

                _leafOperatorsMutex.WaitOne();
                _LeafOperators.Remove(op);
                _leafOperatorsMutex.ReleaseMutex();

                op.Kill();

                ResumeAlgorithm();
            }
        }

        public bool DoesOperatorExist(Guid guid)
        {
            return _operators.ContainsKey(guid);
        }

        public Operator GetOperator(Guid guid)
        {
            if (DoesOperatorExist(guid))
            {
                return _operators[guid];
            }
            return null;
        }

        public List<Operator> GetListOfOperators()
        {
            return _operators.Values.ToList();
        }

        /// <summary>
        /// Begins an iteration of the algorithm. 
        /// </summary>
        public void BeginAlgorithm()
        {
            if (IsAlgorithmStopping) { return; }

            if (!IsAlgorithmFinished)
            {
                string errorMessage = "Attempting to start iteration before previous iteration has completed";
                Exception e = new Exception(errorMessage);
                Workspace.GUIListeners.ExceptionOccured(e);
                return;
            }

            _rootOperatorsMutex.WaitOne();
            if (_RootOperators.Count == 0)
            {
                //Workspace.GUIListeners.AlgorithmFinished();
                _rootOperatorsMutex.ReleaseMutex();
                return;
            }

            foreach (Operator op in _RootOperators)
            {
                op.WakeProcessingThread();
            }

            while (!IsAlgorithmFinished) { }

            Operator postIterationOp;
            while(_queuedPostIterationOperators.TryDequeue(out postIterationOp))
            {
                postIterationOp.WakeProcessingThread(true);
            }

            _rootOperatorsMutex.ReleaseMutex();
        }

        /// <summary>
        /// Forces the algorithm to stop. The algorithm cannot be resumed until <see cref="ResumeAlgorithm"/> is called.
        /// </summary>
        public void StopAlgorithm()
        {
            _algorithmStopCounter += 1;
            while (!IsAlgorithmFinished) { }
        }

        /// <summary>
        /// Allows the algorithm to resume. See <see cref="StopAlgorithm"/>.
        /// </summary>
        public void ResumeAlgorithm()
        {
            _algorithmStopCounter -= 1;
            if (_algorithmStopCounter < 0)
            {
                string errorMessage = "Resuming a non-paused algorithm.";
                Exception e = new Exception(errorMessage);
                Workspace.GUIListeners.ExceptionOccured(e);
                _algorithmStopCounter = 0;
            }
        }

        internal void _QueueOperatorForPostIterationExecution(Operator op)
        {
            _queuedPostIterationOperators.Enqueue(op);
        }

        public bool IsRootOperator(Operator op)
        {
            return _RootOperators.Contains(op);
        }

        public bool IsLeafOperator(Operator op)
        {
            return _LeafOperators.Contains(op);
        }
        
        //todo: Use XDocument instead of XmlWriter
        public string Save()
        {
            return AlgorithmXMLReaderWriter.Save(this);
        }

        public void Load(string saveData)
        {
            List<Exception> exceptions = AlgorithmXMLReaderWriter.Load(this, saveData);
            Workspace.GUIListeners.ExceptionOccured(exceptions);
        }


        internal void _ClearAlgorithm()
        {
            _rootOperatorsMutex.WaitOne();
            _leafOperatorsMutex.WaitOne();

            while (_operators.Count > 0)
            {
                _operators[_operators.Keys.ToList()[0]].Delete();
            }
            _operators.Clear();

            _RootOperators.Clear();
            _LeafOperators.Clear();

            _leafOperatorsMutex.ReleaseMutex();
            _rootOperatorsMutex.ReleaseMutex();
        }

        internal void _OperatorStarting(Operator op)
        {
            _runningOperators.TryAdd(op.InstanceID, op);
        }

        internal void _OperatorFinished(Operator op)
        {
            Operator outOp;
            _runningOperators.TryRemove(op.InstanceID, out outOp);
        }

        internal void _MakeRootOrLeaf(Operator op)
        {
            _rootOperatorsMutex.WaitOne();
            _leafOperatorsMutex.WaitOne();

            bool currentLeaf = op.IsLeafOperator;
            bool currentRoot = op.IsRootOperator;

            bool leaf = true;
            foreach (OperatorOutput output in op.Outputs)
            {
                if (output.Connections.Count != 0)
                {
                    leaf = false;
                    break;
                }
            }

            bool root = true;
            foreach (OperatorInput input in op.Inputs)
            {
                if (input.Connection != null)
                {
                    root = false;
                    break;
                }
            }

            if (currentLeaf != leaf)
            {
                if (currentLeaf == true) { _LeafOperators.Remove(op); }
                else if (currentLeaf == false) { _LeafOperators.Add(op); }
            }

            if (currentRoot != root)
            {
                if (currentRoot == true) { _RootOperators.Remove(op); }
                else if (currentRoot == false) { _RootOperators.Add(op); }
            }

            _leafOperatorsMutex.ReleaseMutex();
            _rootOperatorsMutex.ReleaseMutex();
        }
    }
}
