﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace VisualOCVBackend.Validators
{
    public class Validator
    {
        MethodInfo Method;
        Type InputType;
        public string Name;

        public List<object> Arguments;

        public Validator(string name, MethodInfo method)
        {
            Name = name;
            Method = method;
            InputType = method.GetParameters()[0].ParameterType;
        }

        public Validator(Validator validator, List<object> args)
        {
            Name = validator.Name;
            Method = validator.Method;
            InputType = Method.GetParameters()[0].ParameterType;
            this.Arguments = args;
        }

        public Tuple<bool,string> Validate(object value)
        {
            try
            {
                object[] allArgs = new object[Arguments.Count + 1];

                allArgs[0] = Convert.ChangeType(value, InputType);

                for (int i = 0; i < Arguments.Count; i++)
                {
                    allArgs[i + 1] = Arguments[i];
                }

                return (Tuple<bool, string>)Method.Invoke(null, allArgs);
            }
            catch
            {
                return Tuple.Create(false, "");
            }
        }

        public ParameterInfo[] GetParameterInfo()
        {
            return Method.GetParameters();
        }
    }
}
