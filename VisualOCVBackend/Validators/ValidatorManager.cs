﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace VisualOCVBackend.Validators
{
    public class ValidatorManager
    {
        public static string XmlLocation = AppDomain.CurrentDomain.BaseDirectory + "Validators\\";
        static Dictionary<string, Validator> Validators = new Dictionary<string, Validator>();
        static Workspace Workspace;

        public static void LoadValidators(Workspace workspace)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(XmlLocation);
            FileInfo[] files = directoryInfo.GetFiles();
            foreach(FileInfo file in files)
            {
                if (file.Extension == ".xml")
                {
                    List<Validator> validators = ValidatorXmlReader.Read(Workspace, file.FullName);

                    foreach(Validator validator in validators)
                    {
                        ManageValidator(validator);
                    }
                }
            }
        }

        public static void ManageValidator(Validator validator)
        {
            if(Validators.ContainsKey(validator.Name))
            {
                string errorMessage = "Unable to load validator " + validator.Name + ": Validator with name " + validator.Name + " already exists.";
                Exception e = new Exception(errorMessage);
                Workspace.GUIListeners.ExceptionOccured(e);
                return;
            }
            Validators.Add(validator.Name, validator);
        }

        public static ParameterInfo[] GetValidatorParameterInfo(string name)
        {
            if (Validators.ContainsKey(name))
            {
                return Validators[name].GetParameterInfo();
            }
            else
            {
                return null;
            }
        }

        public static Validator CreateValidator(string name, List<object> args)
        {
            if(Validators.ContainsKey(name))
            {
                return new Validator(Validators[name], args);
            }
            else
            {
                return null;
            }
        }
    }
}
