﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;

using VisualOCVBackend.Types;
namespace VisualOCVBackend.Validators
{
    class ValidatorXmlReader
    {
        static Workspace Workspace;
        public static List<Validator> Read(Workspace workspace, string filePath)
        {
            Workspace = workspace;
            FileStream fileStream = File.OpenRead(filePath);
            XmlReader xmlReader = XmlReader.Create(fileStream);

            int version = -1;

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (xmlReader.Name == "Version")
                    {
                        version = int.Parse(xmlReader.GetAttribute("version"));
                        break;
                    }
                }
            }

            if (version == 1) { return ReadV1(xmlReader); }

            string errorMessage = "Unable to load validators from " + filePath + ": Unknown file version " + version;
            Exception exception = new Exception(errorMessage);
            Workspace.GUIListeners.ExceptionOccured(exception);
            return new List<Validator>();
        }

        private static string getFullAssemblyNameFromAssemblyQualifiedName(string assemblyQualifiedName)
        {
            int comma = assemblyQualifiedName.IndexOf(',');
            string result = assemblyQualifiedName.Remove(0, comma + 2); //plus 2 to remove comma and space
            return result;
        }

        private static string getFullTypeNameFromAssemblyQualifiedName(string assemblyQualifiedName)
        {
            int comma = assemblyQualifiedName.IndexOf(',');
            string result = assemblyQualifiedName.Remove(comma, assemblyQualifiedName.Length - comma);
            return result;
        }

        private static string getTypeNameFromAssemblyQualifiedName(string assemblyQualifiedName)
        {
            string result = getFullTypeNameFromAssemblyQualifiedName(assemblyQualifiedName);
            int comma = assemblyQualifiedName.IndexOf('.');
            result = result.Remove(0, comma + 1);
            return result;
        }

        private static List<Validator> ReadV1(XmlReader xmlReader)//todo: Use XDocument
        {
            List<Validator> result = new List<Validator>();

            LoadDllsV1(xmlReader);

            while (xmlReader.Read())
            {
                if (xmlReader.Name == "Validator" && xmlReader.NodeType == XmlNodeType.Element)
                {
                    result.Add(GetValidatorV1(xmlReader));
                }
            }
            return result;
        }



        private static Validator GetValidatorV1(XmlReader xmlReader)
        {
            Type type = null;
            string validatorName = null;
            string methodName = "";
            List<Type> paramaters = new List<Type>();
            while (xmlReader.Read())
            {
                if(xmlReader.Name == "Name")
                {
                    validatorName = xmlReader.GetAttribute(0);
                }

                if (xmlReader.Name == "Type")
                {
                    ///Todo: throw correct exception

                    string assemblyQualifiedName = xmlReader.GetAttribute(0);
                    string fullAssemblyName = getFullAssemblyNameFromAssemblyQualifiedName(assemblyQualifiedName);
                    string fullTypeName = getFullTypeNameFromAssemblyQualifiedName(assemblyQualifiedName);//getTypeNameFromAssemblyQualifiedName(assemblyQualifiedName);
                    type = AssemblyManager.GetAssemby(fullAssemblyName).GetType(fullTypeName, true);
                    //type = AssemblyManager.GetAssemby(fan)//Type.GetType(xmlReader.GetAttribute(0), true);
                }

                if (xmlReader.Name == "Method")
                {
                    while (xmlReader.MoveToNextAttribute())
                    {
                        if(xmlReader.Name == "Name")
                        {
                            methodName = xmlReader.Value;
                        }
                        else if (xmlReader.Name.ToLower().Contains("id"))
                        {
                            string id = xmlReader.Value;
                            paramaters.Add(TypeManager.GetVisualOCVTypeFromName(id).ActualType);
                        }
                        else
                        {
                            paramaters.Add(Utilities.GetType(xmlReader.Value));
                        }
                    }
                    break;
                }
            }
            MethodInfo method = RuntimeReflectionExtensions.GetRuntimeMethod(type, methodName, paramaters.ToArray());
            return new Validator(validatorName, method);
        }

        private static void LoadDllsV1(XmlReader xmlReader)
        {
            while (xmlReader.Read())
            {
                if (xmlReader.Name == "Dll" && xmlReader.NodeType == XmlNodeType.Element)
                {
                    AssemblyManager.LoadAssembly(xmlReader.GetAttribute(0), true);
                }

                if (xmlReader.Name == "Dlls" && xmlReader.NodeType == XmlNodeType.EndElement)
                {
                    return;
                }
            }
        }
    }
}
