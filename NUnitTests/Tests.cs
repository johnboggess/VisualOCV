/* Created by John Bogggess 
 * Created to fullfill masters degree requirements at Southern Adventist Univeristy
 * 8-4-2020
 * johnboggess@jboggess.net
 */
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System;
using System.Threading;
using System.Drawing;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using VisualOCVBackend;
using VisualOCVBackend.Types;
using VisualOCVBackend.Operators;
using VisualOCVBackend.Validators;
using VisualOCVBackend.GUIListeners;
using VisualOCVBackend.HelperFunctions;
using System.ComponentModel.Design;
using VisualOCVBackend.Controls;

namespace Tests
{
    //dotnet test --collect:"XPlat Code Coverage"
    public class Tests : ExceptionListener
    {
        public static Workspace Workspace = new Workspace();
        public static Random Random;
        public static List<Exception> RecentVisualOCVExceptions = new List<Exception>();

        private static Guid macroByteInputGuid = Guid.NewGuid();
        private static Guid macroFloatInputGuid = Guid.NewGuid();
        private static Guid macroOutputGuid = Guid.NewGuid();
        private static Guid add1Guid = Guid.NewGuid();
        private static Guid add2Guid = Guid.NewGuid();
        private static Guid createByteGuid = Guid.NewGuid();
        private static Guid createFloatGuid = Guid.NewGuid();

        private void setUpTestMacros()
        {
            string data = @"<File>
  <Version Version=""1"" />
  <Operator Type=""Add"" ID=""" + add1Guid + @""" X=""475"" Y=""147.119995"">
    <Inputs>
      <Input Index=""0"" ConnectedToID=""" + createByteGuid + @""" ConnectedToIndex=""0"" />
      <Input Index=""1"" ConnectedToID=""" + createFloatGuid + @""" ConnectedToIndex=""0"" />
    </Inputs>
  </Operator>
  <Operator Type=""CreateByte"" ID=""" + createByteGuid + @""" X=""246"" Y=""93.5400085"">
    <Inputs>
      <Input Index=""0"" ConnectedToID=""" + macroByteInputGuid + @""" ConnectedToIndex=""0"" />
    </Inputs>
  </Operator>
  <Operator Type=""CreateFloat"" ID=""" + createFloatGuid + @""" X=""241"" Y=""313.540009"">
    <Inputs>
      <Input Index=""0"" ConnectedToID=""" + macroFloatInputGuid + @""" ConnectedToIndex=""0"" />
    </Inputs>
  </Operator>
  <Operator Type=""Add"" ID=""" + add2Guid + @""" X=""676"" Y=""320.119995"">
    <Inputs>
      <Input Index=""0"" ConnectedToID=""" + add1Guid + @""" ConnectedToIndex=""0"" />
      <Input Index=""1"" Value=""10"" />
    </Inputs>
  </Operator>
  <Operator Type=""VisualOCVMacroInput"" ID=""" + macroByteInputGuid + @""" X=""51"" Y=""61.539978"">
    <Inputs>
      <Input Index=""0"" Value=""Byte Input"" />
    </Inputs>
  </Operator>
  <Operator Type=""VisualOCVMacroInput"" ID=""" + macroFloatInputGuid + @""" X=""52"" Y=""337.539978"">
    <Inputs>
      <Input Index=""0"" Value=""Float Input"" />
    </Inputs>
  </Operator>
  <Operator Type=""VisualOCVMacroOutput"" ID=""" + macroOutputGuid + @""" X=""851"" Y=""420.040039"">
    <Inputs>
      <Input Index=""0"" Value=""Output"" />
      <Input Index=""1"" ConnectedToID=""" + add2Guid + @""" ConnectedToIndex=""0"" />
    </Inputs>
  </Operator>
</File>";
            File.WriteAllText("Macros/Test.xml", data);
        }

        private void setUpDummyModules()
        {
            Directory.CreateDirectory("Operators/Dummy");
            for (int i = 0; i < 50; i++)
            {
                string data = @"<File>
  <Version version = ""1""/>
  
  <Dlls>
    <Dll path = ""Emgu.CV.World.NetStandard1_4.dll""/>
  </Dlls>

  <Operator>
    <Type AssemblyQualifiedName = ""EMGUCVImageHelpers""/>
    <Method Name = ""ImageFromFile"" param1TypeID = ""C#String""/>
    <ID ID = ""dummy" + i + @"""/>
    <OperatorName Name = ""Image From File"" />
    <Width Width = ""300""/>
    <Resizable/>
    <OperatorInputs>
      <Input>
        <Name Name = ""File Path""/>
        <MethodParamIndex Index = ""0""/>
        <Control Control = ""MultilineTextbox"" default = """"/>
      </Input>
    </OperatorInputs>
    <OperatorOutputs>
      <Output>
        <Name Name = ""Image""/>
        <Control Control = ""Image""/>
      </Output>
    </OperatorOutputs>
  </Operator>
</File>";
                if(!File.Exists("Operators/Dummy/Dummy" + i + ".xml"))
                    File.WriteAllText("Operators/Dummy/Dummy" + i + ".xml", data);
            }
        }

        [OneTimeSetUp]
        public void Setup()
        {
            Random = new Random(DateTime.Now.Millisecond);
            setUpTestMacros();

            Workspace.GUIListeners.AddExceptionOccuredListener(this);

            Stopwatch stopwatch = new Stopwatch();

            AssemblyManager.Init();
            TypeManager.LoadAllTypes();
            ValidatorManager.LoadValidators(Workspace);
            VideoManager.Init();

            stopwatch.Start();
            VisualOCVBackend.Operators.OperatorModule.LoadModules(Workspace);
            VisualOCVBackend.Operators.MacroModule.LoadModules(Workspace);
            stopwatch.Stop();
            long time = stopwatch.ElapsedMilliseconds;

            setUpDummyModules();

        }
        
        [SetUp]
        public void SetupEachTest()
        {
            Workspace.Clear();
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 0);
            Assert.True(Workspace.AlgorithmManager._RootOperators.Count == 0);
            Assert.True(Workspace.AlgorithmManager._LeafOperators.Count == 0);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            Workspace.Clear();
        }

        [Test]
        public void Macro()
        {
            MacroModule macroModule = MacroModule.RootModule;
            Assert.True(macroModule.MacroTypes.ContainsKey("Macros/Test"));

            MacroType macroType = macroModule.MacroTypes["Macros/Test"];

            Assert.True(macroType.ID == "Macros/Test");
            Assert.True(macroType.Name == "Test");
            Assert.True(macroType.Module == macroModule);

            Macro macro = (Macro)macroType.CreateInstance(Workspace.AlgorithmManager);
            Assert.True(macro._AlgorithmManager.GetListOfOperators().Count == 7);

            Assert.True(macro._AlgorithmManager.GetOperator(add1Guid).InstanceID == add1Guid);
            Assert.True(macro._AlgorithmManager.GetOperator(createByteGuid).InstanceID == createByteGuid);
            Assert.True(macro._AlgorithmManager.GetOperator(createFloatGuid).InstanceID == createFloatGuid);

            Assert.True(macro._AlgorithmManager.GetOperator(macroByteInputGuid).InstanceID == macroByteInputGuid);
            Assert.True(macro._AlgorithmManager.GetOperator(macroFloatInputGuid).InstanceID == macroFloatInputGuid);
            Assert.True(macro._AlgorithmManager.GetOperator(macroOutputGuid).InstanceID == macroOutputGuid);

            Assert.True(macro._AlgorithmManager.GetOperator(add1Guid).Inputs[0].Connection.Operator == macro._AlgorithmManager.GetOperator(createByteGuid));
            Assert.True(macro._AlgorithmManager.GetOperator(add1Guid).Inputs[1].Connection.Operator == macro._AlgorithmManager.GetOperator(createFloatGuid));

            Assert.True(macro._AlgorithmManager.GetOperator(add2Guid).Inputs[0].Connection.Operator == macro._AlgorithmManager.GetOperator(add1Guid));
            Assert.True(macro._AlgorithmManager.GetOperator(add2Guid).Inputs[1].Connection == null);
            Assert.True(((double)macro._AlgorithmManager.GetOperator(add2Guid).Inputs[1].Value) == 10d);

            Assert.True(macro.Inputs[0].Name == "Byte Input");
            Assert.True(macro.Inputs[1].Name == "Float Input");
            Assert.True(macro.Outputs[0].Name == "Output");

            Assert.True(macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(macroByteInputGuid)));
            Assert.True(macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(macroFloatInputGuid)));
            Assert.True(!macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(createByteGuid)));
            Assert.True(!macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(createFloatGuid)));
            Assert.True(!macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(add1Guid)));
            Assert.True(!macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(add2Guid)));
            Assert.True(!macro._AlgorithmManager._RootOperators.Contains(macro._AlgorithmManager.GetOperator(macroOutputGuid)));

            Assert.True(macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(macroOutputGuid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(createByteGuid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(createFloatGuid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(add1Guid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(add2Guid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(macroByteInputGuid)));
            Assert.True(!macro._AlgorithmManager._LeafOperators.Contains(macro._AlgorithmManager.GetOperator(macroFloatInputGuid)));

            Assert.True(macro.Inputs.Count == 2);

            Assert.True(macro.Outputs.Count == 1);

            macro.Inputs[0].Value = (byte)1;
            macro.Inputs[1].Value = (float)7;

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Assert.True((double)macro.Outputs[0].Value == 18d);

            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 1);
            Assert.True(Workspace.AlgorithmManager._RootOperators.Count == 1);
            Assert.True(Workspace.AlgorithmManager._LeafOperators.Count == 1);

            //Create algorithm of operators thats equivalent to the macro
            Operator createByteOp = Workspace.NewInstance("CreateByte");
            Operator createFloatOp = Workspace.NewInstance("CreateFloat");
            Operator add1 = Workspace.NewInstance("Add");
            Operator add2 = Workspace.NewInstance("Add");

            createByteOp.Connect(add1.Inputs[0], createByteOp.Outputs[0]);
            createFloatOp.Connect(add1.Inputs[1], createFloatOp.Outputs[0]);

            add1.Connect(add2.Inputs[0], add1.Outputs[0]);
            add2.Inputs[1].Value = 10d;
            
            byte value1 = (byte)Random.Next(0, byte.MaxValue);
            float value2 = (float)Random.Next(-10000, 10000);

            macro.Inputs[0].ReceiveData(value1);
            macro.Inputs[1].ReceiveData(value2);
            createByteOp.Inputs[0].ReceiveData(value1);
            createFloatOp.Inputs[0].ReceiveData(value2);

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Assert.True((double)macro.Outputs[0].Value == (double)add2.Outputs[0].Value);
            Assert.True( (double)macro.Outputs[0].Value == (double)(value1 + value2 + 10d) );

        }

        [Test]
        public void SaveLoadWorkspace()
        {
            Operator opImage = Workspace.NewInstance("EmguCVImageConstructor");
            Operator canny = Workspace.NewInstance("EMGUCVCanny");
            Operator intOp1 = Workspace.NewInstance("CreateInt");
            Operator intOp2 = Workspace.NewInstance("CreateInt");
            Operator dilate = Workspace.NewInstance("EMGUCVDilate");

            opImage.X = Random.Next(-100, 100);
            opImage.Y = Random.Next(-100, 100);
            canny.X = Random.Next(-100, 100);
            canny.Y = Random.Next(-100, 100);
            intOp1.X = Random.Next(-100, 100);
            intOp1.Y = Random.Next(-100, 100);
            intOp2.X = Random.Next(-100, 100);
            intOp2.Y = Random.Next(-100, 100);
            dilate.X = Random.Next(-100, 100);
            dilate.Y = Random.Next(-100, 100);

            opImage.Connect(canny.Inputs[0], opImage.Outputs[0]);
            intOp1.Connect(canny.Inputs[1], intOp1.Outputs[0]);
            intOp2.Connect(canny.Inputs[2], intOp2.Outputs[0]);

            canny.Connect(dilate.Inputs[0], canny.Outputs[0]);

            opImage.Inputs[0].ReceiveData("TestImage.PNG");
            dilate.Inputs[1].ReceiveData(3);

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            string save = Workspace.Save();

            Workspace.Clear();

            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 0);
            Assert.True(Workspace.SelectedOperators.Count == 0);

            Workspace.Load(save);

            Operator new_opImage = Workspace.AlgorithmManager.GetOperator(opImage.InstanceID);
            Operator new_canny = Workspace.AlgorithmManager.GetOperator(canny.InstanceID);
            Operator new_intOp1 = Workspace.AlgorithmManager.GetOperator(intOp1.InstanceID);
            Operator new_intOp2 = Workspace.AlgorithmManager.GetOperator(intOp2.InstanceID);
            Operator new_dilate = Workspace.AlgorithmManager.GetOperator(dilate.InstanceID);

            Assert.True(opImage.X == new_opImage.X);
            Assert.True(opImage.Y == new_opImage.Y);
            Assert.True(canny.X == new_canny.X);
            Assert.True(canny.Y == new_canny.Y);
            Assert.True(intOp1.X == new_intOp1.X);
            Assert.True(intOp1.Y == new_intOp1.Y);
            Assert.True(intOp2.X == new_intOp2.X);
            Assert.True(intOp2.Y == new_intOp2.Y);
            Assert.True(dilate.X == new_dilate.X);
            Assert.True(dilate.Y == new_dilate.Y);

            Assert.True(opImage.Inputs[0].Value.Equals(new_opImage.Inputs[0].Value));
            Assert.True(canny.Inputs[0].Value == null);
            Assert.True(intOp1.Inputs[0].Value.Equals(new_intOp1.Inputs[0].Value));
            Assert.True(intOp2.Inputs[0].Value.Equals(new_intOp2.Inputs[0].Value));
            Assert.True(dilate.Inputs[0].Value == null);

            Assert.True(new_opImage.Outputs[0].Connections[0] == new_canny.Inputs[0]);
            Assert.True(new_intOp1.Outputs[0].Connections[0] == new_canny.Inputs[1]);
            Assert.True(new_intOp2.Outputs[0].Connections[0] == new_canny.Inputs[2]);
            Assert.True(new_canny.Outputs[0].Connections[0] == new_dilate.Inputs[0]);
        }

        [Test]
        public void SaveLoadOperator()
        {
            Operator canny = Workspace.NewInstance("EMGUCVCanny");
            canny.X = (float)Math.PI;
            canny.Y = 234.136f;
            canny.Inputs[1].ReceiveData(123);
            canny.Inputs[2].ReceiveData(Math.E);
            XElement saveData = canny.Save();
            string result = saveData.ToString();

            Workspace.Clear();

            Operator newCanny = Operator.Load(result, Workspace.AlgorithmManager).Operator;
            Assert.True(newCanny.InstanceID == canny.InstanceID);
            Assert.True(newCanny.X == canny.X);
            Assert.True(newCanny.Y == canny.Y);
            Assert.True(newCanny.Inputs[1].Value.Equals(canny.Inputs[1].Value));
            Assert.True(newCanny.Inputs[2].Value.Equals(canny.Inputs[2].Value));
            

            Operator convert = Workspace.NewInstance("EMGUConvertImage");
            convert.X = Random.Next(-100, 100);
            convert.Y = Random.Next(-100, 100);
            convert.Inputs[1].ReceiveData(VisualOCVOperatorFunctions.TColorEnum.Hls);
            convert.Inputs[2].ReceiveData(VisualOCVOperatorFunctions.TDepthEnum.Int);
            saveData = convert.Save();
            result = saveData.ToString();

            Workspace.Clear();

            Operator newConvert = Operator.Load(result, Workspace.AlgorithmManager).Operator;
            Assert.True(newConvert.InstanceID == convert.InstanceID);
            Assert.True(newConvert.X == convert.X);
            Assert.True(newConvert.Y == convert.Y);
            Assert.True(newConvert.Inputs[1].Value.Equals(convert.Inputs[1].Value));
            Assert.True(newConvert.Inputs[2].Value.Equals(convert.Inputs[2].Value));


            Operator createBool = Workspace.NewInstance("CreateBool");
            createBool.X = Random.Next(-100, 100);
            createBool.Y = Random.Next(-100, 100);
            createBool.Inputs[0].ReceiveData(Random.Next(0,2) == 0);
            saveData = createBool.Save();
            result = saveData.ToString();

            Workspace.Clear();

            Operator newCreateBool = Operator.Load(result, Workspace.AlgorithmManager).Operator;
            Assert.True(newCreateBool.InstanceID == createBool.InstanceID);
            Assert.True(newCreateBool.X == createBool.X);
            Assert.True(newCreateBool.Y == createBool.Y);
            Assert.True(newCreateBool.Inputs[0].Value.Equals(createBool.Inputs[0].Value));


            Operator imageFromFile = Workspace.NewInstance("EmguCVImageConstructor");
            imageFromFile.X = Random.Next(-100, 100);
            imageFromFile.Y = Random.Next(-100, 100);
            imageFromFile.Inputs[0].ReceiveData("Foreground.jpg");
            saveData = imageFromFile.Save();
            result = saveData.ToString();

            Workspace.Clear();

            Operator newImageFromFile = Operator.Load(result, Workspace.AlgorithmManager).Operator;
            Assert.True(newImageFromFile.InstanceID == imageFromFile.InstanceID);
            Assert.True(newImageFromFile.X == imageFromFile.X);
            Assert.True(newImageFromFile.Y == imageFromFile.Y);
            Assert.True(newImageFromFile.Inputs[0].Value.Equals(imageFromFile.Inputs[0].Value));

        }

        [Test]
        public void LoadTypes()
        {
            VisualOCVType visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Int");
            Assert.True(visualOCVType.ActualType == typeof(int));

            visualOCVType = TypeManager.GetVisualOCVTypeFromName("CSharpInt");
            Assert.True(visualOCVType.ActualType == typeof(int));

            int i = 32421;
            string savedI = visualOCVType.Save(i);
            Assert.True(savedI == "32421");

            int loadedI = (int)visualOCVType.Load(savedI);
            Assert.True(i == loadedI);
            
            visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Bool");
            Assert.True(visualOCVType.ActualType == typeof(bool));
            bool _bool = true;
            string saveData = visualOCVType.Save(_bool);
            Assert.True((bool)visualOCVType.Load(saveData) == _bool);

            visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Byte");
            Assert.True(visualOCVType.ActualType == typeof(byte));
            byte _byte = 123;
            saveData = visualOCVType.Save(_byte);
            Assert.True((byte)visualOCVType.Load(saveData) == _byte);

            visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Char");
            Assert.True(visualOCVType.ActualType == typeof(char));
            char _char = 'h';
            saveData = visualOCVType.Save(_char);
            Assert.True((char)visualOCVType.Load(saveData) == _char);

            visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Float");
            Assert.True(visualOCVType.ActualType == typeof(float));
            float _float = float.MaxValue;
            saveData = visualOCVType.Save(_float);
            Assert.True((float)visualOCVType.Load(saveData) == _float);

            visualOCVType = TypeManager.GetVisualOCVTypeFromName("C#Double");
            Assert.True(visualOCVType.ActualType == typeof(double));
            double _double = double.MaxValue;
            saveData = visualOCVType.Save(_double);
            Assert.True((double)visualOCVType.Load(saveData) == _double);
        }

        [Test]
        public void OperatorNewInstance()
        {
            Operator op = Workspace.NewInstance("CreateBool");
            OperatorType baseOp = OperatorModule.AllOperatorTypes["CreateBool"];
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 1);

            AccurateInstance(op, baseOp);
            Assert.True(op.InstanceID != Guid.Empty);

            op = Workspace.NewInstance("Add");
            baseOp = OperatorModule.AllOperatorTypes["Add"];
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 2);

            AccurateInstance(op, baseOp);
            Assert.True(op.InstanceID != Guid.Empty);

            Guid id = Guid.NewGuid();
            op = Workspace.NewInstance("Add", id);
            Assert.True(op.InstanceID == id);

            op = Workspace.NewInstance("CreateBool", id);
            Assert.True(op.InstanceID != id);

        }

        [Test]
        public void AutoCastInput()
        {
            /// An oversight during a minor change to how values are assigned to inputs/ouputs resulted in the incorrect casting of types assigned to the inputs/ouputs.
            /// E.g. Assigning a double to an input that should be receive an byte did not cast the double to an byte, resulting in errors
            Workspace.Clear();
            Operator createByte = Workspace.NewInstance("CreateByte");

            createByte.Inputs[0].SetValue(1.12, false);
            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Assert.True(!createByte.Error);
            Assert.True(!createByte.Inputs[0].Error);

            createByte.Inputs[0].ReceiveData(2.53);
            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Assert.True(!createByte.Error);
            Assert.True(!createByte.Inputs[0].Error);
            Workspace.Clear();
        }

        [Test]
        public void MoveWorkspaceAndOperators()
        {
            Operator byteOp = Workspace.NewInstance("CreateByte");
            Operator intOp = Workspace.NewInstance("CreateInt");

            intOp.Move(10, 34);
            Assert.True(intOp.X == 10);
            Assert.True(intOp.Y == 34);

            byteOp.Move(-56, 6);
            Assert.True(byteOp.X == -56);
            Assert.True(byteOp.Y == 6);

            Workspace.Move(2, -48);

            Assert.True(intOp.X == 8);
            Assert.True(intOp.Y == 82);
            Assert.True(byteOp.X == -58);
            Assert.True(byteOp.Y == 54);

            intOp.SetPosition(1, 3);
            Assert.True(intOp.X == 1 && intOp.Y == 3);
        }

        [Test]
        public void LoadOperators()
        {
            OperatorModule module = OperatorModule.FindModule("Operators.Constructors");

            Operator op = Workspace.NewInstance("CreateBool");
            MethodInfo expectedMethod = RuntimeReflectionExtensions.GetRuntimeMethod(typeof(PrimitiveConstructors), "CreateBool", new Type[] { typeof(bool) });
            Assert.True(!op.IsConstructor);
            Assert.True(op.IsMethod);
            Assert.True(op.IsStaticMethod);
            Assert.True(op.Method.MethodInfo == expectedMethod);
            Assert.True(op.Outputs[0].GetValueType(false) == typeof(bool));
            Assert.True(op.Inputs[0].GetValueType(false) == typeof(bool));
            Assert.True(op.Inputs[0].ControlType == VisualOCVBackend.Controls.ControlType.Checkbox);
            Assert.True(op.Inputs[0].Name == "Value");
            Assert.True(op.Outputs[0].Name == "Bool");
            Assert.True(!((VisualOCVBackend.Controls.Checkbox)op.Inputs[0].Control).Value);

            op = Workspace.NewInstance("CreateByte");
            expectedMethod = RuntimeReflectionExtensions.GetRuntimeMethod(typeof(PrimitiveConstructors), "CreateByte", new Type[] { typeof(byte) });
            Assert.True(!op.IsConstructor);
            Assert.True(op.IsMethod);
            Assert.True(op.IsStaticMethod);
            Assert.True(op.Method.MethodInfo == expectedMethod);
            Assert.True(op.Outputs[0].GetValueType(false) == typeof(byte));
            Assert.True(op.Inputs[0].ControlType == VisualOCVBackend.Controls.ControlType.SliderWithTextbox);
            Assert.True(op.Outputs[0].ControlType == VisualOCVBackend.Controls.ControlType.None);
            Assert.True(((VisualOCVBackend.Controls.SliderWithTextbox<byte>)op.Inputs[0].Control).DefaultValue == 0);

            op = Workspace.NewInstance("CreateChar");
            expectedMethod = RuntimeReflectionExtensions.GetRuntimeMethod(typeof(PrimitiveConstructors), "CreateChar", new Type[] { typeof(char) });
            Assert.True(!op.IsConstructor);
            Assert.True(op.IsMethod);
            Assert.True(op.IsStaticMethod);
            Assert.True(op.Method.MethodInfo == expectedMethod);
            Assert.True(op.Outputs[0].GetValueType(false) == typeof(char));
            Assert.True(op.Inputs[0].ControlType == VisualOCVBackend.Controls.ControlType.Textbox);
            Assert.True(op.Outputs[0].ControlType == VisualOCVBackend.Controls.ControlType.None);
            Assert.True(((VisualOCVBackend.Controls.Textbox)op.Inputs[0].Control).Value == " ");

            op = Workspace.NewInstance("CreateString");
            expectedMethod = RuntimeReflectionExtensions.GetRuntimeMethod(typeof(PrimitiveConstructors), "CreateString", new Type[] { typeof(string) });
            Assert.True(!op.IsConstructor);
            Assert.True(op.IsMethod);
            Assert.True(op.IsStaticMethod);
            Assert.True(op.Method.MethodInfo == expectedMethod);
            Assert.True(op.Outputs[0].GetValueType(false) == typeof(string));
            Assert.True(op.Inputs[0].ControlType == VisualOCVBackend.Controls.ControlType.MultilineTextbox);
            Assert.True(op.Outputs[0].ControlType == VisualOCVBackend.Controls.ControlType.None);
            Assert.True(((VisualOCVBackend.Controls.MultilineTextbox)op.Inputs[0].Control).Value == "");

            VisualOCVBackend.Tests.LoadOperatorsTest.ConvertImage(Workspace);
        }
        
        [Test]
        ///Are modules named correctly, is the module hierarchy formed correctly, do modules load all their operators?
        public void LoadModules()
        {
            Assert.True(OperatorModule.RootModule.FullName == "Operators");
            bool b = OperatorModule.RootModule.SubModules.Exists((m) =>
            {
                return m.FullName == "Operators.Constructors";
            });
            Assert.True(b);

            OperatorModule module = OperatorModule.RootModule.FindSubmodule("Operators.Testing");
            Assert.True(module.Name == "Testing");
            Assert.True(module.FullName == "Operators.Testing");

        }

        [Test]
        public void SelectUnselect()
        {
            OperatorType baseOp = OperatorModule.FindOperatorType("Operators.C#.Operators", "Add");
            Operator op1 = Workspace.NewInstance(baseOp);
            Operator op2 = baseOp.CreateInstance(Workspace.AlgorithmManager);
            Operator op3 = baseOp.CreateInstance(Workspace.AlgorithmManager);

            Workspace.Select(op1.InstanceID);
            Assert.True(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.False(Workspace.SelectedOperators.Contains(op3));

            Workspace.Select(op2.InstanceID);
            Assert.True(Workspace.SelectedOperators.Contains(op1));
            Assert.True(Workspace.SelectedOperators.Contains(op2));
            Assert.False(Workspace.SelectedOperators.Contains(op3));


            Workspace.Unselect(op2.InstanceID);
            Assert.True(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.False(Workspace.SelectedOperators.Contains(op3));


            Workspace.Select(op2.InstanceID);
            Workspace.Select(op3.InstanceID, false, false);
            Assert.False(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.True(Workspace.SelectedOperators.Contains(op3));

            Workspace.Select(op1.InstanceID, true, false);
            Assert.True(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.True(Workspace.SelectedOperators.Contains(op3));

            Workspace.Select(op1.InstanceID, true, false);
            Assert.False(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.True(Workspace.SelectedOperators.Contains(op3));

            Workspace.Select(op2.InstanceID, false, true);
            Assert.False(Workspace.SelectedOperators.Contains(op1));
            Assert.True(Workspace.SelectedOperators.Contains(op2));
            Assert.True(Workspace.SelectedOperators.Contains(op3));

            Workspace.Select(op2.InstanceID, false, true);
            Assert.False(Workspace.SelectedOperators.Contains(op1));
            Assert.True(Workspace.SelectedOperators.Contains(op2));
            Assert.True(Workspace.SelectedOperators.Contains(op3));

            op1.SetPosition(10, 32);
            op2.SetPosition(0, 0);
            op3.SetPosition(-12, 23);

            Workspace.ClearSelection();
            Assert.False(Workspace.SelectedOperators.Contains(op1));
            Assert.False(Workspace.SelectedOperators.Contains(op2));
            Assert.False(Workspace.SelectedOperators.Contains(op3));

        }

        [Test]
        public void UniqueID()
        {
            Operator op = Workspace.NewInstance("CreateByte");
            Assert.True(op.Name == "New Byte");
            Assert.True(op.InstanceID != Guid.Empty);
        }

        [Test]
        public void CyclePrevention()
        {
            Operator op1 = Workspace.NewInstance("Add");//ops[0].NewInstance();
            Operator op1_2 = Workspace.NewInstance("Subtract");//ops[0].NewInstance();
            Operator op2 = Workspace.NewInstance("Multiply");//ops[2].NewInstance();

            Assert.False(Operator.WillConnectionCauseCycle(op1, op2));
            Assert.False(Operator.WillConnectionCauseCycle(op2, op1));
            Assert.True(Operator.WillConnectionCauseCycle(op1, op1));
            Assert.True(Operator.WillConnectionCauseCycle(op2, op2));

            op1.Connect(op2.Inputs[0], op1.Outputs[0]);

            Assert.True(op1.Outputs[0].Connections[0] == op2.Inputs[0]);

            Assert.False(Operator.WillConnectionCauseCycle(op2, op1));
            Assert.True(Operator.WillConnectionCauseCycle(op1, op2));
            
            op2.Connect(op1.Inputs[0], op2.Outputs[0]);
            Assert.True(op2.Outputs[0].Connections.Count == 0 && op1.Inputs[0].Connection == null);

            op1.DisconnectFromAll();
            op2.DisconnectFromAll();

            op1.Connect(op1_2.Inputs[0], op1.Outputs[0]);
            op1_2.Connect(op2.Inputs[0], op1_2.Outputs[0]);

            Assert.True(op1.Outputs[0].Connections[0] == op1_2.Inputs[0]);
            Assert.True(op1_2.Outputs[0].Connections[0] == op2.Inputs[0]);
            Assert.True(Operator.WillConnectionCauseCycle(op1, op1_2));

        }
        
        [Test]
        public void CopyPaste()
        {
            Operator op1 = Workspace.NewInstance("Add");
            Operator op2 = Workspace.NewInstance("Multiply");

            op1.X = Random.Next(0, 100);
            op1.Y = Random.Next(0, 100);

            op2.X = Random.Next(0, 100);
            op2.Y = Random.Next(0, 100);

            op1.Inputs[0].ReceiveData(1);
            op1.Inputs[1].ReceiveData(.14159);
            op1.Connect(op2.Inputs[0], op1.Outputs[0]);

            Workspace.Select(op1.InstanceID);
            Workspace.Copy(1, 1);
            List<Operator> pastedOperators = Workspace.Paste(0,0);

            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 3);
            Assert.True(pastedOperators[0].Inputs[0].Value.Equals(1d));
            Assert.True(pastedOperators[0].Inputs[1].Value.Equals(.14159d));
            Assert.True(pastedOperators[0].Outputs[0].Connections.Count == 0);
            Assert.True(Workspace.AlgorithmManager.IsRootOperator(pastedOperators[0]));
            Assert.True(Workspace.AlgorithmManager.IsLeafOperator(pastedOperators[0]));
            Assert.True(pastedOperators[0].X == op1.X-1);
            Assert.True(pastedOperators[0].Y == op1.Y-1);

            Workspace.Select(op2.InstanceID);
            Workspace.Copy(43,64);
            pastedOperators = Workspace.Paste(10,24);

            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 5);
            Assert.True(pastedOperators[0].Inputs[0].Value.Equals(1d));
            Assert.True(pastedOperators[0].Inputs[1].Value.Equals(.14159));
            Assert.True(pastedOperators[0].Outputs[0].Connections[0].Operator == pastedOperators[1]);
            Assert.True(Workspace.AlgorithmManager.IsRootOperator(pastedOperators[0]));
            Assert.True(Workspace.AlgorithmManager.IsLeafOperator(pastedOperators[1]));
            Assert.True(pastedOperators[0].X == op1.X + (10f - 43));
            Assert.True(pastedOperators[0].Y == op1.Y + (24f - 64));
            
            Workspace.Cut(63,19);

            Assert.True(!Workspace.AlgorithmManager.DoesOperatorExist(op1.InstanceID));
            Assert.True(!Workspace.AlgorithmManager.DoesOperatorExist(op2.InstanceID));

            pastedOperators = Workspace.Paste(0,0);

            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 5);
            Assert.True(pastedOperators[0].Inputs[0].Value.Equals(1d));
            Assert.True(pastedOperators[0].Inputs[1].Value.Equals(.14159d));
            Assert.True(pastedOperators[0].Outputs[0].Connections[0].Operator == pastedOperators[1]);

            Workspace.Clear();
            op1 = Workspace.NewInstance("Add");
            Workspace.Select(op1.InstanceID);
            Workspace.Copy(0,0);
            pastedOperators = Workspace.Paste(0, 0);
            pastedOperators = Workspace.Paste(0, 0);
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 3);


            MacroModule macroModule = MacroModule.RootModule;
            MacroType macroType = macroModule.MacroTypes["Macros/TestGUI"];
            Macro macro = (Macro)macroType.CreateInstance(Workspace.AlgorithmManager);

            Workspace.Select(macro.InstanceID);
            Workspace.Cut(0, 0);
            Workspace.Paste(0, 0);

        }

        [Test]
        public void OperatorCommunication()
        {
            OperatorModule module = OperatorModule.FindModule("Operators.Testing");
            
            Operator op1 = Workspace.NewInstance("Add");
            Operator op2 = Workspace.NewInstance("GreaterThan");

            op1.Connect(op2.Inputs[0], op1.Outputs[0]);
            op1.Connect(op2.Inputs[1], op1.Outputs[0]);
            Assert.True(op1.Outputs[0].Connections[0] == op2.Inputs[0]);
            Assert.True(op1.Outputs[0].Connections[1] == op2.Inputs[1]);
            Assert.True(op2.Inputs[0].Connection == op1.Outputs[0]);
            
            op1.Connect(op2.Inputs[0], op1.Outputs[0]);
            Assert.True(op1.Outputs[0].Connections.Count == 2);
            
            op1.Disconnect(op2.Inputs[0], op1.Outputs[0]);
            Assert.True(op1.Outputs[0].Connections.Count == 1 && op1.Outputs[0].Connections[0].Index == 1 && op2.Inputs[0].Connection == null);


            op1.Connect(op2.Inputs[0], op1.Outputs[0]);
            op1.DisconnectFromAll();
            Assert.True(op1.Outputs[0].Connections.Count == 0);
            Assert.True(op2.Inputs[0].Connection == null);
            Assert.True(op2.Inputs[1].Connection == null);
            

            op1.Connect(op2.Inputs[0], op1.Outputs[0]);

            op1.Inputs[0].ReceiveData(1);
            op1.Inputs[1].ReceiveData(.123);

            op2.Inputs[1].ReceiveData(1.0);
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Assert.True((double)op1.Inputs[0].Value == 1d);
            Assert.True((double)op1.Inputs[1].Value == .123d);
            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Assert.True(op1.Outputs[0].Value == op2.Inputs[0].Value);

        }

        [Test]
        public void ValidateInputs()
        {
            Operator imageOp = Workspace.NewInstance("EmguCVImageConstructor");
            Operator sobelOp = Workspace.NewInstance("EMGUCVSobel");

            imageOp.Inputs[0].Value = "TestImage.jpg";

            Assert.True(!imageOp.Error);
            Assert.True(!sobelOp.Error);

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Assert.True(!imageOp.Error);
            Assert.True(sobelOp.Error);
            Assert.True(sobelOp.OperatorError.GetType() == typeof(RequiredInputNotDefined));

            sobelOp.Connect(sobelOp.Inputs[0], imageOp.Outputs[0]);
            sobelOp.Inputs[1].Value = 1;
            sobelOp.Inputs[2].Value = 1;
            sobelOp.Inputs[3].SetValue(4, false);

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Assert.True(!imageOp.Error);
            Assert.True(sobelOp.Error);
            Assert.True(sobelOp.OperatorError.GetType() == typeof(OperatorInputNotValid));
            Assert.True(sobelOp.OperatorError.ErrorMessage() == "Aperture Size - Value must be odd");

        }

        [Test]
        public void AddRemoveOperators()
        {
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 0);

            Operator addInts = Workspace.NewInstance("Add");
            Operator addDoubles = Workspace.NewInstance("Add");
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 2);

            addInts.Connect(addDoubles.Inputs[0], addInts.Outputs[0]);
            Assert.True(addDoubles.Inputs[0].Connection.Operator == addInts);

            Workspace.Select(addInts.InstanceID);
            Assert.True(Workspace.SelectedOperators.Count == 1);

            addInts.Delete();
            Assert.True(Workspace.SelectedOperators.Count == 0);


            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 1);
            Assert.True(!Workspace.AlgorithmManager.DoesOperatorExist(addInts.InstanceID));
            Assert.True(Workspace.SelectedOperators.Count == 0);
            Assert.True(addDoubles.Inputs[0].Connection == null);
            Assert.True(addInts.Inputs[0].Connection == null);

            foreach(OperatorOutput operatorOutput in addDoubles.Outputs)
            {
                Assert.True(operatorOutput.Connections.Count == 0);
            }

            foreach (OperatorOutput operatorOutput in addInts.Outputs)
            {
                Assert.True(operatorOutput.Connections.Count == 0);
            }

            addInts = Workspace.NewInstance("Add");
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 2);

            Workspace.Select(addInts.InstanceID, true, true);
            Workspace.Select(addDoubles.InstanceID, true, true);
            Assert.True(Workspace.SelectedOperators.Count == 2);

            Assert.True(Workspace.SelectedOperators.Count == 2);
            Workspace.DeleteSelectedOperators();
            Assert.True(Workspace.AlgorithmManager.OperatorCount() == 0);

        }

        [Test]
        public void OpListSearch()
        {
            string searchString = "image";

            List<OperatorType> allOpTypes = VisualOCVBackend.Operators.OperatorModule.RootModule.OperatorTypesRecursive().Values.ToList();//OperatorTypes.Values.ToList();
            List<MacroType> allMacroTypes = VisualOCVBackend.Operators.MacroModule.RootModule.MacroTypesRecursive().Values.ToList();//MacroTypes.Values.ToList();
            
            Tuple<OperatorModule, MacroModule> searchResult = OperatorList.Search(searchString);
            List<OperatorType> searchResultOperatorTypes = searchResult.Item1.OperatorTypesRecursive().Values.ToList();
            List<MacroType> searchResultMacroTypes = searchResult.Item2 != null ? searchResult.Item2.MacroTypesRecursive().Values.ToList() : new List<MacroType>();

            foreach (OperatorType opType in searchResultOperatorTypes)
            {
                Assert.True(opType.Name.ToLower().Contains(searchString));
            }
            foreach (MacroType macroType in searchResultMacroTypes)
            {
                Assert.True(macroType.Name.ToLower().Contains(searchString));
            }

            foreach (OperatorType opType in allOpTypes)
            {
                if(!searchResultOperatorTypes.Contains(opType))
                {
                    Assert.True(!opType.Name.ToLower().Contains(searchString));
                }
            }

            foreach (MacroType macroType in allMacroTypes)
            {
                if (!searchResultMacroTypes.Contains(macroType))
                {
                    Assert.True(!macroType.Name.ToLower().Contains(searchString));
                }
            }


            for (int i = 0; i < 20; i++)
            {
                int op = Random.Next(0, allOpTypes.Count);
                int startIndex = Random.Next(0, allOpTypes[op].Name.Length);
                int length = Random.Next(1, allOpTypes[op].Name.Length - startIndex);
                searchString = allOpTypes[op].Name.Substring(startIndex, length).ToLower();

                searchResult = OperatorList.Search(searchString);
                searchResultOperatorTypes = searchResult.Item1.OperatorTypesRecursive().Values.ToList();
                searchResultMacroTypes = searchResult.Item2 != null ? searchResult.Item2.MacroTypesRecursive().Values.ToList() : new List<MacroType>();

                Assert.True(searchResult.Item1.FindOperatorType(allOpTypes[op].ID) != null);

                foreach (OperatorType opType in searchResultOperatorTypes)
                {
                    Assert.True(opType.Name.ToLower().Contains(searchString));
                }
                foreach (MacroType macroType in searchResultMacroTypes)
                {
                    Assert.True(macroType.Name.ToLower().Contains(searchString));
                }

                foreach (OperatorType opType in allOpTypes)
                {
                    if (!searchResultOperatorTypes.Contains(opType))
                    {
                        Assert.True(!opType.Name.ToLower().Contains(searchString));
                    }
                }

                foreach (MacroType macroType in allMacroTypes)
                {
                    if (!searchResultMacroTypes.Contains(macroType))
                    {
                        Assert.True(!macroType.Name.ToLower().Contains(searchString));
                    }
                }
            }
        }

        [Test]
        //Does an algorithm of operators produce the same result as an algorithm made directly from the functions they represent
        public void OperatorAlgorithmProducesCorrectResults()
        {
            Assert.True(VisualOCVBackend.Tests.OperatorAlgorithmProducesCorrectResults.Test(Workspace));
        }

        [Test]
        //Do operators produce the same output as the function they represent?
        public void OperatorProducesCorrectResults()
        {
            Assert.True(VisualOCVBackend.Tests.OperatorProduceCorrectResults.LoadImage(Workspace));
            Assert.True(VisualOCVBackend.Tests.OperatorProduceCorrectResults.Canny(Workspace));
            Assert.True(VisualOCVBackend.Tests.OperatorProduceCorrectResults.Sobel(Workspace));
            Assert.True(VisualOCVBackend.Tests.OperatorProduceCorrectResults.CannyDilate(Workspace));

            Operator add = Workspace.NewInstance("Add");
            add.Inputs[0].Value = Random.Next(-100, 100);
            add.Inputs[1].Value = Random.Next(-100, 100);
            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }
            Assert.True( ((int)add.Inputs[0].Value + (int)add.Inputs[1].Value) == (double)add.Outputs[0].Value);
        }

        [Test][Order(0)]
        public void LoadOperatorsSpeedTest()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(OperatorModule.ModuleLocation + "/Dummy");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            OperatorModule opModule = VisualOCVBackend.Operators.OperatorModule._LoadModulesFromDirectory(directoryInfo, null);
            
            stopwatch.Stop();
            long time = stopwatch.ElapsedMilliseconds;
            
            Assert.True(time < 1_000);
        }

        [Test]
        public void GreenScreenSpeedTest()
        {
            //run once before the actual test. Make sure there is no caching issues
            VisualOCVBackend.Tests.GreenScreenTest.SpeedTest(Workspace);

            for(int i = 0; i < 15; i++)
                Assert.True(VisualOCVBackend.Tests.GreenScreenTest.SpeedTest(Workspace));
        }

        /// <summary>
        /// An algorithm should give an exception if its told to start when its already running and when there is more resume calls than stop calls
        /// Also makes sure that starting an algorithm with no operators doesnt cause issues
        /// </summary>
        [Test]
        public void UnusualAlgorithmStartCases()
        {

            Workspace.AlgorithmManager.BeginAlgorithm();
            Workspace.AlgorithmManager.BeginAlgorithm();

            VisualOCVBackend.Tests.GreenScreenTest.CreateGreenScreenAlgorithm(Workspace);

            Workspace.AlgorithmManager.BeginAlgorithm();
            Workspace.AlgorithmManager.BeginAlgorithm();
            Assert.True(RecentVisualOCVExceptions.Count == 1);
            Assert.True(RecentVisualOCVExceptions[0].Message == "Attempting to start iteration before previous iteration has completed");

            while(!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            RecentVisualOCVExceptions.Clear();
            Workspace.StopAlgorithm();
            Workspace.AlgorithmManager.ResumeAlgorithm();
            Assert.True(RecentVisualOCVExceptions.Count == 0);
            Workspace.AlgorithmManager.ResumeAlgorithm();
            Assert.True(RecentVisualOCVExceptions.Count == 1);
            Assert.True(RecentVisualOCVExceptions[0].Message == "Resuming a non-paused algorithm.");

        }

        [Test]
        ///Most of these tests are here because some lines were not covered by other tests
        public void UtilityTests()
        {
            #region GetIEnumerableLength
            int[] array = new int[0];
            Assert.True(Utilities.GetIEnumerableLength(array) == 0);
            array = new int[123];
            Assert.True(Utilities.GetIEnumerableLength(array) == 123);

            List<int> list = new List<int>();
            Assert.True(Utilities.GetIEnumerableLength(list) == 0);
            list = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Assert.True(Utilities.GetIEnumerableLength(list) == 10);

            Dictionary<int,int> dictionary = new Dictionary<int, int>();
            Assert.True(Utilities.GetIEnumerableLength(dictionary) == 0);
            dictionary = new Dictionary<int, int>() { { 1, 2 }, { 75, 234 } };
            Assert.True(Utilities.GetIEnumerableLength(dictionary) == 2);
            #endregion

            #region Utilities.ImageToBitmap
            Operator imageOp = Workspace.NewInstance("EmguCVImageConstructor");
            imageOp.Inputs[0].Value = "TestImage.jpg";
            Workspace.AlgorithmManager.BeginAlgorithm();
            while(!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Bitmap image = Utilities.ImageToBitmap(imageOp.Outputs[0].Value);
            Bitmap image2 = (Bitmap)Image.FromFile("TestImage.jpg");
            for (int w = 0; w < image.Width; w++)
                for (int h = 0; h < image.Height; h++)
                {
                    int error = 3;//Some error will occur
                    Color clr = image.GetPixel(w, h);
                    Color clr2 = image2.GetPixel(w, h);
                    int diffR = Math.Abs(clr.R - clr2.R);
                    int diffG = Math.Abs(clr.G - clr2.G);
                    int diffB = Math.Abs(clr.B - clr2.B);
                    int diffA = Math.Abs(clr.A - clr2.A);
                    if (diffR > error || diffG > error || diffB > error || diffA > error)
                        Assert.Fail("Images are not the same");
                }
            #endregion

            #region GenericTypeFillParameters
            Type listType = list.GetType();
            Type genericDefinitionListType = listType.GetGenericTypeDefinition();
            Assert.True(Utilities.GenericTypeFillParameters(genericDefinitionListType, listType).GetGenericArguments()[0] == typeof(int));

            bool exceptionThrown = false;
            try { Utilities.GenericTypeFillParameters(listType, genericDefinitionListType); }
            catch { exceptionThrown = true; }
            Assert.True(exceptionThrown);
            #endregion

            #region DoesTypeImplementInterface
            Assert.True(Utilities.DoesTypeImplementInterface(list.GetType(), typeof(IEnumerable)));
            Assert.True(!Utilities.DoesTypeImplementInterface(list.GetType(), typeof(IDictionary)));
            exceptionThrown = false;
            try { Utilities.DoesTypeImplementInterface(list.GetType(), list.GetType()); }
            catch { exceptionThrown = true; }
            Assert.True(exceptionThrown);
            #endregion

            #region TDepthEnumToType
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Byte) == typeof(byte));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Decimal) == typeof(decimal));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Double) == typeof(double));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Float) == typeof(float));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Int) == typeof(int));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Long) == typeof(long));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.SByte) == typeof(sbyte));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.Short) == typeof(short));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.UInt) == typeof(uint));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.ULong) == typeof(ulong));
            Assert.True(Utilities.TDepthEnumToType(VisualOCVOperatorFunctions.TDepthEnum.UShort) == typeof(ushort));
            #endregion

            #region TColorEnumToType
            Assert.True(VisualOCVBackend.Tests.UtilitiesTest.TColorEnumToType());
            #endregion

            #region GetMethod
            Assert.True(Utilities.GetMethod(list.GetType().GetGenericTypeDefinition(), "AddRange", new Type[] { typeof(IEnumerable<int>).GetGenericTypeDefinition() }, true) != null);
            Assert.True(Utilities.GetMethod(list.GetType(), "ASDF", new Type[] { }, true) == null);
            #endregion
        }

        [Test]
        public void VideoManagerTests()
        {
            VideoManager.StartWebCam(0);
            Assert.True(VideoManager.IsWebCamRunning(0));
            Assert.True(VisualOCVBackend.Tests.VideoManagerTest.QueryWebcamImage());
            VideoManager.StopWebCam(0);

            bool exceptionOccured = false;
            try { VideoManager.StartWebCam(-1); }
            catch { exceptionOccured = true; }
            Assert.True(exceptionOccured);

            exceptionOccured = false;
            try { VideoManager.StopWebCam(-1); }
            catch { exceptionOccured = true; }
            Assert.True(exceptionOccured);

            exceptionOccured = false;
            try { VideoManager.IsWebCamRunning(-1); }
            catch { exceptionOccured = true; }
            Assert.True(exceptionOccured);

            VideoManager.LoadVideo("TestVideo.mp4");
            Assert.True(VisualOCVBackend.Tests.VideoManagerTest.QueryVideoImage("TestVideo.mp4"));
            VideoManager.UnLoadVideo("TestVideo.mp4");

            exceptionOccured = false;
            try { VideoManager.LoadVideo("ASDF"); }
            catch { exceptionOccured = true; }
            Assert.True(exceptionOccured);
        }

        [Test]
        public void OperatorControlTests()
        {
            Operator image = Workspace.NewInstance("EmguCVImage");
            Operator convert = Workspace.NewInstance("EMGUConvertImage");
            Operator imageOut = Workspace.NewInstance("EmguCVImage");

            Assert.True(((ImageControl)image.Outputs[0].Control).ColorType() == "None");
            Assert.True(((ImageControl)image.Outputs[0].Control).DepthType() == "None");

            image.Connect(convert.Inputs[0], image.Outputs[0]);
            imageOut.Connect(imageOut.Inputs[0], convert.Outputs[0]);

            ((ImageControl)image.Outputs[0].Control).LoadImageFromFile("TestImage.jpg");
            convert.Inputs[1].Value = VisualOCVOperatorFunctions.TColorEnum.Bgr;

            Workspace.AlgorithmManager.BeginAlgorithm();
            while (!Workspace.AlgorithmManager.IsAlgorithmFinished) { }

            Assert.True(((ImageControl)image.Outputs[0].Control).ColorType() == VisualOCVOperatorFunctions.TColorEnum.Rgb.ToString());
            Assert.True(((ImageControl)image.Outputs[0].Control).DepthType() == VisualOCVOperatorFunctions.TDepthEnum.Byte.ToString());

            Assert.True(((ImageControl)imageOut.Outputs[0].Control).ColorType() == VisualOCVOperatorFunctions.TColorEnum.Bgr.ToString());
            Assert.True(((ImageControl)imageOut.Outputs[0].Control).DepthType() == VisualOCVOperatorFunctions.TDepthEnum.Byte.ToString());

            Assert.True(((Dropdown)convert.Inputs[1].Control).DefaultOption == VisualOCVOperatorFunctions.TColorEnum.Rgb.ToString());
            Assert.True(((Dropdown)convert.Inputs[2].Control).DefaultOption == VisualOCVOperatorFunctions.TDepthEnum.Byte.ToString());

            ((ImageControl)imageOut.Outputs[0].Control).ImageToBitmap();
        }

        [Test]
        public void TypeHelperTests()
        {
            Assert.True(VisualOCVBackend.Tests.TypeHelperTests.CloneAndConversionTest());
            Assert.True(VisualOCVBackend.Tests.TypeHelperTests.CloneAndConversionInAlgorithmTest(Workspace));
        }


        [Test]
        public void HelperTests()
        {
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Blobs(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.CSharpOperators(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Histogram(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Merge(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Contours(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.HuLinesAndHuCircles(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.ConvertScale(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Anaglyph(Workspace));
            Workspace.Clear();

            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.CreateMatrix(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.Camera(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.VideoFile(Workspace));
            Workspace.Clear();
            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.ThrowException(Workspace));
            Workspace.Clear();

            Assert.True(VisualOCVBackend.Tests.HelperFunctionTests.CreateEmptyMatrix(Workspace));
            Workspace.Clear();
        }

        private object GetMethod(string methodName, object obj, Type type, object[] args)
        {
            if (string.IsNullOrWhiteSpace(methodName))
                Assert.Fail("methodName cannot be null or whitespace");

            var method = type.GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Static);

            if (method == null)
                Assert.Fail(string.Format("{0} method not found", methodName));

            return method.Invoke(obj, args);
        }

        public void AccurateInstance(Operator operatorInstance, OperatorType operatorType)
        {
            Assert.True(operatorInstance.OperatorType == operatorType);
            Assert.True(operatorInstance.Inputs.Count == operatorType.Inputs.Count);
            Assert.True(operatorInstance.Outputs.Count == operatorType.Outputs.Count);

            for (int i = 0; i < operatorInstance.Inputs.Count; i++)
            {
                Assert.True(operatorInstance.Inputs[i].ControlType == operatorType.Inputs[i].ControlType);
                Assert.True(operatorInstance.Inputs[i].MaxValue == operatorType.Inputs[i].MaxValue);
                Assert.True(operatorInstance.Inputs[i].MinValue == operatorType.Inputs[i].MinValue);
                Assert.True(operatorInstance.Inputs[i].Value.Equals(operatorType.Inputs[i].Value));
                Assert.True(operatorInstance.Inputs[i].Name == operatorType.Inputs[i].Name);
                Assert.True(operatorInstance.Inputs[i].GetValueType(false) == operatorType.Inputs[i].GetValueType(false));

                for (int v = 0; v < operatorInstance.Inputs[i].Validators.Count; v++)
                {
                    Assert.True(operatorInstance.Inputs[i].Validators[v].Name == operatorType.Inputs[i].Validators[v].Name);
                    for (int a = 0; a < operatorInstance.Inputs[i].Validators[v].Arguments.Count; a++)
                    {
                        Assert.True(operatorInstance.Inputs[i].Validators[v].Arguments[a] == operatorType.Inputs[i].Validators[v].Arguments[a]);
                    }
                }
            }

            for (int i = 0; i < operatorInstance.Outputs.Count; i++)
            {
                Assert.True(operatorInstance.Outputs[i].ControlType == operatorType.Outputs[i].ControlType);
                Assert.True(operatorInstance.Outputs[i].Name == operatorType.Outputs[i].Name);
                Assert.True(operatorInstance.Outputs[i].GetValueType(false) == operatorType.Outputs[i].GetValueType(false));
            }
        }

        public bool ExceptionOccured(Exception e)
        {
            RecentVisualOCVExceptions = new List<Exception>() { e };
            return true;
        }

        public bool ExceptionOccured(List<Exception> e)
        {
            RecentVisualOCVExceptions = e;
            return true;
        }
    }
}