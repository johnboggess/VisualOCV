﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
namespace VisualOCVWindows
{
    class Utilities
    {
        //https://stackoverflow.com/a/30729291
        //this function can be a a really bad performance bottleneck. If any changes are made make sure that it does not significantly effect performance.
        public static BitmapSource BitmapToImageSource(Bitmap bitmap)
        {
            if(bitmap == null) { return null; }
            BitmapData bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            BitmapSource bitmapSource = BitmapSource.Create(
                    bitmapData.Width, bitmapData.Height,
                    bitmap.HorizontalResolution, bitmap.VerticalResolution,
                    ConvertPixelFormat(bitmap.PixelFormat), GetBitmapPalette(bitmap.PixelFormat),
                    bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);
            return bitmapSource;
        }

        public static BitmapPalette GetBitmapPalette(System.Drawing.Imaging.PixelFormat pixelFormat)
        {
            switch(pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    return BitmapPalettes.Gray256;
                default:
                    return null;
            }
        }

        public static System.Windows.Media.PixelFormat ConvertPixelFormat(System.Drawing.Imaging.PixelFormat pixelFormat)
        {
            switch(pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    return PixelFormats.Bgra32;
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    return PixelFormats.Bgr24;
                case System.Drawing.Imaging.PixelFormat.Format16bppGrayScale:
                    return PixelFormats.Gray16;
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    return PixelFormats.Indexed8;
                default:
                    throw new Exception("Unsupported image format: " + pixelFormat);
            }
        }

        public static bool IsCtrlOrShiftHeld()
        {
            return IsShiftHeld() || IsCtrlHeld();
        }

        public static bool IsCtrlHeld()
        {
            return Keyboard.Modifiers == ModifierKeys.Control;
        }

        public static bool IsShiftHeld()
        {
            return Keyboard.Modifiers == ModifierKeys.Shift;
        }
    }
}
