﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;

using VisualOCVBackend.Exceptions;
namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for Popup.xaml
    /// </summary>
    public partial class Popup : Window
    {
        List<Exception> Exceptions = new List<Exception>();
        int currentError = 0;
        bool fatalExceptionThrown = false;
        int remainingErrors { get { return Exceptions.Count - currentError - 1; } }

        public Popup(List<Exception> e)
        {
            Exceptions = e;
            currentError = 0;
            InitializeComponent();
            
            _diplayMessage();
        }

        public Popup(string message)
        {
            InitializeComponent();
            Message.Text = message;
            BtnClose.Focus();
            BtnNext.Visibility = Visibility.Collapsed;
        }

        private void _diplayMessage()
        {
            WindowChrome.Title = "Error";
            if(remainingErrors > 0)
            {
                WindowChrome.Title = WindowChrome.Title + " (" + remainingErrors + ")";
                BtnNext.Content = "Next (" + remainingErrors + ")";
                BtnNext.Visibility = Visibility.Visible;
                BtnNext.Focus();
            }
            else
            {
                BtnNext.Visibility = Visibility.Collapsed;
                BtnClose.Focus();
            }

            Exception e = Exceptions[currentError];
            fatalExceptionThrown = VisualOCVException.IsFatal(e);
            if (fatalExceptionThrown)
                SeverityIcon.Source = Utilities.BitmapToImageSource(Bitmap.FromHicon(SystemIcons.Error.Handle));
            else
                SeverityIcon.Source = Utilities.BitmapToImageSource(Bitmap.FromHicon(SystemIcons.Warning.Handle));

            if (e is VisualOCVException)
            {
                Message.Text = ((VisualOCVException)e).Message;
            }
            else
            {
                Message.Text = e.Message;
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (fatalExceptionThrown)
                FatalExceptionManager.FatalExceptionThrown(Exceptions[currentError]);
            Close();
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if (fatalExceptionThrown)
                FatalExceptionManager.FatalExceptionThrown(Exceptions[currentError]);

            if (remainingErrors >= 0)
            {
                currentError += 1;
                _diplayMessage();
                return;
            }
            Close();
        }
    }
}
