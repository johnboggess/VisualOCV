﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

using VisualOCVBackend.Controls;
using System.Runtime.InteropServices.ComTypes;

namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for ImagePopup.xaml
    /// </summary>
    public partial class ImagePopup : Window
    {
        private float _zoomLevel = 1;
        private Matrix _translateMatrix;
        private Matrix _scaleMatrix;
        private System.Windows.Point _translateImageOrigin;
        private bool _isTranslatingImage;
        private Operator _operator;
        private Operator.OutputRow _operatorOutput;
        private Thread _thread;
        private Bitmap _bitmap;
        private bool _isOpen = true;

        public ImagePopup(Operator op)
        {
            InitializeComponent();

            _operator = op;
            _operatorOutput = op.Outputs[0];

            _thread = new Thread(updateImage);
            _thread.Start();

            ImageBorder.MouseMove += ImagePopup_MouseMove;
            ImageBorder.MouseUp += ImagePopup_MouseUp;
            ImageBorder.MouseDown += ImagePopup_MouseDown;
            ImageBorder.MouseWheel += ImagePopup_MouseWheel;

            Closing+= (sender, args) => _isOpen = false;
        }

        private void updateImageTransform()
        {
            Image.RenderTransform = new MatrixTransform(_translateMatrix * _scaleMatrix);
        }

        private void updateImage()
        {
            while(_isOpen)
            {
                App.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ImageControl imageControl = (ImageControl)_operatorOutput.OperatorOutput.Control;
                    _bitmap = imageControl.ImageToBitmap();
                    Image.Source = Utilities.BitmapToImageSource(_bitmap);
                }));
                Thread.Sleep(100);
            }
        }

        private void ImagePopup_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _translateImageOrigin = Mouse.GetPosition(ImageBorder);
            _isTranslatingImage = true;
        }

        private void ImagePopup_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _isTranslatingImage = false;
        }

        private void ImagePopup_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isTranslatingImage)
            {
                System.Windows.Point tranlatePoint = Mouse.GetPosition(ImageBorder);
                float panDeltaX = (float)tranlatePoint.X - (float)_translateImageOrigin.X;
                float panDeltaY = (float)tranlatePoint.Y - (float)_translateImageOrigin.Y;
                _translateImageOrigin = tranlatePoint;
                
                _translateMatrix.OffsetX += panDeltaX * (1f / _zoomLevel);
                _translateMatrix.OffsetY += panDeltaY * (1f / _zoomLevel);
                updateImageTransform();
            }
        }

        private void ImagePopup_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            _zoomLevel += Math.Sign(e.Delta) * .1f;
            _zoomLevel = Math.Min(Math.Max(.1f, _zoomLevel), 5);
            _scaleMatrix = new ScaleTransform(_zoomLevel, _zoomLevel, Image.ActualWidth / 2, Image.ActualHeight / 2).Value;
            updateImageTransform();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image | *.png";
            if(saveFileDialog.ShowDialog() == true)
            {
                _bitmap.Save(saveFileDialog.FileName);
            }
        }
    }
}
