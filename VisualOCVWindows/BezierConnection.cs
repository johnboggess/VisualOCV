﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;

namespace VisualOCVWindows
{
    class BezierConnection
    {
        public static readonly int VisibleThickness = 2;
        public static List<BezierConnection> SelectedConnections = new List<BezierConnection>();

        public Path VisiblePath;
        public bool IsSelected { get { return SelectedConnections.Contains(this); } }

        private PathGeometry _pathGeometry;
        private PathFigure _pathFigure;
        private PolyBezierSegment _bezierSegment;

        private Point _startPoint = new Point(0,0);
        private Point _endPoint = new Point(0, 0);
        private MainWindow _mainWindow;

        private VisualOCVBackend.Operators.OperatorInput _operatorInput;

        public BezierConnection(MainWindow mainWindow, VisualOCVBackend.Operators.OperatorInput operatorInput)
        {
            _mainWindow = mainWindow;
            _operatorInput = operatorInput;

            VisiblePath = new Path();
            VisiblePath.StrokeThickness = VisibleThickness;

            VisiblePath.SetResourceReference(Path.StrokeProperty, "ColorConnection");

            _pathGeometry = new PathGeometry();
            
            _pathFigure = new PathFigure();
            _pathFigure.StartPoint = _startPoint;
            _pathGeometry.Figures.Add(_pathFigure);

            _bezierSegment = new PolyBezierSegment();
            _bezierSegment.Points.Add(_startPoint);
            _bezierSegment.Points.Add(_startPoint);
            _bezierSegment.Points.Add(_startPoint);

            _pathFigure.Segments.Add(_bezierSegment);

            VisiblePath.Data = _pathGeometry;

            mainWindow.OperatorContainer.Children.Add(VisiblePath);

            VisiblePath.MouseDown += Path_MouseDown;
        }

        /// <summary>
        /// Set the starting and endpoints of the connection.
        /// </summary>
        /// <param name="start">The starting point</param>
        /// <param name="end">The ending point</param>
        public void UpdatePosition(Point start, Point end)
        {
            _startPoint = start;
            _endPoint = end;

            _pathFigure.StartPoint = _startPoint;
            _bezierSegment.Points[0] = new Point((_startPoint.X + _endPoint.X) / 2f, _startPoint.Y);
            _bezierSegment.Points[1] = new Point((_startPoint.X + _endPoint.X) / 2f, _endPoint.Y);
            _bezierSegment.Points[2] = _endPoint;
            Panel.SetZIndex(VisiblePath, int.MaxValue);
        }

        /// <summary>
        /// Select the connection
        /// </summary>
        public void Select()
        {
            if(!IsSelected)
            {
                SelectedConnections.Add(this);
                UpdateSelectionGraphic();
            }
        }

        /// <summary>
        /// Unselect the connection
        /// </summary>
        public void Unselect()
        {
            if (IsSelected)
            {
                SelectedConnections.Remove(this);
                UpdateSelectionGraphic();
            }
        }

        /// <summary>
        /// Makes the connectoin blue if selected, or black if unselected.
        /// </summary>
        public void UpdateSelectionGraphic()
        {
            if(IsSelected)
            {
                VisiblePath.Stroke = App.ColorSelectedOperator;
            }
            else
            {
                VisiblePath.SetResourceReference(Path.StrokeProperty, "ColorConnection");
            }
        }

        /// <summary>
        /// Removes the connection from the visual tree. Doenst remove the actual connection between the operators.
        /// </summary>
        public void RemoveVisual()
        {
            Unselect();
            _mainWindow.OperatorContainer.Children.Remove(VisiblePath);
        }

        /// <summary>
        /// Unselect all of the selected connections
        /// </summary>
        public static void UnselectAll()
        {
            while(SelectedConnections.Count > 0)
            {
                SelectedConnections[0].Unselect();
            }
        }

        /// <summary>
        /// Remove all of the selected connections.
        /// </summary>
        public static void RemoveSelected()
        {
            while (SelectedConnections.Count > 0)
            {
                VisualOCVBackend.Operators.OperatorInput input = SelectedConnections[0]._operatorInput;
                input.Operator.Disconnect(input, input.Connection);
            }
        }

        private void Path_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (Utilities.IsCtrlHeld())
            {
                if (IsSelected) { Unselect(); }
                else { Select(); }
                return;
            }
            else if (Utilities.IsShiftHeld())
            {
                Select();
            }
            else
            {
                UnselectAll();
                Select();

                App.Workspace.ClearSelection();

                while (_mainWindow.SelectedOperators.Count != 0)
                {
                    Operator _operator = _mainWindow.SelectedOperators[0];
                    _mainWindow.SelectedOperators.RemoveAt(0);
                    _operator.UpdateSelectionGraphic();
                }
            }
        }

    }
}
