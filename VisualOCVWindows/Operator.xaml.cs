﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using Microsoft.Win32;

using VisualOCVBackend.Operators;
using VisualOCVBackend.Controls;
using VisualOCVBackend.GUIListeners;
namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for Operator.xaml
    /// </summary>
    public partial class Operator : UserControl, ControlValueChangedListener, OperatorMovedListener, OperatorErrorStateChangedListener
    {
        public static int MaximumZIndex = 0;
        public VisualOCVBackend.Operators.Operator BackendOperator;
        public MainWindow MainWindow;

        public List<InputRow> Inputs = new List<InputRow>();
        public Dictionary<OperatorInput, InputRow> OperatorInputToInputRow = new Dictionary<OperatorInput, InputRow>();
        public List<OutputRow> Outputs = new List<OutputRow>();
        public Dictionary<OperatorOutput, OutputRow> OperatorOutputToOutputRow = new Dictionary<OperatorOutput, OutputRow>();

        public float CenterX { get { return (float)ActualWidth / 2f; } }
        public float CenterY { get { return (float)ActualHeight / 2f; } }

        internal ResizeGrip _ResizeGrip;

        private Dictionary<Path, OutputRow> OutputPinToOutputRow = new Dictionary<Path, OutputRow>();
        private Dictionary<Path, InputRow> InputPinToInputRow = new Dictionary<Path, InputRow>();
        private Dictionary<VisualOCVBackend.Controls.Control, InputOutputRow> ControlToInputOutputRow = new Dictionary<VisualOCVBackend.Controls.Control, InputOutputRow>();

        public float X
        {
            get { return BackendOperator.X; }
            set
            {
                BackendOperator.Move(value - X, 0);
            }
        }

        public float Y
        {
            get { return BackendOperator.Y; }
            set
            {
                BackendOperator.Move(0, value - Y);
            }
        }

        public Point GUI_Position
        {
            get { return TransformToAncestor(MainWindow.Workspace).Transform(new Point(0, 0)); }
        } 

        public Operator(VisualOCVBackend.Operators.Operator op, MainWindow mainWindow)
        {
            InitializeComponent();

            App.Workspace.GUIListeners.AddControlValueChangedListeners(this);
            App.Workspace.GUIListeners.AddOperatorMovedListener(this);
            App.Workspace.GUIListeners.AddOperatorErrorStateChangedListeners(this);

            MouseDown += Operator_MouseDown;
            TitleBorder.MouseDown += TitleBorder_MouseDown;
            this.SizeChanged += Operator_SizeChanged;

            MainWindow = mainWindow;
            BackendOperator = op;
            OperatorName.Text = BackendOperator.Name;
            DrawInputsAndOutput();

            foreach (OperatorInput input in op.Inputs)
            {
                ControlValueChanged(input.Control, input.Value);
            }

            UpdateContextMenu();

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftDoubleClick);
            mouseBinding.Command = new RelayCommand((o) =>
            {
                if (BackendOperator.Error == true)
                {
                    Popup popup = new Popup(BackendOperator.OperatorError.ErrorMessage());
                    popup.ShowDialog();
                }
            });
            TitleBorder.InputBindings.Add(mouseBinding);
        }

        private void Operator_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainWindow.UpdateConnections(this);
        }

        #region Mouse Events
        private void Operator_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.ClearKeyboardFocus();
            MainWindow.SelectOperator(this, Keyboard.Modifiers == ModifierKeys.Control, Keyboard.Modifiers == ModifierKeys.Shift);
            e.Handled = true;
        }
        
        private void TitleBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.ClearKeyboardFocus();
            MainWindow.OperatorToMove = this;
            MainWindow._LeftClickPosition = e.GetPosition(MainWindow.Workspace);
            MainWindow.SelectOperator(this, Keyboard.Modifiers == ModifierKeys.Control, Keyboard.Modifiers == ModifierKeys.Shift);
            e.Handled = true;
        }

        private void Pin_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (InputPinToInputRow.ContainsKey((Path)sender))
            {
                MainWindow.AttemptConnection(InputPinToInputRow[((Path)sender)]);
            }
            else if (OutputPinToOutputRow.ContainsKey((Path)sender))
            {
                MainWindow.AttemptConnection(OutputPinToOutputRow[((Path)sender)]);
            }
        }

        private void Pin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.SelectOperator(this, Keyboard.Modifiers == ModifierKeys.Control, Keyboard.Modifiers == ModifierKeys.Shift);

            if (e.ClickCount == 1)
            {
                if (InputPinToInputRow.ContainsKey((Path)sender))
                {
                    MainWindow.SelectedPin = InputPinToInputRow[(Path)sender];
                }
                else if (OutputPinToOutputRow.ContainsKey((Path)sender))
                {
                    MainWindow.SelectedPin = OutputPinToOutputRow[(Path)sender];
                }
            }
            else
            {
                if (InputPinToInputRow.ContainsKey((Path)sender))
                {
                    MainWindow.RemoveConnection(InputPinToInputRow[((Path)sender)]);
                }
                else if (OutputPinToOutputRow.ContainsKey((Path)sender))
                {
                    MainWindow.RemoveConnection(OutputPinToOutputRow[((Path)sender)]);
                }
            }
        }
        
        private void ResizeGrip_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow.IsResizingOperator = true;
            MainWindow.OperatorBeingResized = this;
        }

        private void ResizeGrip_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MainWindow.IsResizingOperator = false;
        }
        #endregion

        public void UpdateSelectionGraphic()
        {
            bool selected = MainWindow.SelectedOperators.Contains(this);
            if (!selected)
            {
                MainBorder.BorderThickness = new Thickness(2);
                MainBorder.SetResourceReference(Border.BorderBrushProperty, nameof(App.ColorBorder));
                MainGrid.Margin = new Thickness(0);
                MainBorder.Margin = new Thickness(0);
            }
            else
            {
                MainBorder.BorderThickness = new Thickness(5);
                MainBorder.SetResourceReference(Border.BorderBrushProperty, nameof(App.ColorSelectedOperator));
                MainBorder.Margin = new Thickness(-3);
            }
        }

        private Path ConnectionPinGraphic(bool isInput)
        {

            int size = 10;

            Path Path = new Path();
            Path.Fill = Brushes.Transparent;
            Path.Width = size * 2;
            Path.Height = size * 2;
            Path.Margin = new Thickness(isInput ? 2 : 0, 0, isInput ? 0 : 2, 0);
            Path.SetResourceReference(Path.StrokeProperty, "ColorConnection");

            PathGeometry pathGeometry = new PathGeometry();

            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(0, 0);
            pathGeometry.Figures.Add(pathFigure);

            LineSegment line1 = new LineSegment();
            line1.Point = new Point(0, 2 * size);
            pathFigure.Segments.Add(line1);

            LineSegment line2 = new LineSegment();
            line2.Point = new Point(size, 2 * size);
            pathFigure.Segments.Add(line2);

            LineSegment line3 = new LineSegment();
            line3.Point = new Point(2 * size, size);
            pathFigure.Segments.Add(line3);

            LineSegment line4 = new LineSegment();
            line4.Point = new Point(size, 0);
            pathFigure.Segments.Add(line4);

            LineSegment line5 = new LineSegment();
            line5.Point = new Point(0, 0);
            pathFigure.Segments.Add(line5);

            Path.Data = pathGeometry;

            return Path;
        }

        #region Draw Controls
        public void DrawInputsAndOutput()
        {
            if (InputGrid.ColumnDefinitions.Count > 1)
            {
                InputGrid.ColumnDefinitions.RemoveRange(1, InputGrid.ColumnDefinitions.Count - 1);
            }

            for (int i = 0; i < BackendOperator.Inputs.Count; i++)
            {
                OperatorInput input = BackendOperator.Inputs[i];
                InputGrid.RowDefinitions.Add(new RowDefinition());

                Grid inputGrid = inputOutputGrid();
                InputGrid.Children.Add(inputGrid);
                Grid.SetRow(inputGrid, i);

                Path inputPin = ConnectionPinGraphic(true);
                inputPin.HorizontalAlignment = HorizontalAlignment.Center;
                inputGrid.Children.Add(inputPin);
                Grid.SetColumn(inputPin, 0);

                StackPanel controlPanel = new StackPanel();
                controlPanel.VerticalAlignment = VerticalAlignment.Center;
                controlPanel.Margin = new Thickness(2, 0, 2, 0);
                inputGrid.Children.Add(controlPanel);
                Grid.SetColumn(controlPanel, 1);

                TextBlock inputTitle = new TextBlock();
                inputTitle.SetResourceReference(TextBlock.ForegroundProperty, nameof(App.ColorText));
                inputTitle.Text = input.Name;
                inputTitle.HorizontalAlignment = HorizontalAlignment.Center;
                inputTitle.VerticalAlignment = VerticalAlignment.Center;
                controlPanel.Children.Add(inputTitle);
                
                InputRow inputRow = new InputRow(this, input, inputGrid, inputPin, inputTitle);
                Inputs.Add(inputRow);
                OperatorInputToInputRow.Add(input, inputRow);

                createValueControls(inputRow, input, controlPanel);

                InputPinToInputRow.Add(inputPin, inputRow);
                
                inputPin.MouseUp += Pin_MouseUp;
                inputPin.MouseDown += Pin_MouseDown;
                
            }
            
            for (int i = 0; i < BackendOperator.Outputs.Count; i++)
            {
                OperatorOutput output = BackendOperator.Outputs[i];
                OutputGrid.RowDefinitions.Add(new RowDefinition());
                OutputGrid.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);

                Grid outputGrid = inputOutputGrid();
                OutputGrid.Children.Add(outputGrid);
                Grid.SetRow(outputGrid, i);

                StackPanel controlPanel = new StackPanel();
                controlPanel.VerticalAlignment = VerticalAlignment.Stretch;
                controlPanel.Margin = new Thickness(2, 0, 2, 0);
                outputGrid.Children.Add(controlPanel);
                Grid.SetColumn(controlPanel, 1);

                TextBlock outputTitle = new TextBlock();
                outputTitle.SetResourceReference(TextBlock.ForegroundProperty, nameof(App.ColorText));
                outputTitle.Text = output.Name;
                outputTitle.HorizontalAlignment = HorizontalAlignment.Center;
                outputTitle.VerticalAlignment = VerticalAlignment.Center;
                controlPanel.Children.Add(outputTitle);

                Path outputPin = ConnectionPinGraphic(false);
                outputPin.HorizontalAlignment = HorizontalAlignment.Center;
                outputGrid.Children.Add(outputPin);
                Grid.SetColumn(outputPin, 2);

                OutputRow row = new OutputRow(this, output, outputGrid, outputPin, outputTitle);
                Outputs.Add(row);
                OperatorOutputToOutputRow.Add(output, row);

                createValueControls(row, output, controlPanel);

                OutputPinToOutputRow.Add(outputPin, row);

                outputPin.MouseUp += Pin_MouseUp;
                outputPin.MouseDown += Pin_MouseDown;
            }
        }

        private Grid inputOutputGrid()
        {
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());

            grid.ColumnDefinitions[0].Width = new GridLength(25, GridUnitType.Pixel);
            grid.ColumnDefinitions[1].Width = new GridLength(4, GridUnitType.Star);
            grid.ColumnDefinitions[2].Width = new GridLength(25, GridUnitType.Pixel);
            grid.Margin = new Thickness(0, 0, 0, 5);

            return grid;
        }

        //todo: create user control for these
        private void createValueControls(InputOutputRow inputOutputRow, OperatorInputOutput operatorInputOutput, StackPanel controlPanel)
        {
            if (operatorInputOutput.ControlType == ControlType.Checkbox)
            {
                CheckBox checkBox = new CheckBox();
                checkBox.Margin = new Thickness(0, 0, 0, 2);
                checkBox.HorizontalAlignment = HorizontalAlignment.Center;

                checkBox.IsChecked = (bool)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("Value", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(bool));

                checkBox.Checked += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(checkBox.IsChecked, false);
                    }
                };
                checkBox.Unchecked += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(checkBox.IsChecked, false);
                    }
                };

                controlPanel.Children.Add(checkBox);
                inputOutputRow.Controls.Add(checkBox);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
            else if (operatorInputOutput.ControlType == ControlType.Label)
            {
                System.Windows.Controls.Label label = new System.Windows.Controls.Label();
                label.HorizontalAlignment = HorizontalAlignment.Center;

                label.Content = operatorInputOutput.Control.GetType().GetField("Value", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control).ToString();
                label.SetResourceReference(System.Windows.Controls.Label.ForegroundProperty, nameof(App.ColorText));

                controlPanel.Children.Add(label);
                inputOutputRow.Controls.Add(label);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
            else if (operatorInputOutput.ControlType == ControlType.MultilineTextbox)
            {
                TextBox textbox = new TextBox();
                textbox.TextWrapping = TextWrapping.Wrap;
                textbox.AcceptsReturn = true;

                textbox.HorizontalAlignment = HorizontalAlignment.Stretch;

                textbox.Text = operatorInputOutput.Control.GetType().GetField("Value", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control).ToString();

                textbox.TextChanged += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(textbox.Text, false);
                    }
                };

                controlPanel.Children.Add(textbox);
                inputOutputRow.Controls.Add(textbox);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
            else if (operatorInputOutput.ControlType == ControlType.Slider)
            {
                Slider slider = new Slider();
                slider.Margin = new Thickness(0, 0, 0, 2);

                System.Windows.Controls.Label label = new System.Windows.Controls.Label();
                label.Margin = new Thickness(0, 0, 0, 2);
                label.Content = slider.Value.ToString();
                label.HorizontalAlignment = HorizontalAlignment.Center;
                label.SetResourceReference(System.Windows.Controls.Label.ForegroundProperty, nameof(App.ColorText));

                slider.Minimum = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("Min", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));
                slider.Maximum = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("Max", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));
                slider.Value = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("DefaultValue", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));

                slider.ValueChanged += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(slider.Value, false);
                    }
                    label.Content = operatorInputOutput.Value == null ? "" : operatorInputOutput.Value.ToString();
                };

                controlPanel.Children.Add(label);
                controlPanel.Children.Add(slider);
                inputOutputRow.Controls.Add(label);
                inputOutputRow.Controls.Add(slider);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
            else if (operatorInputOutput.ControlType == ControlType.SliderWithTextbox)
            {
                Slider slider = new Slider();
                slider.Margin = new Thickness(0, 0, 0, 2);

                slider.Minimum = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("Min", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));
                slider.Maximum = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("Max", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));
                slider.Value = (double)Convert.ChangeType(operatorInputOutput.Control.GetType().GetField("DefaultValue", BindingFlags.Public | BindingFlags.Instance).GetValue(operatorInputOutput.Control), typeof(double));

                TextBox textBox = new TextBox();
                textBox.Margin = new Thickness(0, 0, 0, 2);
                textBox.Text = slider.Value.ToString();

                slider.ValueChanged += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(slider.Value, false);
                    }
                    textBox.Text = operatorInputOutput.Value == null ? "" : operatorInputOutput.Value.ToString();
                };

                textBox.TextChanged += (o, args) =>
                {
                    double d;
                    bool cast = double.TryParse(((TextBox)o).Text, out d);
                    if (cast)
                    {
                        slider.Value = d;
                    }
                };

                controlPanel.Children.Add(textBox);
                controlPanel.Children.Add(slider);
                inputOutputRow.Controls.Add(textBox);
                inputOutputRow.Controls.Add(slider);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
            else if (operatorInputOutput.ControlType == ControlType.Image)
            {
                Image image = new Image();
                System.Windows.Controls.Label colorLabel = new System.Windows.Controls.Label();
                System.Windows.Controls.Label depthLabel = new System.Windows.Controls.Label();

                image.VerticalAlignment = VerticalAlignment.Stretch;
                image.HorizontalAlignment = HorizontalAlignment.Stretch;
                image.Width = controlPanel.Width;
                image.MinHeight = 100;
                
                colorLabel.Padding = new Thickness(0);
                colorLabel.VerticalAlignment = VerticalAlignment.Stretch;
                colorLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
                colorLabel.Content = "Color";
                
                depthLabel.Padding = new Thickness(0);
                depthLabel.VerticalAlignment = VerticalAlignment.Stretch;
                depthLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
                depthLabel.Content = "Depth";

                controlPanel.Children.Add(image);
                controlPanel.Children.Add(colorLabel);
                controlPanel.Children.Add(depthLabel);
                inputOutputRow.Controls.Add(image);
                inputOutputRow.Controls.Add(colorLabel);
                inputOutputRow.Controls.Add(depthLabel);
                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);


                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftDoubleClick);
                mouseBinding.Command = new RelayCommand((o) =>
                {
                    ImagePopup imagePopup = new ImagePopup(this);
                    imagePopup.Show();
                });
                image.InputBindings.Add(mouseBinding);
            }
            else if (operatorInputOutput.ControlType == ControlType.Dropdown)
            {
                ComboBox comboBox = new ComboBox();
                comboBox.Margin = new Thickness(0, 0, 0, 2);

                string[] options = ((Dropdown)operatorInputOutput.Control).Options.Values.ToArray();

                for (int i = 0; i < options.Length; i++)
                {
                    comboBox.Items.Add(options[i]);
                }
                comboBox.SelectedItem = ((Dropdown)operatorInputOutput.Control).DefaultOption;

                comboBox.SelectionChanged += (o, args) =>
                {
                    if (operatorInputOutput is OperatorInput)
                    {
                        ((OperatorInput)operatorInputOutput).SetValue(((Dropdown)operatorInputOutput.Control).ParseStringToEnum((string)((ComboBox)o).SelectedItem), false);
                    }
                };
                
                controlPanel.Children.Add(comboBox);
                inputOutputRow.Controls.Add(comboBox);

                ControlToInputOutputRow.Add(operatorInputOutput.Control, inputOutputRow);
            }
        }
        #endregion

        public void UpdateContextMenu()
        {
            if(BackendOperator is Macro) { return; }
            if(BackendOperator.OperatorType.ID == "EmguCVImage")
            {
                ContextMenu.Items.Clear();

                MenuItem menuItem = new MenuItem();
                menuItem.Header = "See Error";
                menuItem.Click += MenuItem_SeeError_Click;
                ContextMenu.Items.Add(menuItem);

                ContextMenu.Items.Add(new Separator());

                menuItem = new MenuItem();
                menuItem.Header = "Cut";
                menuItem.Click += MenuItem_Cut_Click;
                ContextMenu.Items.Add(menuItem);

                menuItem = new MenuItem();
                menuItem.Header = "Copy";
                menuItem.Click += MenuItem_Copy_Click;
                ContextMenu.Items.Add(menuItem);

                menuItem = new MenuItem();
                menuItem.Header = "Delete";
                menuItem.Click += MenuItem_Delete_Click;
                ContextMenu.Items.Add(menuItem);

                if (BackendOperator.IsRootOperator)
                {
                    ContextMenu.Items.Add(new Separator());
                    menuItem = new MenuItem();
                    menuItem.Header = "Load Image";
                    menuItem.Click += MenuItem_LoadImage_Click;

                    ContextMenu.Items.Add(menuItem);
                }
                else
                {
                    ContextMenu.Items.Add(new Separator());
                    menuItem = new MenuItem();
                    menuItem.Header = "Save Image";
                    menuItem.Click += MenuItem_SaveImage_Click;

                    ContextMenu.Items.Add(menuItem);
                }
            }
        }

        #region Callbacks
        public bool ControlValueChanged(VisualOCVBackend.Controls.Control control, object value)
        {
            if(ControlToInputOutputRow.ContainsKey(control))
            {
                InputOutputRow inputOutputRow = ControlToInputOutputRow[control];
                if (control.ControlType == ControlType.Checkbox)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((CheckBox)inputOutputRow.Controls[0]).IsChecked = (bool)value;
                    }));
                    return true;
                }
                else if (control.ControlType == ControlType.Label)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((System.Windows.Controls.Label)inputOutputRow.Controls[0]).Content = value.ToString();
                    }));
                    return true;
                }
                else if (control.ControlType == ControlType.MultilineTextbox)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((TextBox)inputOutputRow.Controls[0]).Text = value.ToString();
                    }));
                    return true;
                }
                else if (control.ControlType == ControlType.Slider)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((System.Windows.Controls.Label)inputOutputRow.Controls[0]).Content = value.ToString();
                        ((Slider)inputOutputRow.Controls[1]).Value = (double)Convert.ChangeType(value, typeof(double));
                    }));
                    return true;
                }
                else if (control.ControlType == ControlType.SliderWithTextbox)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((TextBox)inputOutputRow.Controls[0]).Text = value.ToString();
                        ((Slider)inputOutputRow.Controls[1]).Value = (double)Convert.ChangeType(value, typeof(double));
                    }));
                    return true;
                }
                else if (control.ControlType == ControlType.Dropdown)
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((ComboBox)inputOutputRow.Controls[0]).SelectedItem = value.ToString();
                    }));
                    return true;
                }
                else if(control.ControlType == ControlType.Image)
                {
                    ImageControl imageControl = (ImageControl)control;
                    string color = imageControl.ColorType();
                    string depth = imageControl.DepthType();

                    System.Drawing.Bitmap bitmap = imageControl.ImageToBitmap();//convert image to bitmap synchronously with algorithm before drawing on screen, otherwise its possible the image can be modified during the conversion
                    App.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((System.Windows.Controls.Label)inputOutputRow.Controls[1]).Content = "Color: " + color;
                        ((System.Windows.Controls.Label)inputOutputRow.Controls[2]).Content = "Depth: " + depth;

                        ((Image)inputOutputRow.Controls[0]).Source = Utilities.BitmapToImageSource(bitmap);
                    }));

                    return true;
                }
            }
            return false;
        }

        public bool OperatorMoved(VisualOCVBackend.Operators.Operator op, float newX, float newY, float oldX, float oldY)
        {
            if(op == BackendOperator)
            {
                TranslateTransform transform = new TranslateTransform();
                transform.X = newX;
                transform.Y = newY;
                RenderTransform = transform;
                MainWindow.OperatorMoved(this);
                return true;
            }
            return false;
        }

        public bool OperatorErrorStateChanged(VisualOCVBackend.Operators.Operator op, bool error)
        {
            if(op == BackendOperator)
            {
                App.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (error)
                    {
                        TitleBorder.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                    }
                    else
                    {
                        TitleBorder.Background = new SolidColorBrush(Color.FromRgb(102, 255, 102));
                    }
                }));
                return true;
            }
            return false;
        }
        #endregion

        public class InputOutputRow
        {
            public Operator Operator;
            public Grid Grid;
            public Path ConnectionPin;
            public TextBlock Title;
            public List<System.Windows.FrameworkElement> Controls = new List<System.Windows.FrameworkElement>();

            public Point ConnectionPinPosition()
            {
                if (this is OutputRow)
                {
                    Point point = ConnectionPin.TransformToAncestor(Operator.MainWindow.OperatorContainer).Transform(new Point(0, 0));
                    point.X += ConnectionPin.Width;
                    point.Y += ConnectionPin.Height / 2;
                    return point; 
                }
                else
                {
                    Point point = ConnectionPin.TransformToAncestor(Operator.MainWindow.OperatorContainer).Transform(new Point(0, 0));
                    point.Y += ConnectionPin.Height / 2;
                    return point;
                }
            }

            public void DisableControls()
            {
                foreach(System.Windows.FrameworkElement control in Controls)
                {
                    if(!(control is System.Windows.Controls.Image))
                        control.IsEnabled = false;
                }
            }

            public void EnableControls()
            {
                foreach (System.Windows.FrameworkElement control in Controls)
                {
                    control.IsEnabled = true;
                }
            }
        }

        public class InputRow : InputOutputRow
        {
            public OperatorInput OperatorInput;

            public InputRow(Operator op, OperatorInput operatorInput, Grid grid, Path connectionPin, TextBlock title)
            {
                Operator = op;
                OperatorInput = operatorInput;
                Grid = grid;
                ConnectionPin = connectionPin;
                Title = title;
            }
        }
        
        public class OutputRow : InputOutputRow
        {
            public OperatorOutput OperatorOutput;

            public OutputRow(Operator op, OperatorOutput operatorOutput, Grid grid, Path connectionPin, TextBlock title)
            {
                Operator = op;
                OperatorOutput = operatorOutput;
                Grid = grid;
                ConnectionPin = connectionPin;
                Title = title;
            }
        }

        private void MenuItem_SeeError_Click(object sender, RoutedEventArgs e)
        {
            if(BackendOperator.Error == true)
            {
                Popup popup = new Popup(BackendOperator.OperatorError.ErrorMessage());
                popup.ShowDialog();
            }
        }

        private void MenuItem_SaveImage_Click(object sender, RoutedEventArgs e)
        {
            ImageControl imageControl = (ImageControl)BackendOperator.Outputs[0].Control;
            System.Drawing.Bitmap bitmap = imageControl.ImageToBitmap();
            if (bitmap == null) { return; }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image|*.jpg;*.jpeg;*.bmp;*.png|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                bitmap.Save(saveFileDialog.FileName);
            }
        }

        private void MenuItem_LoadImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image|*.jpg;*.jpeg;*.bmp;*.png|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                ImageControl imageControl = (ImageControl)BackendOperator.Outputs[0].Control;
                imageControl.LoadImageFromFile(openFileDialog.FileName);
                //ControlValueChanged(imageControl, imageControl.InputOutput.Value);
            }
        }
        
        private void MenuItem_Cut_Click(object sender, RoutedEventArgs e)
        {
            Point mousePos = MainWindow.GetMousePositionInWorkspace();
            App.Workspace.Cut((float)mousePos.X, (float)mousePos.Y);
        }

        private void MenuItem_Copy_Click(object sender, RoutedEventArgs e)
        {
            Point mousePos = MainWindow.GetMousePositionInWorkspace();
            App.Workspace.Copy((float)mousePos.X, (float)mousePos.Y);
        }

        private void MenuItem_Delete_Click(object sender, RoutedEventArgs e)
        {
            while(MainWindow.SelectedOperators.Count > 0)
            {
                MainWindow.SelectedOperators[0].Button_Delete_Click(sender, e);
            }
        }

        private void Button_Delete_Click(object sender, RoutedEventArgs e)
        {
            BackendOperator.Delete();
        }

        private void ResizeGrip_Loaded(object sender, RoutedEventArgs e)
        {
            _ResizeGrip = (ResizeGrip)sender;
            _ResizeGrip.Visibility = BackendOperator.Resizable() ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
