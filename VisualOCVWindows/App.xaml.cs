﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Reflection;

using VisualOCVBackend;
using VisualOCVBackend.Types;
using VisualOCVBackend.Validators;
using VisualOCVBackend.Operators;
using VisualOCVBackend.GUIListeners;

namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, ExceptionListener
    {

        public static Workspace Workspace;

        public static RoutedCommand CopyRoutedCommand = new RoutedCommand();
        public static RoutedCommand CutRoutedCommand = new RoutedCommand();
        public static RoutedCommand PasteRoutedCommand = new RoutedCommand();
        public static RoutedCommand DeleteRoutedCommand = new RoutedCommand();

        public static SolidColorBrush ColorText { get { return (SolidColorBrush)Application.Current.Resources["ColorText"]; } }
        public static SolidColorBrush ColorWindowBackground { get { return (SolidColorBrush)Application.Current.Resources["ColorWindowBackground"]; } }
        public static SolidColorBrush ColorSecondaryBackground { get { return (SolidColorBrush)Application.Current.Resources["ColorSecondaryBackground"]; } }
        public static SolidColorBrush ColorBorder { get { return (SolidColorBrush)Application.Current.Resources["ColorBorder"]; } }
        public static SolidColorBrush ColorConnection { get { return (SolidColorBrush)Application.Current.Resources["ColorConnection"]; } }

        public static SolidColorBrush ColorTitleNoError { get { return (SolidColorBrush)Application.Current.Resources["ColorTitleNoError"]; } }
        public static SolidColorBrush ColorTitleError { get { return (SolidColorBrush)Application.Current.Resources["ColorTitleError"]; } }
        public static SolidColorBrush ColorSelectedOperator { get { return (SolidColorBrush)Application.Current.Resources["ColorSelectedOperator"]; } }

        private static bool _isDarkTheme = false;
        public static bool IsDarkTheme { get { return _isDarkTheme; } }

        static Random rng = new Random();
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Workspace = new Workspace();
            Workspace.GUIListeners.AddExceptionOccuredListener(this);

            CopyRoutedCommand.InputGestures.Add(new KeyGesture(Key.C, ModifierKeys.Control));
            CutRoutedCommand.InputGestures.Add(new KeyGesture(Key.X, ModifierKeys.Control));
            PasteRoutedCommand.InputGestures.Add(new KeyGesture(Key.V, ModifierKeys.Control));
            DeleteRoutedCommand.InputGestures.Add(new KeyGesture(Key.Delete));
        }

        public static void DarkTheme()
        {
            _isDarkTheme = true;
            Application.Current.Resources["ColorText"] = Application.Current.Resources["ColorTextDarkTheme"];
            Application.Current.Resources["ColorWindowBackground"] = Application.Current.Resources["ColorWindowBackgroundDarkTheme"];
            Application.Current.Resources["ColorSecondaryBackground"] = Application.Current.Resources["ColorSecondaryBackgroundDarkTheme"];
            Application.Current.Resources["ColorBorder"] = Application.Current.Resources["ColorBorderDarkTheme"];
            Application.Current.Resources["ColorTitle"] = Application.Current.Resources["ColorTitleDarkTheme"];
            Application.Current.Resources["ColorConnection"] = Application.Current.Resources["ColorConnectionDarkTheme"];
        }

        public static void LightTheme()
        {
            _isDarkTheme = false;
            Application.Current.Resources["ColorText"] = Application.Current.Resources["ColorTextLightTheme"];
            Application.Current.Resources["ColorWindowBackground"] = Application.Current.Resources["ColorWindowBackgroundLightTheme"];
            Application.Current.Resources["ColorSecondaryBackground"] = Application.Current.Resources["ColorSecondaryBackgroundLightTheme"];
            Application.Current.Resources["ColorBorder"] = Application.Current.Resources["ColorBorderLightTheme"];
            Application.Current.Resources["ColorTitle"] = Application.Current.Resources["ColorTitleLightTheme"];
            Application.Current.Resources["ColorConnection"] = Application.Current.Resources["ColorConnectionLightTheme"];
        }
        
        public bool ExceptionOccured(Exception e)
        {
            return ExceptionOccured(new List<Exception>() { e });
        }

        public bool ExceptionOccured(List<Exception> e)
        {
            Popup popup = new Popup(e);
            popup.ShowDialog();
            return true;
        }
    }
}
