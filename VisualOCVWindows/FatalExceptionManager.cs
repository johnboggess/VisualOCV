﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Windows;
namespace VisualOCVWindows
{
    class FatalExceptionManager
    {
        public static string LogsLocation = System.AppDomain.CurrentDomain.BaseDirectory+"\\Crash Logs";

        public static void FatalExceptionThrown(Exception e)
        {
            LogException(e);

            if (IsException(e, typeof(FileLoadException)))
                UnblockDlls();

            Application.Current.Shutdown();
        }

        public static void LogException(Exception e)
        {
            if (!Directory.Exists(LogsLocation))
                Directory.CreateDirectory(LogsLocation);

            DateTime now = DateTime.Now;
            string fileName = now.Month + "_" + now.Day + "_" + now.Year + "_" + now.Hour + "_" + now.Minute + "_" + now.Second + ".txt";
            string message = GetExceptionMessage(e);

            File.Create(LogsLocation + "\\" + fileName).Close();
            File.WriteAllText(LogsLocation + "\\" + fileName, message);
        }

        public static string GetExceptionMessage(Exception e, bool innerException = false, string message="")
        {
            message += e.Message + "\n" + e.StackTrace + "\n";
            if (innerException)
                message = "##########Inner Exception##########n" + message;
            if (e.InnerException != null)
                message += GetExceptionMessage(e.InnerException, true);
            return message;
        }

        public static bool IsException(Exception e, Type type)
        {
            if (e.GetType() == type)
                return true;
            if (e.InnerException != null)
                return IsException(e.InnerException, type);
            return false;
        }

        public static void UnblockDlls()
        {
            ProcessStartInfo processInfo = new ProcessStartInfo("UnblockDlls.bat");

            Process process = Process.Start(processInfo);

            Popup popup = new Popup("It appears the dlls required for operators may be blocked. Attempting to unblock them.");
            popup.ShowDialog();
            popup.Focus();

            process.WaitForExit();

            popup.Close();
            if (process.ExitCode != 0)
            {
                popup = new Popup("Failed to unblock the required dlls. It is likely the unblocker is also blocked. Goto the the application directory, right click on UnblockDlls, click properties, check unblock, click apply, and run the file manually. Once its finished you can restart VisualOCV.");//todo: link to documentation
                popup.ShowDialog();
                popup.Focus();
            }
            else
            {
                popup = new Popup("Dlls should be unblocked, please restart VisualOCV.");
                popup.ShowDialog();
                popup.Focus();
            }


        }
    }
}
