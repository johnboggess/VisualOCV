﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using VisualOCVBackend.Operators;
namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for OperatorList.xaml
    /// </summary>
    public partial class OperatorList : UserControl
    {
        public event EventHandler<OperatorType> OperatorSelected;
        public event EventHandler<MacroType> MacroSelected;
        public object SelectedOperator_Macro
        {
            get
            {
                if(TreeView.SelectedItem == null) { return null; }
                if(TreeItemToOperator.ContainsKey((TreeViewItem)TreeView.SelectedItem))
                    return TreeItemToOperator[(TreeViewItem)TreeView.SelectedItem];
                if (TreeItemToMacro.ContainsKey((TreeViewItem)TreeView.SelectedItem))
                    return TreeItemToMacro[(TreeViewItem)TreeView.SelectedItem];
                return null;
            }
        }
        private Dictionary<TreeViewItem, OperatorType> TreeItemToOperator = new Dictionary<TreeViewItem, OperatorType>();
        private Dictionary<TreeViewItem, MacroType> TreeItemToMacro = new Dictionary<TreeViewItem, MacroType>();

        public OperatorList()
        {
            InitializeComponent();
            TreeView.Items.SortDescriptions.Add(new SortDescription("Header", ListSortDirection.Ascending));
        }
        
        public void Search(string searchTerm)
        {
            TreeView.Items.Clear();
            TreeItemToOperator.Clear();
            TreeItemToMacro.Clear();
            Tuple<OperatorModule, MacroModule> root = VisualOCVBackend.Operators.OperatorList.Search(searchTerm);
            if (root.Item1 != null)
            {
                LoadOperatorList(root.Item1, null);
            }
            if (root.Item2 != null)
            {
                LoadMacroList(root.Item2, null);
            }
            
            if (searchTerm != "")
            {
                ExpandAll();
                SelectTopOption();
            }
            else
            {
                ExpandAll(false);
            }
        }

        public void SelectTopOption()
        {
            ItemCollection itemCollection = TreeView.Items;
            if(itemCollection.Count == 0) { return; }
            _selectTopOption((TreeViewItem)itemCollection[0]);
        }

        private void _selectTopOption(TreeViewItem treeViewItem)
        {
            ItemCollection itemCollection = treeViewItem.Items;
            if (itemCollection.Count > 0)
            {
                _selectTopOption((TreeViewItem)itemCollection[0]);
            }
            else
            {
                treeViewItem.IsSelected = true;
            }
        }

        public void ExpandAll(bool expand = true)
        {
            ItemCollection itemCollection = TreeView.Items;
            foreach(TreeViewItem treeViewItem in itemCollection)
            {
                _expandAll(treeViewItem, expand);
            }
        }

        private void _expandAll(TreeViewItem treeViewItem, bool expand)
        {
            treeViewItem.IsExpanded = expand;
            ItemCollection itemCollection = treeViewItem.Items;
            foreach (TreeViewItem child in itemCollection)
            {
                _expandAll(child, expand);
            }
        }

        public void LoadOperatorList(OperatorModule opModule, TreeViewItem parentTreeViewItem)
        {
            TreeViewItem currentTreeViewItem = new TreeViewItem();
            currentTreeViewItem.Header = opModule.Name;
            if (parentTreeViewItem == null)
            {
                TreeView.Items.Add(currentTreeViewItem);
                TreeView.Items.Refresh();
            }
            else
            {
                parentTreeViewItem.Items.Add(currentTreeViewItem);
                TreeView.Items.Refresh();
            }

            foreach (OperatorModule mdl in opModule.SubModules)
            {
                LoadOperatorList(mdl, currentTreeViewItem);
            }

            List<OperatorType> opTypes = opModule.OperatorTypes.Values.ToList();
            foreach (OperatorType opType in opTypes)
            {
                TreeViewItem treeViewItemOperator = new TreeViewItem();
                treeViewItemOperator.Header = opType.Name;
                TreeItemToOperator.Add(treeViewItemOperator, opType);
                currentTreeViewItem.Items.Add(treeViewItemOperator);

            }

            currentTreeViewItem.Items.SortDescriptions.Clear();
            currentTreeViewItem.Items.SortDescriptions.Add(new SortDescription("Header", ListSortDirection.Ascending));
            currentTreeViewItem.Items.Refresh();
        }

        public void LoadMacroList(MacroModule module, TreeViewItem parentTreeViewItem)
        {
            TreeViewItem currentTreeViewItem = new TreeViewItem();
            currentTreeViewItem.Header = module.Name;
            if (parentTreeViewItem == null)
            {
                TreeView.Items.Add(currentTreeViewItem);
                TreeView.Items.Refresh();
            }
            else
            {
                parentTreeViewItem.Items.Add(currentTreeViewItem);
            }

            foreach (MacroModule mdl in module.SubModules)
            {
                LoadMacroList(mdl, currentTreeViewItem);
            }

            List<MacroType> macroTypes = module.MacroTypes.Values.ToList();
            foreach (MacroType macroType in macroTypes)
            {
                TreeViewItem treeViewItemOperator = new TreeViewItem();
                treeViewItemOperator.Header = macroType.Name;
                TreeItemToMacro.Add(treeViewItemOperator, macroType);
                currentTreeViewItem.Items.Add(treeViewItemOperator);
            }

            currentTreeViewItem.Items.SortDescriptions.Clear();
            currentTreeViewItem.Items.SortDescriptions.Add(new SortDescription("Header", ListSortDirection.Ascending));
            currentTreeViewItem.Items.Refresh();
        }

        public void ClearMacros()
        {
            for (int i = 0; i < TreeView.Items.Count; i++)
            {
                if ((string)((TreeViewItem)TreeView.Items[i]).Header == "Macros")
                {
                    TreeView.Items.RemoveAt(i);
                    break;
                }
            }
        }

        private void TreeViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (OperatorSelected != null && TreeItemToOperator.ContainsKey((TreeViewItem)sender))
            {
                object arg = TreeItemToOperator[(TreeViewItem)sender];
                OperatorSelected(this, (OperatorType)arg);
            }

            if(MacroSelected != null && TreeItemToMacro.ContainsKey((TreeViewItem)sender))
            {
                object arg = TreeItemToMacro[(TreeViewItem)sender];
                MacroSelected(this, (MacroType)arg);
            }
        }

        private void TreeViewItem_MouseMove(object sender, MouseEventArgs e)
        {
            if (SelectedOperator_Macro != null && e.LeftButton == MouseButtonState.Pressed)
                DragDrop.DoDragDrop(TreeView, SelectedOperator_Macro, DragDropEffects.Move);
        }
    }
}
