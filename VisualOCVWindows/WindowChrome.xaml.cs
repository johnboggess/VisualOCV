﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for WindowChrome.xaml
    /// </summary>
    public partial class WindowChrome : UserControl
    {
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register
        (nameof(Title), typeof(string), typeof(WindowChrome), new PropertyMetadata(string.Empty, TitleChanged));

        public static readonly DependencyProperty MinButtonProperty = DependencyProperty.Register
        (nameof(ShowMinimizeButton), typeof(bool), typeof(WindowChrome), new PropertyMetadata(true, MinBtnChanged));
        public static readonly DependencyProperty MaxButtonProperty = DependencyProperty.Register
        (nameof(ShowMaximizeButton), typeof(bool), typeof(WindowChrome), new PropertyMetadata(true, MaxBtnChanged));
        public static readonly DependencyProperty CloseButtonProperty = DependencyProperty.Register
        (nameof(ShowCloseButton), typeof(bool), typeof(WindowChrome), new PropertyMetadata(true, CloseBtnChanged));

        private static void TitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WindowChrome)d).TitleLabel.Content = e.NewValue;
        }

        private static void MinBtnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WindowChrome)d).BtnMin.Visibility = (bool)e.NewValue ? Visibility.Visible : Visibility.Collapsed;
        }

        private static void MaxBtnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WindowChrome)d).BtnMax.Visibility = (bool)e.NewValue ? Visibility.Visible : Visibility.Collapsed;
        }

        private static void CloseBtnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WindowChrome)d).BtnClose.Visibility = (bool)e.NewValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public bool ShowMinimizeButton
        {
            get { return (bool)GetValue(MinButtonProperty); }
            set { SetValue(MinButtonProperty, value); }
        }

        public bool ShowMaximizeButton
        {
            get { return (bool)GetValue(MaxButtonProperty); }
            set { SetValue(MaxButtonProperty, value); }
        }

        public bool ShowCloseButton
        {
            get { return (bool)GetValue(CloseButtonProperty); }
            set { SetValue(CloseButtonProperty, value); }
        }

        public WindowChrome()
        {
            InitializeComponent();
        }
        
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void Maximize_Click(object sender, RoutedEventArgs e)
        {
            if (Window.GetWindow(this).WindowState != WindowState.Maximized)
            {
                Window.GetWindow(this).WindowState = WindowState.Maximized;
            }
            else
            {
                Window.GetWindow(this).WindowState = WindowState.Normal;
            }
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).WindowState = WindowState.Minimized;
        }
    }
}
