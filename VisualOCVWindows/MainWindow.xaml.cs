﻿/* Created by John Bogggess 
 * Created to fullfill masters degree requirements at Southern Adventist Univeristy
 * 8-4-2020
 * johnboggess@jboggess.net
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;

using Microsoft.Win32;

using VisualOCVBackend;
using VisualOCVBackend.Controls;
using VisualOCVBackend.GUIListeners;
using VisualOCVBackend.Types;
using VisualOCVBackend.Operators;
using VisualOCVBackend.Validators;
namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, OperatorAddedListener, OperatorConnectionChangedListener, OperatorRemovedListener
    {
        public List<Operator> SelectedOperators = new List<Operator>();
        public Operator.InputOutputRow SelectedPin;
        public Operator OperatorToMove;

        public bool IsResizingOperator = false;
        public Operator OperatorBeingResized;

        public MatrixTransform GetViewTransform { get { return new MatrixTransform(_translateMatrix * _scaleMatrix); } }
        public MatrixTransform GetViewTransformInverse { get { return (MatrixTransform)new MatrixTransform(_translateMatrix * _scaleMatrix).Inverse; } }
        
        internal Point _LeftClickPosition = new Point(0, 0);
        
        private BezierConnection ConnectionLine;
        private Frame SelectionRectangle;
        private Dictionary<Operator.OutputRow, Dictionary<Operator.InputRow, BezierConnection>> OutputToConnection = new Dictionary<Operator.OutputRow, Dictionary<Operator.InputRow, BezierConnection>>();
        private Dictionary<Operator.InputRow, Dictionary<Operator.OutputRow, BezierConnection>> InputToConnection = new Dictionary<Operator.InputRow, Dictionary<Operator.OutputRow, BezierConnection>>();

        private Dictionary<VisualOCVBackend.Operators.Operator, Operator> BackendOperatorToGUIOperator = new Dictionary<VisualOCVBackend.Operators.Operator, Operator>();
        private Thread AlgorithmThread;
        private string OpenFile_FilePath = "";

        OperatorListWindow _opListPopup = null;

        private float _zoomLevel = 1;
        private float _zoomLevelInverse { get { return 1f / _zoomLevel; } }
        private bool _isTranslatingWorkspace = false;
        private bool _isSelectingRangeInOperators = false;
        private Matrix _translateMatrix = Matrix.Identity;
        private Matrix _scaleMatrix { get { return new ScaleTransform(_zoomLevel, _zoomLevel, Workspace.ActualWidth / 2, Workspace.ActualHeight / 2).Value; } }//Matrix.Identity;
        private Point? _newOperatorPosition = null;

        public MainWindow()
        {
            InitializeComponent();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            App.Workspace.GUIListeners.AddOperatorAddedListener(this);
            App.Workspace.GUIListeners.AddOperatorConnectionChangedListeners(this);
            App.Workspace.GUIListeners.AddOperatorRemovedListener(this);
            
            AssemblyManager.Init();
            VideoManager.Init();
            TypeManager.Workspace = App.Workspace;
            TypeManager.LoadAllTypes();
            ValidatorManager.LoadValidators(App.Workspace);
            OperatorModule.LoadModules(App.Workspace);
            MacroModule.LoadModules(App.Workspace);
            
            OperatorList.LoadOperatorList(OperatorModule.RootModule, null);
            OperatorList.LoadMacroList(MacroModule.RootModule, null);

            ConnectionLine = new BezierConnection(this, null);

            this.MouseDown += MainWindow_MouseDown;

            Workspace.MouseMove += Workspace_MouseMove;
            Workspace.MouseUp += Workspace_MouseUp;
            Workspace.MouseDown += Workspace_MouseDown;
            Workspace.MouseWheel += Workspace_MouseWheel;

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftDoubleClick);
            mouseBinding.Command = new RelayCommand((o) =>
            {
                if(_opListPopup != null) { return; }
                _opListPopup = new OperatorListWindow();
                _newOperatorPosition = GetMousePositionInWorkspace();
                _opListPopup.Closing += (sender, e) =>
                {
                    if(_opListPopup.SelectedOperatorMacro != null)
                    {
                        if (_opListPopup.SelectedOperatorMacro is OperatorType) { OperatorList_OperatorSelected(sender, (OperatorType)_opListPopup.SelectedOperatorMacro); }
                        else if (_opListPopup.SelectedOperatorMacro is MacroType) { OperatorList_MacroSelected(sender, (MacroType)_opListPopup.SelectedOperatorMacro); }
                    }
                    else { _newOperatorPosition = null; }
                    _opListPopup = null;
                };

                _opListPopup.Show();
            });
            Workspace.InputBindings.Add(mouseBinding);

            this.Closing += MainWindow_Closing;
            this.StateChanged += MainWindow_StateChanged;

            SelectionRectangle = new Frame();
            SelectionRectangle.Background = Brushes.AliceBlue;
            SelectionRectangle.BorderBrush = Brushes.LightBlue;
            SelectionRectangle.BorderThickness = new Thickness(1);
            SelectionRectangle.Opacity = .5;
            Workspace.Children.Add(SelectionRectangle);

            AlgorithmThread = new Thread(RunAlgorithm);
            AlgorithmThread.Start();
        }

        private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClearKeyboardFocus();
        }

        public Point GetMousePositionInWorkspace()
        {
            return Mouse.GetPosition(OperatorContainer);
        }

        public Point GetCenterOfView()
        {
            Point p = new Point((float)Workspace.ActualWidth / 2f, (float)Workspace.ActualHeight / 2f);
            return new MatrixTransform(_translateMatrix).Inverse.Transform(p);
        }

        public void ClearKeyboardFocus()
        {
            FocusManager.SetFocusedElement(this, this);
        }

        private void ButtonCopy(object sender, RoutedEventArgs e)
        {
            if (sender is MainWindow)
            {
                Point mousePos = GetMousePositionInWorkspace();
                App.Workspace.Copy((float)mousePos.X, (float)mousePos.Y);
            }
            else
            {
                Point p = GetCenterOfView();
                App.Workspace.Copy((float)p.X, (float)p.Y);
            }
        }

        private void ButtonCut(object sender, RoutedEventArgs e)
        {
            if (sender is MainWindow)
            {
                Point mousePos = GetMousePositionInWorkspace();
                App.Workspace.Cut((float)mousePos.X, (float)mousePos.Y);
            }
            else
            {
                Point p = GetCenterOfView();
                App.Workspace.Cut((float)p.X, (float)p.Y);
            }
        }

        private void ButtonPaste(object sender, RoutedEventArgs e)
        {
            if (sender is MainWindow)
            {
                Point mousePos = GetMousePositionInWorkspace();
                App.Workspace.Paste((float)mousePos.X, (float)mousePos.Y);
            }
            else
            {
                Point p = GetCenterOfView();
                App.Workspace.Paste((float)p.X, (float)p.Y);
            }
        }

        private void ButtonDelete(object sender, RoutedEventArgs e)
        {
            while(App.Workspace.SelectedOperators.Count > 0)
            {
                App.Workspace.DeleteSelectedOperators();
            }
            BezierConnection.RemoveSelected();
        }

        private void RunAlgorithm()
        {
            while (true)
            {
                App.Workspace.AlgorithmManager.BeginAlgorithm();
                while (!App.Workspace.AlgorithmManager.IsAlgorithmFinished) { }
                Thread.Sleep(10);
            }
        }

        private void Workspace_MouseMove(object sender, MouseEventArgs e)
        {
            if (OperatorToMove != null)
            {
                Point point = e.GetPosition(Workspace);
                float deltaX = (float)point.X - (float)_LeftClickPosition.X;
                float deltaY = (float)point.Y - (float)_LeftClickPosition.Y;
                _LeftClickPosition = point;

                deltaX = deltaX * _zoomLevelInverse;
                deltaY = deltaY * _zoomLevelInverse;
                foreach (Operator op in SelectedOperators)
                {
                    op.X += deltaX;
                    op.Y += deltaY;
                }
                return;
            }

            if(SelectedPin != null)
            {
                Point outputPos = SelectedPin.ConnectionPinPosition();
                Point mousePos = Mouse.GetPosition(OperatorContainer);
                mousePos.Y += BezierConnection.VisibleThickness;//dont have the end of the path right under the mouse, it messes with mouse events.
                ConnectionLine.UpdatePosition(mousePos, outputPos);
                return;
            }

            ConnectionLine.UpdatePosition(new Point(-1, -1), new Point(-1, -1));

            if (IsResizingOperator)
            {
                if(double.IsNaN(OperatorBeingResized.Height)) { OperatorBeingResized.Height = OperatorBeingResized.ActualHeight; }

                Point mousePosition = e.GetPosition(OperatorContainer);
                Point gripPosition = OperatorBeingResized._ResizeGrip.TranslatePoint(new Point(10, 10), OperatorContainer);

                float dW = (float)(mousePosition.X - gripPosition.X);
                float dH = (float)(mousePosition.Y - gripPosition.Y);
                
                OperatorBeingResized.Width = Math.Max(100, OperatorBeingResized.Width + dW);
                OperatorBeingResized.Height = Math.Max(100, OperatorBeingResized.Height + dH);
                
            }
            else if (_isTranslatingWorkspace)
            {
                Point tranlatePoint = Mouse.GetPosition(Workspace);
                float panDeltaX = (float)tranlatePoint.X - (float)_LeftClickPosition.X;
                float panDeltaY = (float)tranlatePoint.Y - (float)_LeftClickPosition.Y;
                _LeftClickPosition = tranlatePoint;

                _translateMatrix.OffsetX += panDeltaX * _zoomLevelInverse;
                _translateMatrix.OffsetY += panDeltaY * _zoomLevelInverse;
                OperatorContainer.RenderTransform = GetViewTransform;
            }
            else if(_isSelectingRangeInOperators)
            {
                MatrixTransform transform = GetViewTransformInverse;

                Point tranlatePoint = Mouse.GetPosition(Workspace);
                Rect rect = new Rect(transform.Transform(_LeftClickPosition), transform.Transform(tranlatePoint));
                
                Rect selectionRect = new Rect(_LeftClickPosition, tranlatePoint);
                SelectionRectangle.RenderTransform = new TranslateTransform(selectionRect.Left, selectionRect.Top);
                SelectionRectangle.Width = selectionRect.Width;
                SelectionRectangle.Height = selectionRect.Height;

                List<Operator> operators = BackendOperatorToGUIOperator.Values.ToList();

                foreach(Operator op in operators)
                {
                    Size size = op.RenderSize;
                    Point topLeft = new Point(op.X, op.Y);
                    Point bottomRight = new Point(op.X+size.Width, op.Y+size.Height);

                    Rect opRect = new Rect(topLeft, bottomRight);

                    if(opRect.IntersectsWith(rect))
                    {
                        App.Workspace.Select(op.BackendOperator.InstanceID);
                    }
                }

                UpdateOperatorSelectionGraphics();
            }

        }

        private void Workspace_MouseUp(object sender, MouseButtonEventArgs e)
        {
            OperatorToMove = null;
            SelectedPin = null;
            ConnectionLine.UpdatePosition(new Point(-1, -1), new Point(-1, -1));
            IsResizingOperator = false;
            _isTranslatingWorkspace = false;
            _isSelectingRangeInOperators = false;

            SelectionRectangle.Width = 0;
            SelectionRectangle.Height = 0;
        }
        
        internal void Workspace_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClearKeyboardFocus();
            if (Utilities.IsShiftHeld())
            {
                _isSelectingRangeInOperators = true;
                _LeftClickPosition = Mouse.GetPosition(Workspace);
            }
            else if(!Utilities.IsCtrlOrShiftHeld())
            {
                SelectOperator(null, false, false);
                BezierConnection.UnselectAll();
                _LeftClickPosition = Mouse.GetPosition(Workspace);
                _isTranslatingWorkspace = true;
            }
        }
        
        private void Workspace_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            _zoomLevel += Math.Sign(e.Delta) * .1f;
            _zoomLevel = Math.Max(Math.Min(1, _zoomLevel), .1f);
            
            //_scaleMatrix = new ScaleTransform(_zoomLevel, _zoomLevel, Workspace.ActualWidth / 2, Workspace.ActualHeight / 2).Value;
            OperatorContainer.RenderTransform = GetViewTransform;
        }

        public void AttemptConnection(Operator.InputOutputRow otherPin)
        {
            if (SelectedPin == null || otherPin.GetType() == SelectedPin.GetType()) { return; }

            if (SelectedPin is Operator.OutputRow)
            {
                Operator.InputRow inputRow = (Operator.InputRow)otherPin;
                if (SelectedPin != null)
                {
                    SelectedPin.Operator.BackendOperator.Connect(inputRow.OperatorInput, ((Operator.OutputRow)SelectedPin).OperatorOutput);
                }
                SelectedPin = null;
            }
            else if (SelectedPin is Operator.InputRow)
            {
                Operator.OutputRow outputRow = (Operator.OutputRow)otherPin;
                if (SelectedPin != null)
                {
                    SelectedPin.Operator.BackendOperator.Connect(((Operator.InputRow)SelectedPin).OperatorInput, outputRow.OperatorOutput);
                }
                SelectedPin = null;
            }
        }

        public void RemoveConnection(Operator.InputOutputRow pin)
        {
            if(pin is Operator.InputRow)
            {
                ((Operator.InputRow)pin).OperatorInput.RemoveConnection();
            }
            else
            {
                ((Operator.OutputRow)pin).OperatorOutput.RemoveAllConnections();
            }
        }

        public void OperatorMoved(Operator op)
        {
            UpdateLayout();
            UpdateConnections(op);
        }

        public void UpdateConnections(Operator op)
        {
            foreach (Operator.OutputRow outputRow in op.Outputs)
            {
                if (!OutputToConnection.ContainsKey(outputRow)) { continue; }

                List<Operator.InputRow> connectedInputs = OutputToConnection[outputRow].Keys.ToList();

                foreach (Operator.InputRow connection in connectedInputs)
                {
                    if (!connection.ConnectionPin.IsVisible) { continue; }
                    Point outputPos = outputRow.ConnectionPinPosition();
                    Point inputPos = connection.ConnectionPinPosition();

                    BezierConnection bezierConnection = OutputToConnection[outputRow][connection];
                    bezierConnection.UpdatePosition(outputPos, inputPos);
                }
            }
            
            foreach (Operator.InputRow inputRow in op.Inputs)
            {
                if (!InputToConnection.ContainsKey(inputRow)) { continue; }

                List<Operator.OutputRow> connectedOutputs = InputToConnection[inputRow].Keys.ToList();

                foreach (Operator.OutputRow connection in connectedOutputs)
                {
                    if (!connection.ConnectionPin.IsVisible) { continue; }
                    Point outputPos = connection.ConnectionPinPosition();
                    Point inputPos = inputRow.ConnectionPinPosition();

                    BezierConnection bezierConnection = InputToConnection[inputRow][connection];

                    PolyBezierSegment bezierSegment = new PolyBezierSegment();
                    bezierConnection.UpdatePosition(outputPos, inputPos);
                }
            }
        }

        public void SelectOperator(Operator _op, bool ctrlHeld, bool shiftHeld)
        {
            if(!(ctrlHeld || shiftHeld))
            {
                BezierConnection.UnselectAll();
            }

            if (_op != null)
            {
                App.Workspace.Select(_op.BackendOperator.InstanceID, ctrlHeld, shiftHeld);

                Panel.SetZIndex(_op, Operator.MaximumZIndex + 1);
                Operator.MaximumZIndex += 1;
                UpdateOperatorSelectionGraphics();
            }
            else
            {
                App.Workspace.ClearSelection();

                while (SelectedOperators.Count != 0)
                {
                    Operator _operator = SelectedOperators[0];
                    SelectedOperators.RemoveAt(0);
                    _operator.UpdateSelectionGraphic();
                }
            }
        }

        public void UpdateOperatorSelectionGraphics()
        {
            while (SelectedOperators.Count != 0)
            {
                Operator _operator = SelectedOperators[0];
                SelectedOperators.RemoveAt(0);
                _operator.UpdateSelectionGraphic();
            }


            foreach (VisualOCVBackend.Operators.Operator backendOp in App.Workspace.SelectedOperators)
            {
                Operator op = BackendOperatorToGUIOperator[backendOp];
                SelectedOperators.Add(op);
                op.UpdateSelectionGraphic();
            }
        }

        private void OperatorList_OperatorSelected(object sender, OperatorType e)
        {
            e.CreateInstance(App.Workspace.AlgorithmManager);
        }

        private void OperatorList_MacroSelected(object sender, MacroType e)
        {
            e.CreateInstance(App.Workspace.AlgorithmManager);
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            OperatorList.Search(SearchBox.Text);
        }

        private void MenuItem_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            //todo:stop algorithm before save
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "VisualOCV Algorithm|*." + VisualOCVBackend.Workspace.AlgorithmFileExtension + "|All Files|*.*";
            saveFileDialog.DefaultExt = VisualOCVBackend.Workspace.AlgorithmFileExtension;
            if (saveFileDialog.ShowDialog() == true)
            {
                OpenFile_FilePath = saveFileDialog.FileName;
                File.WriteAllText(OpenFile_FilePath, App.Workspace.Save());
            }
        }

        private void MenuItem_SaveAsMacro_Click(object sender, RoutedEventArgs e)
        {
            //todo:stop algorithm before save
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "VisualOCV Macro|*." + VisualOCVBackend.Workspace.MacroFileExtension + "|All Files|*.*";
            saveFileDialog.DefaultExt = VisualOCVBackend.Workspace.MacroFileExtension;
            saveFileDialog.InitialDirectory = System.IO.Path.GetFullPath(MacroModule.ModuleLocation);
            if (saveFileDialog.ShowDialog() == true)
            {
                OpenFile_FilePath = saveFileDialog.FileName;
                File.WriteAllText(OpenFile_FilePath, App.Workspace.Save());
                MacroModule.LoadModules(App.Workspace);
                OperatorList.ClearMacros();
                OperatorList.LoadMacroList(MacroModule.RootModule, null);
            }
        }

        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            //todo:stop algorithm before save
            if (OpenFile_FilePath == "")
            {
                MenuItem_SaveAs_Click(sender, e);
            }
            else
            {
                File.WriteAllText(OpenFile_FilePath, App.Workspace.Save());
            }
        }

        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            //todo:stop algorithm before open
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "VisualOCV Algorithm|*." + VisualOCVBackend.Workspace.AlgorithmFileExtension + "|All Files|*.*";
            openFileDialog.DefaultExt = VisualOCVBackend.Workspace.AlgorithmFileExtension;
            if (openFileDialog.ShowDialog() == true)
            {
                OpenFile_FilePath = openFileDialog.FileName;
                App.Workspace.Clear();
                App.Workspace.Load(File.ReadAllText(OpenFile_FilePath));
                _zoomLevel = 1;
                _translateMatrix = Matrix.Identity;
                OperatorContainer.RenderTransform = GetViewTransform;
            }
        }

        private void MenuItem_New_Click(object sender, RoutedEventArgs e)
        {
            App.Workspace.Clear();
            _zoomLevel = 1;
            _translateMatrix = Matrix.Identity;
            OperatorContainer.RenderTransform = GetViewTransform;
            OpenFile_FilePath = "";
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItem_ViewHelp_Click(object sender, RoutedEventArgs e)
        {
            App.Workspace.OpenWiki();
        }

        private void MenuItem_SwitchTheme_Click(object sender, RoutedEventArgs e)
        {
            if (App.IsDarkTheme)
            {
                App.LightTheme();
            }
            else
            {
                App.DarkTheme();
            }
        }

        private void MenuItem_ViewAbout_Click(object sender, RoutedEventArgs e)
        {
            new About().Show();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AlgorithmThread.Abort();
            App.Workspace.Clear();
        }


        private void MainWindow_StateChanged(object sender, EventArgs e)
        {
            if(WindowState == WindowState.Maximized)
            {
                WindowGrid.Margin = new Thickness(5);
                return;
            }
            WindowGrid.Margin = new Thickness(0);
        }

        public void OperatorAdded(VisualOCVBackend.Operators.Operator op)
        {
            Operator newOp = new Operator(op, this);
            newOp.Width = newOp.BackendOperator.Width;

            OperatorContainer.Children.Add(newOp);
            BackendOperatorToGUIOperator.Add(op, newOp);
            UpdateLayout();//force operator into visual tree

            if (_newOperatorPosition == null)
            {
                Point Center = GetCenterOfView();
                newOp.X = (float)Center.X - newOp.CenterX;
                newOp.Y = (float)Center.Y - newOp.CenterY;
            }
            else
            {
                newOp.X = (float)_newOperatorPosition.Value.X - newOp.CenterX;
                newOp.Y = (float)_newOperatorPosition.Value.Y - newOp.CenterY;
            }

            
            Panel.SetZIndex(newOp, Operator.MaximumZIndex + 1);
            Operator.MaximumZIndex += 1;
        }


        public void OperatorRemoved(VisualOCVBackend.Operators.Operator op)
        {
            Operator GUIOperator = BackendOperatorToGUIOperator[op];
            BackendOperatorToGUIOperator.Remove(op);

            if (SelectedOperators.Contains(GUIOperator))
            {
                SelectedOperators.Remove(GUIOperator);
            }

            if(SelectedPin != null && SelectedPin.Operator == GUIOperator)
            {
                SelectedPin = null;
            }

            if(OperatorToMove == GUIOperator)
            {
                OperatorToMove = null;
            }

            OperatorContainer.Children.Remove(GUIOperator);

        }

        public void OperatorConnectionChanged(OperatorInput target, OperatorOutput source, bool connectionCreated)
        {
            if(connectionCreated)
            {
                Operator opSource = BackendOperatorToGUIOperator[source.Operator];
                Operator opTarget = BackendOperatorToGUIOperator[target.Operator];
                Operator.OutputRow outputRow = opSource.OperatorOutputToOutputRow[source];
                Operator.InputRow inputRow = opTarget.OperatorInputToInputRow[target];

                outputRow.DisableControls();//todo: remove
                inputRow.DisableControls();

                PolyBezierSegment bezierSegment = new PolyBezierSegment();

                if (!OutputToConnection.ContainsKey(outputRow))
                {
                    OutputToConnection.Add(outputRow, new Dictionary<Operator.InputRow, BezierConnection>());
                }

                if (!InputToConnection.ContainsKey(inputRow))
                {
                    InputToConnection.Add(inputRow, new Dictionary<Operator.OutputRow, BezierConnection>());
                }

                BezierConnection bezierConnection = new BezierConnection(this, target);

                OutputToConnection[outputRow].Add(inputRow, bezierConnection);
                InputToConnection[inputRow].Add(outputRow, bezierConnection);
                
                UpdateConnections(opSource);

                opSource.UpdateContextMenu();
                opTarget.UpdateContextMenu();
            }
            else
            {
                Operator opSource = BackendOperatorToGUIOperator[source.Operator];
                Operator opTarget = BackendOperatorToGUIOperator[target.Operator];
                Operator.OutputRow outputRow = opSource.OperatorOutputToOutputRow[source];
                Operator.InputRow inputRow = opTarget.OperatorInputToInputRow[target];

                outputRow.EnableControls();
                inputRow.EnableControls();

                OutputToConnection[outputRow][inputRow].RemoveVisual();
                OutputToConnection[outputRow].Remove(inputRow);

                InputToConnection[inputRow][outputRow].RemoveVisual();
                InputToConnection[inputRow].Remove(outputRow);

                UpdateConnections(opSource);

                opSource.UpdateContextMenu();
                opTarget.UpdateContextMenu();
            }
        }

        private void Workspace_DropOperator(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(typeof(OperatorType)))
            {
                OperatorType operatorType = (OperatorType)e.Data.GetData(typeof(OperatorType));
                VisualOCVBackend.Operators.Operator op = operatorType.CreateInstance(App.Workspace.AlgorithmManager);
                Point MousePos = e.GetPosition(OperatorContainer);
                op.SetPosition((float)MousePos.X, (float)MousePos.Y);
                
            }
            else if (e.Data.GetDataPresent(typeof(MacroType)))
            {
                MacroType macroType = (MacroType)e.Data.GetData(typeof(MacroType));
                VisualOCVBackend.Operators.Operator op = macroType.CreateInstance(App.Workspace.AlgorithmManager);
                Point MousePos = e.GetPosition(OperatorContainer);
                op.SetPosition((float)MousePos.X, (float)MousePos.Y);
            }
        }
    }
}
