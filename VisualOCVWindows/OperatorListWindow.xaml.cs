﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using VisualOCVBackend.Operators;
namespace VisualOCVWindows
{
    /// <summary>
    /// Interaction logic for OperatorListWindow.xaml
    /// </summary>
    public partial class OperatorListWindow : Window
    {
        public object SelectedOperatorMacro = null;
        private Dictionary<TreeViewItem, OperatorType> TreeItemToOperator = new Dictionary<TreeViewItem, OperatorType>();
        
        public OperatorListWindow()
        {
            InitializeComponent();
            OperatorList.LoadOperatorList(OperatorModule.RootModule, null);
            OperatorList.LoadMacroList(MacroModule.RootModule, null);
            SearchBox.Focus();
        }

        private void OperatorList_OperatorSelected(object sender, OperatorType e)
        {
            SelectedOperatorMacro = e;
            Close();
        }

        private void OperatorList_MacroSelected(object sender, MacroType e)
        {
            SelectedOperatorMacro = e;
            Close();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            OperatorList.Search(SearchBox.Text);
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            SelectedOperatorMacro = OperatorList.SelectedOperator_Macro;
            Close();
        }
    }
}
